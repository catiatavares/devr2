@Regression
Feature: Payment Support Contact Details
  As a client
  I want to be able click on Customer Support
  So I can view view Payment support contact details

  @PaymentSupport
  Scenario:
    Given I am on the Home Page
    And I click on Customer Support scheme
    When I click on Payment Support Contact Details page
    Then I can see the "Payment Support Contact Details" display
