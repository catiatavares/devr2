@Regression
Feature: General Support Contact Details
  As a client
  I want to be able click on Customer Support
  So I can view view General support contact details

  @GeneralSupport
  Scenario:
    Given I am on the Home Page
    And I click on Customer Support scheme
    When I click on General Support Contact Details page
    Then I can view "General Support Contact Details" displayed