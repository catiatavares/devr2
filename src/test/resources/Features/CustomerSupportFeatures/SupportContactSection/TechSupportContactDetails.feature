@Regression
Feature: Technical Support Contact Details
  As a client
  I want to be able click on Customer Support
  So I can view view technical support contact details

  @TechnicalSupport
  Scenario:
    Given I am on the Home Page
    And I click on Customer Support scheme
    When I click on Technical Support Contact Details page
    Then I can see it "Technical Support Contact Details" displayed