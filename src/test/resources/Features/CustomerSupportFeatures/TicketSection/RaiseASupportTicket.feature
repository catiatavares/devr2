@regression
Feature: Raise A Support Ticket
  As a client buyer
  I want to raise a support ticket to customer service

  @raiseticket
  Scenario: Raise A Support Ticket
    Given I am on the Home Page
    When I click on Customer Support scheme
    And I click on Raise a Support Ticket
    And I enter Subject & Message in subject field in fields provided
    And I tick checkbox and click on submit ticket button
    Then I can see "Ticket Raised" displayed on ticket screen