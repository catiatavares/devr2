@Regression

Feature: Frequently asked Questions
  As a client
  I want to be able click on FAQ
  So I can view frequently asked questions & Answers

  @FAQ
  Scenario:
    Given I am on the Home Page
    And I click on Customer Support scheme
    When I click on FAQ page
    Then I can see the frequently asked questions of Payment Process section
    Then I can see the frequently asked questions of Registration section
    Then I can see the frequently asked questions of Payment Status section
    Then I can see the frequently asked questions of Other section
