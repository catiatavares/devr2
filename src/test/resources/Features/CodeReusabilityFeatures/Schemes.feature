@reg
  Feature: schemes should be called from here
  As a user
  I want to call schemes from here
  So I can avoid duplication of re-usable code

  Scenario: Calling common object across the project
    And I click on corporate settings
    And I click on settings
