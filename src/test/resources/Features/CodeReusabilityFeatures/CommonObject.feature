@reg
Feature: common objects should be called from here
  As a user
  I want to call common objects from here
  So I can avoid duplication of re-usable code

  Scenario: Calling common object across the project
    And I click Ok button
    And I click on OK button
    And I click on save button
    And I click the Edit button
    And I click on Delete button
    And I click on the place order button
    And I click on save button
    And I click on submit type button
    And I click on search button
    And I click on First Transaction from search result
    And I click continue
    And I click on back button
    And I click on home scheme






