@regression
Feature: Inviting a New Vendor
  As a client buyer
  I want to send an invitation a new vendor

  Background:
    Given I am on the Home Page
    When I click on invite a new vendor

  @invitess
  Scenario: Invite a new vendor
    And I fill the required fields below:
      | Field                 | Values                |
      | name Field            | Realist Mosh          |
      | addy1 Field           | 15 London road        |
      | addy2 Field           | Plumstead             |
      | cty   Field           | London                |
      | postcode Field        | SE18 1HP              |
      | area Field            | London                |
      | telephoneNum Field    | +442035698745         |
      | mobileNum Field       | +447908273459         |
      | faxNum Field          | +442085851265         |
      | emailAdd Field        | realistmosh@yahoo.com |
      | confirmEmailAdd Field | realistmosh@yahoo.com |
    And I select dropdown in Vendorform
      | VAT/Tax Registered Country  | UNITED KINGDOM |
      | VAT/Tax Registration Number | GB928644589    |
    And I click on Send Invitation
    And I click on OK button
    Then I should be able to see a new page with a message "Vendor invitation has been successfully sent to the following vendor."

  @resendinvitation
  Scenario: Resend Invitation
    And I loop into Resend Invitation to resend pending invitation

  @cancelinvitation
  Scenario: Cancel Invitation
    And I loop into Cancel Invitation to cancel pending invitation

