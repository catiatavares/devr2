@regression
Feature: Add New User
  As an admin
  I want to be able to Add New Users
  So that i can manage them within my corporate

  @addnewusermgt
  Scenario: Adding a New User
    Given I am on the Home Page
    When I click on corporate settings
    When I click on Add New User
    And I entered username as below:
      | Username | usertest |
    And I click on Next Step
    Then my username and password login details should display
    And I click continue
    And I enter the details below
      | Firstname        | Realists              |
      | Surname          | RealMVPist            |
      | Address1         | 10 sedgemere road     |
      | Address 2        | Abbeywood             |
      | City             | London                |
      | Postcode         | SE2 9SW               |
      | Country          | UNITED KINGDOM        |
      | Area/State       | London                |
      | Email Add        | bhavinoptal@gmail.com |
      | Confirm Email    | bhavinoptal@gmail.com |
      | Telephone number | 442035698745          |
      | Mobile number    | 447424441108          |
      | FaxNumber        | 442085851265          |
    And I click on Add User
    Then user should be added and page redirected to User Settings