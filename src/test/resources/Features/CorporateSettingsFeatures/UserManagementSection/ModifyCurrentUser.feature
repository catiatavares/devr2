@regression
Feature: Add New User
  As an admin
  I want to be able to Add New Users
  So that i can manage them within my corporate

  @modifyuser
  Scenario: Modify User
    Given I am on the Home Page
    When I click on corporate settings
    Then I click on Modify User
    And I should see "Modify Current System Users"
    Then I select user with Area Manager as name to Edit
    And I click on Amend Profile Details