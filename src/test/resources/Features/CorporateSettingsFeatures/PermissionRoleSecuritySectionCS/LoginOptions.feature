@regression
@loginOpt
Feature: Login Options
  As a client
  I should be able to use the Login options feature
  so that i can set password expiry alerts
  and add extra security to my login option.

  Background:
    Given I am on the Home Page
    When I click on corporate settings
    And I click on Login Options

  @loginoptions
  Scenario: Setting password expiry alert
    And I tick the checkbox
    And I enter figure in the table below
      | expire passwords older than   | 40 |
      | send expiry email alert after | 30 |
    And I tick the lock accounts with unchanged passwords
    And I tick checkbox for Email to go out until password expires
    And I click on save button
    Then I should see login options settings successfully updated
    And I untick the checkbox to disable feature
    Then I click on save button

  @extrasecurity
  Scenario: Adding Extra security to Login Options
    And I tick and untick the extra security checkbox
    And I click on save button
    Then I should see login options settings successfully updated
