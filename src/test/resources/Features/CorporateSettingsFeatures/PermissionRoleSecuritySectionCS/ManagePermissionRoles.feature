@regression
@permission
Feature: Managing Permission and Roles -Create New Role
  As an admin user
  I want to be able to create a new role
  so that i can view and edit permission for my corporate

  Background:
    Given I am on the Home Page
    When I click on corporate settings
    And I click on Manage permission roles

  @createnewrole
  Scenario: Creating a new role
    And I click on Create a new role
    And I enter text in the field below
      | Description | WebtesterAdmin |
    And I tick checkbox Allow the user to raise a quote
    And I click on save button
    Then I should see permission roles updated successfully
# Editing role
    And I click the Edit button
    And I change description
    And I click on save button
    Then I should see permission roles updated successfully

 # @deleterole
 # Scenario: Deleting roles
   # Given I click on Delete button
   # And I click on OK button
    #Then I should see permission roles updated successfully