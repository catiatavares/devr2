@regression
Feature: Amend Budget Code

  @amendBudget
  Scenario: Amend Budget Code
    Given I am on the Home Page
    And I click on corporate settings
    And I click on Amend Budget Code link
    When I click Add Budget Code button
    And I enter text in the popup window and click save code
      | Field       | Value     |
      | Description | old codes |
      | Code        | CODE ABC  |
    Then Text "Budget Code added successfully" should be displayed
    #
    And I click the Edit button
    Then I edit details of Code
      | Field       | Value    |
      | Description | new code |
      | Code        | CODE DEF |
    And I click on submit type button
    Then I can see "Budget Code edited successfully" displayed on screen
    #
    Given I click on Delete button
    And I click on OK button
    Then I can see "Budget Code deleted" displayed