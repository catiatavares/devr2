@regression
Feature: Amend GL Code

  @amendgl
  Scenario: Amend GL Code
    Given I am on the Home Page
    And I click on corporate settings
    And I click on Amend GL Code link
    When I click Add GL Code button
    And I enter text in the popup
      | Field          | Value         |
      | Description    | enter desc    |
      | Code           | enter code    |
      | Pop-up Message | enter message |
    And I click on submit type button
    Then I can see Add "General Ledger Code added successfully" displayed
    #
    And I click the Edit button
    Then I edit details of glCode
      | Description    | M&S              |
      | Code           | Clothin          |
      | Pop-up Message | Clothing Materia |
    And I click on submit type button
    Then I can see Edit "General Ledger Code edited successfully" displayed
   #
    Given I click on Delete button
    And I click on OK button
    Then I can see Delete "General Ledger Code deleted" displayed