@regression
Feature: Amend Cost Centre Code

  @amendCostCentre
  Scenario: Amend Cost centre Code
    Given I am on the Home Page
    And I click on corporate settings
    And I click on Cost centre Code link
    When I click Add Cost centre Code button
    And I enter text in the popup window and click save code
      | Field       | Value                  |
      | Description | costcode desc          |
      | Code        | codeCode message field |
    Then Added "Cost Centre added successfully" should be displayed
    #
    And I click the Edit button
    Then I edit details of Code
      | Field       | Value                 |
      | Description | edited CostCode desc  |
      | Code        | edited CostCode field |
    And I click on submit type button
    Then Edited "Cost Centre edited successfully" should be displayed
    #
    Given I click on Delete button
    And I click on OK button
    Then Deleted "Cost Centre deleted" should be displayed