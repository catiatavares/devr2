@regression
Feature: PCard Management Lodge cards
  As a client
  I should be able to manage Lodge cards
  so that I can Add, Edit and Delete Lodge Cards

  @paylodgecards
  Scenario: Adding Lodge cards
    Given I am on the Home Page
    When I click on corporate settings
    And I click on PCard Management lodge cards under financial section
    And I click on Add a New Card button
    And I fill the form fields below and click on submit type button
      | Card Number            | 4111111111111111        |
      | CVV Number             | 555                     |
      | Card Currency          | Dollars (United States) |
      | Card Type              | Visa                    |
      | Card Holder Title      | Mr                      |
      | Card Holder First Name | Ade                     |
      | Card Holder Surname    | Moshood                 |
      | Expiry Month           | 2                       |
      | Expiry Year            | 2022                    |
      | Issue Number           | 15                      |
      | Limit                  | 1000                    |
      | Description            | Team add description    |
    And I should see PCard Added "New P-Card added successfully" message

    #  User Edit(Amend) Master Card
    And I click the Edit button
    And I edit the below details and click on submit type button
      | Card Number            | 5555555555554444 |
      | Card Holder First Name | Ade              |
    And I should see Pcard Edited "P-Card edited successfully" message

    #  Deleted New added  Edited card
    And I click on Delete button
    And I click on OK button
    And I should see Pcard Deleted "P-Card deleted" message
    And I can successfully log out





