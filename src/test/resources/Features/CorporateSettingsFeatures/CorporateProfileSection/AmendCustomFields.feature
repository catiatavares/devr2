@regression
Feature: Amend Custom Fields
  As a user
  I want to be able to configure custom fields
  So that these are available to users on the transaction screen

  @AmendCustomFields
  Scenario: Amend Custom Fields
    Given I am on the Home Page
    And I click on corporate settings
    And I amend custom fields
    Then I click on add field
    And I click on fields
    And I enter text in name
    And I select type
    And I select level
    And I click on save button
    Then I should see "Custom Field added successfully" massage
    And I click on back button
    Then I should see "Custom Field Visibility Updated successfully" successfully




