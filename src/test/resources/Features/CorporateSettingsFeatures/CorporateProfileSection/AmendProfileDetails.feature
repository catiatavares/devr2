@regression
Feature: Amend Profile Details
  As a client buyer
  I want to be able to amend profile detail
  So I can manage my corporate account

  @amendprof
  Scenario: Amend Profile Details
    Given I am on the Home Page
    And I click on corporate settings
    And I click on Amend Profile Details
    And I edit profile detail
      | Fields                      | Values             |
      | Display Name                | Barclays           |
      | Trading Name                | Barclays Ltd       |
      | Address 1                   | 44 barclays street |
      | Address 2                   | east london        |
      | Town/City                   | eastham            |
      | Postcode                    | ec9 8jh            |
      | County                      | london             |
      | Main Contact Name           | Paul James         |
      | Main Email Address          | test@yahoo.com     |
      | Confirm Email Address       | test@yahoo.com     |
      | Telephone Number            | +442035698745      |
      | Mobile Number               | +442035698745      |
      | Fax Number                  | +442083698521      |
      | VAT/Tax Registered Country  | UNITED KINGDOM     |
      | VAT/Tax Registration Number | GB928644589        |
      | Company Registration Number | A987654            |
      | Country                     | UNITED KINGDOM     |
      | Country                     | UNITED KINGDOM     |
      | Currency                    | Euro               |
    Then I click Save Profile button