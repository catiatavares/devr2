@regression
Feature: Corporate message
  As a client buyer
  I want to be able to create a corporate message
  So I can view the message displayed

  @CorpMgs
  Scenario: Corporate message display
    Given I am on the Home Page
    And I click on corporate settings
    And I click on Corporate Messages
    And Add Message and tick Corporate and Orders
    And I can see "Ade Testing" on Corporate Homepage
    And I search for an order
    Then I can see "Ade Testing" on Order footer


