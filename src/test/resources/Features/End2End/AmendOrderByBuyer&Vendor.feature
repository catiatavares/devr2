@regression
@AmendOder
Feature: Amend Oder Flow by Vendor & Buyer
 # Call Create order test
 # Amend Order by Vendor (Add Comment, Add & Remove line, Line Checkboxes)
 # Amend Order by Buyer ( Add Comment, Add & Remove line, Vat Rate, Currency Dropdown, Vendor Fee Subsidy, Header/Line Checkboxes)

  Scenario: Amend Order Flow by Vendor and buyer
  #call add pCard Tests and pcard Assignment
    Given I am on the Home Page
    And I click on corporate settings
    And I call Add PCard lodge test
    And I click on submit type button
    And I click on settings
    And I click on P-Card Management - Lodge Cards Assignment
    And I tick a checkbox
    And I click on save button

    #create order test

    And I click on home scheme
    When I search for Vendor
    And I enter a "vendor name" on the search field and other fields
    Then I should be able to see "vendor name" and details of that specific Vendor
    When I select the "place order" button for the selected vendor
    And I fill the required fields as below: and click OK button
      | Field       | Value       |
      | Item Name   | item name   |
      | Order Price | order price |
      | Line GLCode | gl code     |
    And I select a "commodity code"
    And I select to split the order
    And I should fill up split line coding form
    And I should add line & remove line
    And I should add Comment
    And I enter the date and click place order button
    And I select Payment Card
    And I select Address
   # And I click on the place order button
    Then I should be able to successfully place and get an order number
    When I click the order number
    Then I can get the order status as "Awaiting Vendor Confirmation"
    And I can successfully log out

 # Amend Order by Vendor
    When I log in with "vendor username" and "vendor password"
    And I search for the new order
    And I select the order
    Then I can see order status as "New Order"
    And I add new line
      | Field       | Value       |
      | Item Name   | item name   |
      | Item Code   | item code   |
      | Order Price | order price |
    And I Change currency and submit change
    Then I can get the order status as "Awaiting Buyer Confirmation"
    And I can successfully log out

    # login to buyer and change subsidy by buyer
    Given I am on the Home Page
    And I search for the new order
    And I select the order
    And I should fill up split line coding form by vendor
    And I Update auxiliary details
    When I update line coding custom fields
    Then I can get the order status as "Awaiting Vendor Confirmation"
    And I can successfully log out

    # Accept amend by vendor
    When I log in with "vendor username" and "vendor password"
    And I search for the new order
    And I select the order
    When I Accept order
    And I accept full order and dispatch good
    Then I can get the order status as "Awaiting Delivery Confirmation"
    And I can successfully log out

   # Login to buyer and accept
    Given I am on the Home Page
    And I search for the new order
    And I select the order
    When I accept full order and receipt goods & authorise payment
    Then I can get the order status as "Archived"
    And I can successfully log out