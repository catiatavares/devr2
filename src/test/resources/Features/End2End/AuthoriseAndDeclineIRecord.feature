@regression
Feature: Authorise I Record
  As a corporate user
  I want to be able to add a Post authorisation user to an account
  So I can authorise I Record

  @authIrecord
  Scenario: Add a Post authorisation user

#Add Post Authorisation user
    Given I am on the Home Page
    When I click on settings
    And I select to amend transaction authorisation
    When I add a User Authorisation
    And I select options as below:
      | Options            | Value               |
      | Authorisation User | authorisation user  |
      | Authorisation Type | authorisation type1 |
      | Currencies         | all currencies      |
      | Threshold          | cash limit          |
    Then I can see the user added as an authoriser with a message displayed "authorisation added successfully"
    And I can successfully log out

#Create Order
    Given I am on the Home Page
    When I search for Vendor
    And I enter a "vendor name" on the search field and other fields
    Then I should be able to see "vendor name" and details of that specific Vendor
    When I select the "place order" button for the selected vendor
    And I fill the required fields as below: and click OK button
      | Field       | Value       |
      | Item Name   | item name   |
      | Order Price | order price |
      | Line GLCode | gl code     |
    And I select a "commodity code"
    And I select to split the order
    And I should fill up split line coding form
    And I should add line & remove line
    And I should add Comment
    And I enter the date and click place order button
    And I select Payment Card
    And I select Address
    And I click on the place order button
    Then I should be able to successfully place and get an order number
    When I click the order number
    Then I can get the order status as "Awaiting Vendor Confirmation"
    And I can successfully log out

    # Login Vendor to accept order
    When I log in with "vendor username" and "vendor password"
    And I search for the new order
    And I select the order
    Then I can see order status as "New Order"
    When I Accept order
    Then Order status will change to "Awaiting Delivery Confirmation"
    And I can successfully log out

    #Login as testingteam credential and pay for order
    When I am on the Home Page
    And I search for the new order
    And I select the order
    When I accept full order and receipt goods & authorise payment
    And I can successfully log out

  #Sign in as Post Authoriser and Authorise Order
    When I log in with "PostAuthoriser Username" and "PostAuthoriser Password"
    And I search for the new order
    And I click on Search Criteria
    And I select corporate user as All Users
    And I click on search button
    And I select the order
    And I click on Associated Records
    And I click on OK button
    And I click on the Irecord Transaction
    Then I can get the order status as "Awaiting Post Order Authorisation"
    And I click on Authorise button
    And I can successfully log out

    #Delete Order
    When I am on the Home Page
    And I click on settings
    And I select to amend transaction authorisation
    And I click on Delete button
    And I click on OK button
    Then I should be able to see the authorised user deleted with a message "authorisation deleted successfully"

  #Cancel Post Authorise order

  @cancelIrecord
  Scenario: Cancel a Post authorisation user
#Add Post Authorisation user
    Given I am on the Home Page
    When I click on settings
    And I select to amend transaction authorisation
    When I add a User Authorisation
    And I select options as below:
      | Options            | Value               |
      | Authorisation User | authorisation user  |
      | Authorisation Type | authorisation type1 |
      | Currencies         | all currencies      |
      | Threshold          | cash limit          |
    Then I can see the user added as an authoriser with a message displayed "authorisation added successfully"
    And I can successfully log out

#Create Order
    Given I am on the Home Page
    When I search for Vendor
    And I enter a "vendor name" on the search field and other fields
    Then I should be able to see "vendor name" and details of that specific Vendor
    When I select the "place order" button for the selected vendor
    And I fill the required fields as below: and click OK button
      | Field       | Value       |
      | Item Name   | item name   |
      | Order Price | order price |
      | Line GLCode | gl code     |
    And I select a "commodity code"
    And I select to split the order
    And I should fill up split line coding form
    And I should add line & remove line
    And I should add Comment
    And I enter the date and click place order button
    And I select Payment Card
    And I select Address
    And I click on the place order button
    Then I should be able to successfully place and get an order number
    When I click the order number
    Then I can get the order status as "Awaiting Vendor Confirmation"
    And I can successfully log out

    # Login Vendor to accept order
    When I log in with "vendor username" and "vendor password"
    And I search for the new order
    And I select the order
    Then I can see order status as "New Order"
    When I Accept order
    Then Order status will change to "Awaiting Delivery Confirmation"
    And I can successfully log out

    #Login as testingteam credential and pay for order
    When I am on the Home Page
    And I search for the new order
    And I select the order
    When I accept full order and receipt goods & authorise payment
    And I can successfully log out

  #Sign in as Post Authoriser and Cancel Order
    When I log in with "PostAuthoriser Username" and "PostAuthoriser Password"
    And I search for the new order
    And I click on Search Criteria
    And I select corporate user as All Users
    And I click on search button
    And I select the order
    And I click on Associated Records
    And I click on OK button
    And I click on the Irecord Transaction
    Then I can get the order status as "Awaiting Post Order Authorisation"
    And I click on Decline button
    Then I comment reason for decline and click reject order
    And I can successfully log out

    #Decline/Delete Order
    When I am on the Home Page
    And I click on settings
    And I select to amend transaction authorisation
    And I click on Delete button
    And I click on OK button
    Then I should be able to see the authorised user deleted with a message "authorisation deleted successfully"