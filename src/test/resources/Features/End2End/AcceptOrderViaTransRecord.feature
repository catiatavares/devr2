@regression
Feature: Creating and Accepting an Order Via Transaction Record
  As a user
  I want to create an Order(Client) and Accept an order(Vendor) via my Invapay Overview
  So I can manage my transactions

  @endToAwait
  Scenario: Create and Accept Order Via Invapay Overview
     #call add pCard Tests and pcard Assignment
    Given I am on the Home Page
    And I click on corporate settings
    And I call Add PCard lodge test
    And I click on submit type button
    And I click on settings
    And I click on P-Card Management - Lodge Cards Assignment
    And I tick a checkbox
    And I click on save button

    #create order test
    Given I click on home scheme
    When I search for Vendor
    And I enter a "vendor name" on the search field and other fields
    Then I should be able to see "vendor name" and details of that specific Vendor
    When I select the "place order" button for the selected vendor
    And I fill the required fields as below: and click OK button
      | Field       | Value       |
      | Item Name   | item name   |
      | Order Price | order price |
      | LedgerCode  | ledger code |
    And I enter the date and click place order button
    And I select Payment Card
    And I select Address
    Then I should be able to successfully place and get an order number
    When I click the order number
    Then I can get the order status as "Awaiting Vendor Confirmation"
    And I can successfully log out
    When I log in with "vendor username" and "vendor password"
    And I select awaiting transactions
    And I select the new order
    When I Accept the order
    Then I should be able to see the new order status of "Awaiting Delivery Confirmation"