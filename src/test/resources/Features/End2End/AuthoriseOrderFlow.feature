@regression
  @authdecflow
Feature: PRE-Authorise Order Flow
  As a corporate user
  I want to be able to add an authorisation user to an account
  So I can authorise transactions above certain limits

  Background:
    Given I am on the Home Page
    #call add pCard Tests and pcard Assignment
    And I click on corporate settings
    And I call Add PCard lodge test
    And I click on submit type button
    And I click on settings
    And I click on P-Card Management - Lodge Cards Assignment
    And I tick a checkbox
    And I click on save button

  @authorderflow
  Scenario: Add an authorisation user
#Add Authorisation user
    When I click on settings
    And I select to amend transaction authorisation
    When I add a User Authorisation
    And I select options as below:
      | Options            | Value              |
      | Authorisation User | authorisation user |
      | Authorisation Type | authorisation type |
      | Currencies         | all currencies     |
      | Threshold          | cash limit         |
    Then I can see the user added as an authoriser with a message displayed "authorisation added successfully"
    And I can successfully log out

#Create Order
    Given I am on the Home Page
    When I search for Vendor
    And I enter a "vendor name" on the search field and other fields
    Then I should be able to see "vendor name" and details of that specific Vendor
    When I select the "place order" button for the selected vendor
    And I fill the required fields as below: and click OK button
      | Field       | Value       |
      | Item Name   | item name   |
      | Order Price | order price |
      | Line GLCode | gl code     |
    And I select a "commodity code"
    And I select to split the order
    And I should fill up split line coding form
    And I should add line & remove line
    And I should add Comment
    And I enter the date and click place order button
    And I select Payment Card
    And I select Address
  #  And I click on the place order button #
    Then I should be able to successfully place and get an order number
    When I click the order number
    Then I can get the order status as "Awaiting Pre Order Authorisation"
    And I can successfully log out

#Sign in as Pre Authoriser and Authorise Order
    When I log in with "PreAuthoriser Username" and "PreAuthoriser Password"
    And I search for the new order
    And I select the order
    And I click on Update Subsidy changes
    And I click on Authorise button
    And I can successfully log out

    #Assertion for Validating the order has been preauthorised
    Given I am on the Home Page
    And I search for the new order
    And I select the order
    Then I can get the order status as "Awaiting Vendor Confirmation"
    And I can successfully log out

#Delete Authorisation user
    Given I am on the Home Page
    And I click on settings
    And I select to amend transaction authorisation
    When I select the delete the added authorisation user
    Then I should be able to see the authorised user deleted with a message "authorisation deleted successfully"

  @declineOrderFlow
  Scenario: Add an authorisation user
#Add Authorisation user
    When I click on settings
    And I select to amend transaction authorisation
    When I add a User Authorisation
    And I select options as below:
      | Options            | Value              |
      | Authorisation User | authorisation user |
      | Authorisation Type | authorisation type |
      | Currencies         | all currencies     |
      | Threshold          | cash limit         |
    Then I can see the user added as an authoriser with a message displayed "authorisation added successfully"
    And I can successfully log out

#Create Order
    Given I am on the Home Page
    When I search for Vendor
    And I enter a "vendor name" on the search field and other fields
    Then I should be able to see "vendor name" and details of that specific Vendor
    When I select the "place order" button for the selected vendor
    And I fill the required fields as below: and click OK button
      | Field       | Value       |
      | Item Name   | item name   |
      | Order Price | order price |
      | Line GLCode | gl code     |
    And I select a "commodity code"
    And I select to split the order
    And I should fill up split line coding form
    And I should add line & remove line
    And I should add Comment
    And I enter the date and click place order button
    And I select Payment Card
    And I select Address
   # And I click on the place order button #
    Then I should be able to successfully place and get an order number
    When I click the order number
    Then I can get the order status as "Awaiting Pre Order Authorisation"
    And I can successfully log out

#Sign in as Pre Authoriser and Decline Order
    When I log in with "PreAuthoriser Username" and "PreAuthoriser Password"
    And I search for the new order
    And I select the order
     # And I click on Update Subsidy changes
    And I click on Decline button
    Then I comment reason for decline and click reject order
    And I can successfully log out

    #Assertion for Validating the order has been declined
    Given I am on the Home Page
    And I search for the new order
    And I select the order
    Then I can get the order status as "Authorisation Declined"
    And I can successfully log out

#Delete Authorisation user
    Given I am on the Home Page
    And I click on settings
    And I select to amend transaction authorisation
    When I select the delete the added authorisation user
    Then I should be able to see the authorised user deleted with a message "authorisation deleted successfully"