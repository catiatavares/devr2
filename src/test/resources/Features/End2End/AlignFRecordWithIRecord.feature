@regression
Feature: F-records generated should be in the same currency as the associated I-Records generated
  As a (All) user (excluding Leonardo)
  I want to F-records generated should be in the same currency as the associated I-Records generated
  So I can manage my transactions

  @AlignFIRecord
  Scenario: Align F-record with I-record

    #create order test
    Given I am on the Home Page
    When I search for Vendor
    And I enter a "vendor name" on the search field and other fields
    Then I should be able to see "vendor name" and details of that specific Vendor
    When I select the "place order" button for the selected vendor
    And I fill the required fields as below: and click OK button
      | Field       | Value       |
      | Item Name   | item name   |
      | Order Price | order price |
      | LedgerCode  | ledger code |
    And I enter the date

  # add line in order
    And I add new line
      | Field       | Value       |
      | Item Name   | item name   |
      | Item Code   | item code   |
      | Order Price | order price |
    And I add GLCode
    And I enter the date in new line Auxiliary Details
    And I Change currency
    And I click on place order
    And I select Payment Card
    And I select Address
    Then I should be able to successfully place and get an order number
    When I click the order number
    Then I can get the order status as "Awaiting Vendor Confirmation"
    And I can successfully log out

  # Vendor
    When I log in with "vendor username" and "vendor password"
    And I search for the new order
    And I select the order
    Then I can see order status as "New Order"
    When I Accept order
    And I accept full order and dispatch good
    Then I can get the order status as "Awaiting Delivery Confirmation"
    And I can successfully log out

  # Buyer
    Given I am on the Home Page
    And I search for the new order
    And I select the order
    And I can get the order status as "Goods Receipting Required"
    And I accept first partial order and receipt goods & authorise payment
    When I accept second partial order and receipt goods & authorise payment
    And I can get the order status as "Archived"
    Then I verify I and F record
    And I can successfully log out





