@regression
@homepage
Feature: Repeat order and cancel order
  Automation IPAY-1487
  Call Create Order test
  Repeat Order (Update Custom Fields, Submit Changes)
  Open order to 'Cancel Order'

  @RepeatOrderAndCancelOrder
  Scenario: Repeat order and cancel order
    #call add pCard Tests and pcard Assignment
    Given I am on the Home Page
    And I click on corporate settings
    And I call Add PCard lodge test
    And I click on submit type button
    And I click on settings
    And I click on P-Card Management - Lodge Cards Assignment
    And I tick a checkbox
    And I click on save button

    #create order test
    Given I click on home scheme
    When I search for Vendor
    And I enter a "vendor name" on the search field and other fields
    Then I should be able to see "vendor name" and details of that specific Vendor
    When I select the "place order" button for the selected vendor
    And I fill the required fields as below: and click OK button
      | Field       | Value       |
      | Item Name   | item name   |
      | Order Price | order price |
      | Line GLCode | gl code     |
    And I select a "commodity code"
    And I select to split the order
    And I should fill up split line coding form
    And I should add line & remove line
    And I should add Comment
    And I enter the date and click place order button
    And I select Payment Card
    And I select Address
    And I click the order number
    And I place repeat order and submit changes
    When I cancel order
    And I click on transaction search and select Order Cancelled status
    Then I should successfully place an order with an order status of "Order Cancelled"
    And I can successfully log out

