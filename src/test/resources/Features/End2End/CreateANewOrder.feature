@regression
Feature: End To End User Journey - Create Order, Accept order and Authorise Payment
  As a user
  I want to create an Order(Client)
  Accept an order(Vendor)
  Receipt Goods and Authorise Payment
  So I can manage my transactions

  @End
  Scenario: Create an Order, Accept an order and Authorise Payment
    #Add message
    Given I am on the Home Page
    And I click on corporate settings
    And I click on Corporate Messages
    And Add Message and tick Corporate and Orders
    And I can successfully log out

    #call add pCard Tests and pcard Assignment
    Given I am on the Home Page
    And I click on corporate settings
    And I call Add PCard lodge test
    And I click on submit type button
    And I click on settings
    And I click on P-Card Management - Lodge Cards Assignment
    And I tick a checkbox
    And I click on save button

    #create order test
    Given I click on home scheme
    When I search for Vendor
    And I enter a "vendor name" on the search field and other fields
    Then I should be able to see "vendor name" and details of that specific Vendor
    When I select the "place order" button for the selected vendor
    And I fill the required fields as below: and click OK button
      | Field       | Value       |
      | Item Name   | item name   |
      | Order Price | order price |
      | LedgerCode  | ledger code |
    And I enter the date and click place order button
    And I select Payment Card
    And I select Address
    Then I should be able to successfully place and get an order number
    When I click the order number
    Then I can get the order status as "Awaiting Vendor Confirmation"
    And I can successfully log out

    When I log in with "vendor username" and "vendor password"
    And I can see message "Ade Testing" announcement box
    And I search for the new order
    And I select the order
    Then I can see order status as "New Order"
    When I Accept order
    Then Order status will change to "Awaiting Delivery Confirmation"
    And I can see message "Ade Testing" Transaction Footer
    And I can successfully log out


    When I login in with "client username" & "client password"
    And I search for the specific order
    And I select the specific order
    Then I can see the order status as "Goods Receipting Required"
    And I Receipt Goods and Authorise Payment
    Then the Order status will change to "Archived"
    And I can successfully log out
    And I can successfully log out




