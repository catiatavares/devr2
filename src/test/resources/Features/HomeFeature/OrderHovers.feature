@regression
@orderhover
Feature: Hover mouse on ORDER child elements
  As a user
  I want to hover mouse on Order Scheme
  So that I can navigate through the child elements

  Background:
    Given I am on the Home Page

  Scenario: Create A New Order
    When I hover mouse on Order scheme and click on Create a new order
    And I should see "Vendor Search" message

  Scenario: Create Order With Unregistered Vendor
    When I hover mouse on Order scheme and click on Create order with an unregistered vendor
    And I should see "Create an order with a new vendor" on screen

  Scenario: Request a New Quote
    When I hover mouse on Order scheme and click on Request a new quotation
    And I should see "Vendor Selection" page