@regression
Feature: Request a new Quotation
  As a user
  I want to be able to request a new quotation
  So that i can receive a purchase order

  @requestquote
  Scenario:
    Given I am on the Home Page
    When I click on Request a new Quotation link
    And I enter texts in the search criteria field
      | Fields     | Value      |
      | Name       | Ade Mumeen |
      | Keyword    | Ade        |
      | Area/State | London     |
      | City       | London     |
    And I select from the dropdown
      | MCC Code | AUTOMOBILE RENTAL AGENCY |
      | Country  | UNITED KINGDOM           |
    Then I click on search button
    And I click on view profile
    And I click on OK button
    And I click on select vendor
    Then I click on Add Product Items
    And I enter text in the fields below
      | Item Name | Bread      |
      | Quantity  | 10         |
      | UOM       | Board foot |
    And I add text in the comment text box
    And I click on Add comment
    Then  I click on request quote and assert text




