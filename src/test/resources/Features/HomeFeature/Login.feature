@regression
Feature: Testing Login requirement feature
  As a client buyer
  I want to login to the application
  So I can manage my user account

  @login
  Scenario: Login test for invabuyR2
    Given I am on the login page
    When I login in as a corporate buyer with "client username" & "client password"
    Then I should be able to see the "welcome message" text
    And the home page would contain "homePage url"
