@regression
Feature: Search Transaction
  As a user - client or Vendor
  I want to search for a specific transaction
  So I can manage these transactions

  @trans
  Scenario: Search for a transaction
    Given I log in as a vendor with "vendor username" and "vendor password"
    When I select the transaction search link
    And I enter the "transaction number" on the search field
    And I enter Date Type Corporate and Status
    Then I should be able to see the "transaction number" and the transaction details

#Create Custom Export File but unable to download because the file is not generated (TO DO)
  @CustomFile
  Scenario: Create Custom Export File
    Given I am on the Home Page
    When I select the transaction search link
    And I enter Transaction number
    And I click on search button
    And I click on Create Custom Export File
    And I enter File description and File Type
    And I click on OK button
    Then I can see a "Search Criteria:" message



