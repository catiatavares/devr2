@regression
Feature: Search Vendor
  As a client buyer
  I want to search for a specific vendor
  So I can manage my transactions

  @vendorsearch
  Scenario: Search for a Vendor
    Given I am on the Home Page
    When I search for Vendor
    And I enter a "vendor name" on the search field and other fields
