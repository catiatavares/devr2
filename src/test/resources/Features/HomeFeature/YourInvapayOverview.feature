@regression
@yio
Feature: Manage Awaiting Transaction
  As a user
  I should be able to use the hyperlinks
  So that I can complete purchase orders

  Background:
    Given I am on the Home Page

  Scenario: Recently rejected/declined transactions
    And I select the create a new order link
    And I enter a "vendor name" on the search field and other fields
    When I select the "place order" button for the selected vendor
    And I fill the required fields as below: and click OK button
      | Field       | Value       |
      | Item Name   | item name   |
      | Order Price | order price |
      | LedgerCode  | ledger code |
    And I enter the date and click place order button
    And I click on back button
    And I reject the order
    Then I click on home scheme
    And I click on Recently rejected & declined transactions
    And I click date to get latest order
    When I click the order
    Then I should see status as "Rejected By Buyer"

  Scenario: Archived transaction
    And I click on archived transaction
    When I click the order
    Then I should see status as "Archived"

  #@goodreceipt
    #This is a manual regression test - The link will not appear if there is no order to good receipt
  #Scenario: Good receipt transactions
 #And I click on transaction to goods receipt from Your Invapay Overview
   #When I click the order
    #Then I should see status as "Goods Receipting Required"

  #@draft
    #This is a manual regression test - The link will not appear if there is no order to in draft status
 #Scenario: Draft Transaction
    #And I click on draft transaction from Your Invapay Overview
    #When I click on the order

 #@awaitingaction
    #This is a manual regression test - The link will not appear if there is no order in awaiting action status
 #Scenario: Transaction awaiting your action
  #And I click on transaction awaiting your action
   #When I click the order

 #@inprogress
    #This is a manual regression test - The link will not appear if there is no order in progress status
  #Scenario: In progress transactions awaiting action from other users
   #And I click on in progress transactions awaiting action from other users
    #When I click the order

  #@awaitingauth
    #This is a manual regression test - The link will not appear if there is no order in awaiting authorisation status.
  #Scenario: Transactions awaiting your authorisation
  #And I call add authorisation user tests
   #And I click on transactions waiting your authorisation
    #When I click the order
    #Then I should see status as "Awaiting Pre Order Authorisation"

  #@tranerror
    #This is a manual regression test - the link will only appear when a payment didn't go through
  #Scenario: Transactions in error
    #And I click on transactions in error
    #When I click the order
    #Then I should see status as "Payment Issue"



