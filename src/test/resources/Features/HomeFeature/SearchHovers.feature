@regression
@search
Feature: Hover mouse on SEARCH child elements
  As a user
  I want to hover mouse on Search Scheme
  So that I can navigate through the child elements

  Background:
    Given I am on the Home Page

  #Scenario:Data File Search
   # When I hover mouse on Search scheme and click on Data File Search
    #And I should see "Export Files" msg on screen

  Scenario:Transaction Search
    When I hover mouse on Search scheme and click on Transaction Search
    And I should see "Search" text

  Scenario:Search child element hover
    When I hover mouse on Search scheme and click on Vendor Search
    And I should see "Vendor Search" page displayed