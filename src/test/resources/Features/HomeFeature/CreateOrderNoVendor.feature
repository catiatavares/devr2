@regression

@novendororder
Feature: Create order with an unregistered vendor
  As a user
  I want to create order with an unregistered vendor
  So that the new vendor receive purchase order

  @createnovendor
  Scenario:Create order with an unregistered vendor
    Given I am on the Home Page
    And I click on Create order with an unregistered vendor from Homepage
    Then I enter text in textfields
      | Field           | Value                  |
      | name Field      | McDonalds              |
      | add1 Field      | 23 test street         |
      | add2  Field     | bexley                 |
      | cty   Field     | london                 |
      | pcode  Field    | lo9 8hn                |
      | area  Field     | Manchester             |
      | telNum Field    | +442035698745          |
      | mobNum  Field   | 07865435678            |
      | faxNum  Field   | +442035698745          |
      | emailAdd Field  | ade.adekugbe@optal.com |
      | confEmail Field | ade.adekugbe@optal.com |
    And I select dropdown in form
    When I click on Create vendor and place order button
    And it should navigate to create new order and call create order tests

  @deletevendor
  Scenario: Loop and Click Delete Vendor Button
    Given I am on the Home Page
    And I hover mouse on Order scheme and click on Create order with an unregistered vendor
    And I loop into Delete Vendor Column to delete and click OK

  @placeorder
  Scenario: Click Place Order Button
    Given I am on the Home Page
    And I hover mouse on Order scheme and click on Create order with an unregistered vendor
    And I click on place order no vendor button
    And I should see "Create Order" screen


