@regression
@forgottenPasswordResetEmail
Feature: Forgotten your password
  As a user
  I should be able to use the forgotten lost password link
  So that I can create a new password

  @forgottenPasswordResetEmailEnterUserName
  Scenario: Forgotten your password
    Given I am on the Landing Page
    And I click on Forgotten your password link
    When I enter username on the search field
    And I click on submit type button
    Then I should see password reset request sent to the registered email address massage

  @forgottenPasswordResetEmailOpenEmail
  Scenario:  Open email account
    Given I navigate to "https://www.gmail.com" url
    And I sign with valid credential
    When I click on "Invapay - Password Reset Request" email
    Then I validate email logo and header content
    And I validate email body content
    And I validate email footer content
    And I completed rest user journey through link
    And I validate that link not work again
    And I delete all email

  @forgottenPasswordResetEmailLoginWithNewPassword
  Scenario: Login With New Password
    Given I am on the Landing Page
    When I login with new password
    Then I verify my account