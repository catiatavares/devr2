@regression
@AssignAccountingCodes
Feature:Assign Accounting Codes

  Background:
    Given I am on the Home Page
    And I click on settings
    When I click on Amend Accounting Codes

  @BudgetCode
  Scenario:Assign Accounting Codes for budget code
    And I click on budget code
    And I click on double forward arrow and save
    And I click on double backward arrow and save
    And I click on forward arrow and save for budget code
    Then I should be able to see "Accounting Codes updated successfully"

  @CostCentre
  Scenario:Assign Accounting Codes for cost centre
    And I click on cost centre
    And I click on double forward arrow and save
    And I click on double backward arrow and save
    And I click on forward arrow and save for Cost Centre
    Then I should be able to see "Accounting Codes updated successfully"

  @GLCode
  Scenario:Assign Accounting^I click on  forward arrow and save$ Codes for GL code
    And I click on GL Code
    And I click on double forward arrow and save
    And I click on double backward arrow and save
    And I click on forward arrow and save for Gl Code
    Then I should be able to see "Accounting Codes updated successfully"
