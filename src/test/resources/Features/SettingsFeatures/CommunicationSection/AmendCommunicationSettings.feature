@Regression
Feature:Amend Communication settings
  As a User
  I want to be able to amend my communication settings
  So that I can update my profile

  @Amend
  Scenario:Amend communication settings
    Given I am on the Home Page
    And I click on settings
    And I select Amend Communication settings link
    When I deselect any tick boxes
    And I click on save button
    Then A successful updated message is displayed
