@regression
Feature: Assign & UnAssign User Permissions
  As a user
  I want to be able to Assign/UnAssign Permission for user

  @UserPermissions
  Scenario: Assign & UnAssign User Permissions
    Given I am on the Home Page
    And I click on settings
    And I click on assign user permissions
    When I am able to unassigned administration permission and save
    Then I should see information massage "Settings saved successfully"
    And I am able to move all available roles permission and save
    Then I should see information massage "Settings saved successfully"
    And I am able to unassigned all permission and save
    Then I should see information massage "Settings saved successfully"
    And I am able to assign administrator permission
    Then I should see information massage "Settings saved successfully"

