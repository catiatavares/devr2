@regression
@authsummary
Feature: Authorisation Summary (Another user)
  As a user
  I want to assign and unassign authorisation
  So that I can manage Pre Authorisation and Post Authorisation

  Background:
    Given I am on the Home Page
    And I click on corporate settings
    When I select Modify Current user link
    And I enter user detail in the search fields
      | Key       | Value     |
      | firstname | ade       |
      | lastname  | adekugbe2 |
    And I click on search button
    And I click the Edit button
    And I click amend transaction authorisation link

  @assign
  Scenario: Assign and unassign authorisation user
    Then I call add authorisation user tests
    When I click on settings
    And I click on Authorisation Summary link
    And I click on Select All button and should see bottons checked
    Given I click on UnSelectAll button and should see bottons unchecked
    And I click on Unassign All Pre Authorised Users
    And I click Ok button
    And I click on Unassign All Post Authorised Users
    And I click Ok button

  @reassign
  Scenario: Reassign authorisation user
    When I click on settings
    And I click on Authorisation Summary link
    And I tick a checkbox
    And I click on Reassign Selected Users button
    And I select new authorisation user as below:
      | Options            | Value     |
      | Authorisation User | auth user |
    And I check if the default checkbox is ticked
    And I click on save button

