@regression
Feature: Adding and Deleting Authorisation User
  As a client buyer
  I want to add/delete an authorisation user to an account
  So I can have authorisation for specific transactions above certain limits

  Background:
    Given I am on the Home Page
    And I click on settings
    And I select to amend transaction authorisation

  @addauthuser
  Scenario: Add Authorisation user
    When I add a User Authorisation
    And I select options as below:
      | Options            | Value              |
      | Authorisation User | authorisation user |
      | Authorisation Type | authorisation type |
      | Currencies         | all currencies     |
      | Threshold          | cash limit         |
    Then I can see the user added as an authoriser with a message displayed "authorisation added successfully"

  @edituser
  Scenario: Edit Authorisation user
    And I click edit
    And I edit threshold value as below: and click save
      | Options      | Value      |
      | new authUser | auth user2 |
    Then I can see the user edited message displayed "authorisation updated successfully"

  @ignore
  Scenario: Delete Authorisation user
    When I select the delete the added authorisation user
    Then I should be able to see the authorised user deleted with a message "authorisation deleted successfully"
