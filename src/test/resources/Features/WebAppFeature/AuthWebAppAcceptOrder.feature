@regression
@webAppAcceptOrder

Feature: Auth WebApp Accept Order

  @addPCardAcceptFromWebApp
  Scenario: add pCard and pcard Assignment
    Given I am on the Home Page
    And I click on corporate settings
    And I call Add PCard lodge test
    And I click on submit type button
    And I click on settings
    And I click on P-Card Management - Lodge Cards Assignment
    And I tick a checkbox
    And I click on save button

  @addingPreAuthorisationUserAcceptFromWebApp
  Scenario: Add an authorisation user
#Add Authorisation user
    Given I am on the Home Page
    When I click on settings
    And I select to amend transaction authorisation
    When I add a User Authorisation
    And I select options as below:
      | Options            | Value              |
      | Authorisation User | authorisation user |
      | Authorisation Type | authorisation type |
      | Currencies         | all currencies     |
      | Threshold          | cash limit         |
    Then I can see the user added as an authoriser with a message displayed "authorisation added successfully"
    And I can successfully log out

  @createOrderAcceptFromWebApp
    Scenario: create order for web app
    Given I am on the Home Page
    When I search for Vendor
    And I enter a "vendor name" on the search field and other fields
    Then I should be able to see "vendor name" and details of that specific Vendor
    When I select the "place order" button for the selected vendor
    And I fill the required fields as below: and click OK button
      | Field       | Value       |
      | Item Name   | item name   |
      | Order Price | order price |
      | Line GLCode | gl code     |
    And I select a "commodity code"
    And I select to split the order
    And I should fill up split line coding form
    And I should add line & remove line
    And I should add Comment
    And I enter the date and click place order button
    And I select Payment Card
    And I select Address
    Then I should be able to successfully place and get an order number
    When I click the order number
    Then I can get the order status as "Awaiting Pre Order Authorisation"
    And I can successfully log out

#AUTH WEBAPP
  @authWebAppAcceptFromWebApp
  Scenario: Add an authorisation user
    Given I navigate to "https://demoauth.invapay.com" url
    And I sign in as Authoriser with
    And I select pins
    And I click on submit type button
    And I should be able to see "Invapay Authorisations" text
    And I change emails settings
    And I accept order
    And I log out

  @verifyStatusAcceptFromWebApp
  Scenario: verify order status
    Given I am on the Home Page
    When I search for the new order and select the order
    Then I can get order status as "Awaiting Vendor Confirmation"

  #Delete Authorisation user
  @deletePreAuthorisationAcceptFromWebApp
  Scenario: Deleted authorisation user
    Given I am on the Home Page
    And I click on settings
    And I select to amend transaction authorisation
    When I select the delete the added authorisation user
    Then I should be able to see the authorised user deleted with a message "authorisation deleted successfully"
    And I can successfully log out