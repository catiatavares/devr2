package com.optal.CustomerSupportStep.TicketSection;

import com.optal.CustomerSupportPage.TicketSection.RaiseASupportTicketPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class RaiseASupportTicketStep {

    private WorldHelper helper;
    private RaiseASupportTicketPage raiseASupportTicketPage;

    public RaiseASupportTicketStep(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I click on Raise a Support Ticket$")
    public void iClickOnRaiseASupportTicket() throws Throwable {
        raiseASupportTicketPage=helper.getRaiseASupportTicketPage().clickRaiseASupportLink();
    }

    @When("^I enter Subject & Message in subject field in fields provided$")
    public void iEnterSubjectInSubjectField() throws Throwable {
        raiseASupportTicketPage.fillTicketDetails("AUTOMATION TESTING", "Running automation tests");
    }

    @When("^I tick checkbox and click on submit ticket button$")
    public void iTickCheckbox() throws Throwable {
        raiseASupportTicketPage.tickCheckboxSumbit();
    }

    @Then("^I can see \"([^\"]*)\" displayed on ticket screen")
    public void iCanSeeTicketRaisedConfirmation(String ticketRaised) throws Throwable {
        Assert.assertEquals(ticketRaised, raiseASupportTicketPage.assertMessage());
    }
}
