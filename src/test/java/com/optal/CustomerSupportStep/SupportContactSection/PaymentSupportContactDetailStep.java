package com.optal.CustomerSupportStep.SupportContactSection;

import com.optal.CustomerSupportPage.SupportContactSection.PaymentSupportContactDetailPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class PaymentSupportContactDetailStep {
    private WorldHelper helper;
    private PaymentSupportContactDetailPage paymentSupportContactDetailPage;

    public PaymentSupportContactDetailStep(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I click on Payment Support Contact Details page$")
    public void iClickOnPaymentSupportContactDetailsPage() throws Throwable {
        paymentSupportContactDetailPage = helper.getPaymentSupportContactDetailPage().clickSupportLink();
    }

    @Then("^I can see the \"([^\"]*)\" display$")
    public void iCanSeeTheContactDetailDisplay(String pay) throws Throwable {
        Assert.assertEquals("Error message: invalid text", pay, paymentSupportContactDetailPage.verifyText());
    }
}

