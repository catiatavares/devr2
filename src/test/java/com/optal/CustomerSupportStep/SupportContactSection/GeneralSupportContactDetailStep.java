package com.optal.CustomerSupportStep.SupportContactSection;

import com.optal.CustomerSupportPage.SupportContactSection.GeneralSupportContactDetailPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class GeneralSupportContactDetailStep {

        private WorldHelper helper;
        private GeneralSupportContactDetailPage generalSupportContactDetailPage;

        public GeneralSupportContactDetailStep(WorldHelper helper) { this.helper = helper;
        }

        @When("^I click on General Support Contact Details page$")
        public void iClickOnGeneralSupportContactDetailsPage() throws Throwable {
            generalSupportContactDetailPage = helper.getGeneralSupportContactDetailPage().clickGeneralLink();
        }

        @Then("^I can view \"([^\"]*)\" displayed$")
        public void iCanViewGeneralContactDetailDisplayed(String exp) throws Throwable {
            Assert.assertEquals("Error message: invalid text", exp, generalSupportContactDetailPage.verityText());
        }
    }


