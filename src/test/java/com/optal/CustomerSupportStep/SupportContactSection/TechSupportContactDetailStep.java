package com.optal.CustomerSupportStep.SupportContactSection;

import com.optal.CustomerSupportPage.SupportContactSection.TechSupportContactDetailPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;


public class TechSupportContactDetailStep {

    private WorldHelper helper;
    private TechSupportContactDetailPage techSupportContactDetailPage;

    public TechSupportContactDetailStep(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I click on Technical Support Contact Details page$")
    public void iClickOnTechnicalSupportContactDetailsPage() throws Throwable {
        techSupportContactDetailPage= helper.getTechSupportContactDetailPage().clickSupportLink();
    }

    @Then("^I can see it \"([^\"]*)\" displayed$")
    public void iCanSeeItContactDetailsDisplayed(String tech) throws Throwable {
        Assert.assertEquals("Error",tech,techSupportContactDetailPage.verifyTitle());
    }
}