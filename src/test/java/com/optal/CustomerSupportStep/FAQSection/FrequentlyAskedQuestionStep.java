package com.optal.CustomerSupportStep.FAQSection;

import com.optal.CustomerSupportPage.FAQSection.FrequentlyAskedQuestionPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class FrequentlyAskedQuestionStep {

    private WorldHelper helper;
    private FrequentlyAskedQuestionPage frequentlyAskedQuestionPage;

    public FrequentlyAskedQuestionStep(WorldHelper helper) {
        this.helper = helper;
    }


    @When("^I click on FAQ page$")
    public void iClickOnFaqPage() throws Throwable {
        frequentlyAskedQuestionPage = helper.getFrequentlyAskedQuestionPage().iClickOnFaqLink();
    }

    @Then("^I can see the frequently asked questions of Payment Process section$")
    public void iCanSeeTheFrequentlyAskedQuestionsOfPaymentProcessAnd() throws Throwable {
        frequentlyAskedQuestionPage.howItWorksSection();
//        Assert.assertEquals("Error msg: Text not found", paymentProcess, frequentlyAskedQuestionPage.howItWorksSection());
//        Assert.assertTrue(frequentlyAskedQuestionPage.howItWorksSection(paymentProcess));
//        Assert.assertTrue(frequentlyAskedQuestionPage.howItWorksSection(editText));

        frequentlyAskedQuestionPage.howDoIAcceptOrderSection();
        frequentlyAskedQuestionPage.howLongPaymentTakesSection();
        frequentlyAskedQuestionPage.whereFindDatePaymentAuthorisedSection();
    }

    @Then("^I can see the frequently asked questions of Registration section$")
    public void ICanSeeTheFrequentlyAskedQuestionsOfRegistration() throws Throwable {
        frequentlyAskedQuestionPage.systemDoesNotAcceptTaxSection();
        frequentlyAskedQuestionPage.iDoNotPayVatTaxSection();
        frequentlyAskedQuestionPage.whatIsVendorCatCodeSection();

    }

    @Then("^I can see the frequently asked questions of Payment Status section$")
    public void IcanSeeTheFrequentlyAskedQuestionsOfPaymentStatusSection() throws Throwable {
        frequentlyAskedQuestionPage.whatDoesNewOrderStatusMeanSection();
        frequentlyAskedQuestionPage.whatDoesAwaitingDelConfStatusMeanSection();
        frequentlyAskedQuestionPage.whatDoesPaymentAprovedMeanSection();
        frequentlyAskedQuestionPage.whatDoesAwaitingBuyerConfStatusMeanSection();
    }

    @Then("^I can see the frequently asked questions of Other section$")
    public void IcanSeeTheFrequentlyAskedQuestionsOfOtherSection() throws Throwable {
        frequentlyAskedQuestionPage.whyPayingfeeActualSection();
        frequentlyAskedQuestionPage.whoIsInvapaySection();
    }
}
