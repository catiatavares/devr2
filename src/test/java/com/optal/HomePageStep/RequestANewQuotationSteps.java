package com.optal.HomePageStep;

import com.optal.HomePage.RequestANewQuotationpage;
import com.optal.support.WorldHelper;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

public class RequestANewQuotationSteps {

    private WorldHelper helper;
    private RequestANewQuotationpage requestANewQuotationpage;

    public RequestANewQuotationSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I click on Request a new Quotation link$")
    public void iClickOnRequestANewQuotationLink() throws Throwable {
        requestANewQuotationpage = helper.getRequestANewQuotationPage().clickRequestQuote();
    }

    @When("^I enter texts in the search criteria field$")
    public void iEnterTextsInTheSearchCriteriaField(DataTable vendorSearchCriteria) throws Throwable {
        System.out.println(vendorSearchCriteria);
        List<List<String>> formObjects = vendorSearchCriteria.raw();
        String nameSearch = formObjects.get(1).get(1);
        String keyword = formObjects.get(2).get(1);
        String area = formObjects.get(3).get(1);
        String city = formObjects.get(4).get(1);
        requestANewQuotationpage.vendorSelectionForm(nameSearch, keyword, area, city);
    }

    @When("^I select from the dropdown$")
    public void iSelectFromTheDropdown(DataTable vendorSearchForm) {
        List<List<String>> formObjects = vendorSearchForm.raw();
        String mcCode = formObjects.get(0).get(1);
        String contryy = formObjects.get(1).get(1);
        requestANewQuotationpage.selectDropdowndetails(mcCode, contryy);
    }

    @Then("^I click on view profile$")
    public void iClickOnViewProfile() throws Throwable {
        requestANewQuotationpage.clickViewProfile();
    }

    @Then("^I click on select vendor$")
    public void iClickOnSelectVendor() throws Throwable {
        requestANewQuotationpage.clickOnSelectVendor();
    }

    @Then("^I click on Add Product Items$")
    public void iClickOnAddProductItems() throws Throwable {
        requestANewQuotationpage.clickAddProdItems();
    }

    @Then("^I enter text in the fields below$")
    public void iEnterTextInTheFieldsBelow(DataTable addProductItemsForms) throws Throwable {
        List<List<String>> formObjects = addProductItemsForms.raw();
        String itemNamBox = formObjects.get(0).get(1);
        String quantBox = formObjects.get(1).get(1);
        String uomBox = formObjects.get(2).get(1);
        requestANewQuotationpage.addProductItemsForm(itemNamBox, quantBox, uomBox);
    }

    @Then("^I add text in the comment text box$")
    public void iAddTextInTheTextBox() throws Throwable {
        requestANewQuotationpage.commentTextBox("Bread");
    }

    @Then("^I click on Add comment$")
    public void iClickOnAddComment() throws Throwable {
        requestANewQuotationpage.clickAddComment();
    }

    @Then("^I click on request quote and assert text$")
    public void iClickOnRequestQuote() throws Throwable {
        requestANewQuotationpage.clickQuoteRequest();
    }
}
