package com.optal.HomePageStep;

import com.optal.HomePage.HomePage;
import com.optal.HomePage.VendorSearchPage;
import com.optal.support.WorldHelper;
import com.optal.utilities.TestData;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class VendorSearchSteps {

    private WorldHelper helper;
    private HomePage homePage;
    private VendorSearchPage vendorSearchPage;

    public VendorSearchSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @Given("^I am on the Home Page$")
    public void iAmOnTheHomePage() throws Throwable {
        String username = TestData.getValue("client username");
        String password = TestData.getValue("client password");
        homePage = helper.getBasePage().loginWith(username, password);
    }

    @When("^I search for Vendor$")
    public void iSearchForVendor() throws Throwable {
        vendorSearchPage = helper.getAccountHomePage().goToVendorSearchPage();
    }

        @And("^I enter a \"([^\"]*)\" on the search field and other fields$")
    public void iEnterAOnTheSearchFieldAndOtherFields(String vendor) throws Throwable {
        vendor = TestData.getValue(vendor);
        vendorSearchPage = helper.getVendorSearchPage().searchForVendor(vendor);
    }

    @Then("^I should be able to see \"([^\"]*)\" and details of that specific Vendor$")
    public void iShouldBeAbleToSeeAndDetailsOfThatSpecificVendor(String vendorName) throws Throwable {
        vendorName = TestData.getValue(vendorName);
        Assert.assertTrue(vendorSearchPage.validateSearch(vendorName));
    }
}
