package com.optal.HomePageStep;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.HomePage.HomePage;
import com.optal.HomePage.TransactionSearchPage;
import com.optal.support.WorldHelper;
import com.optal.utilities.TestData;
import com.optal.waits.WebWaits;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class TransactionSearchSteps {


    private WorldHelper helper;
    private HomePage homePage;
    private TransactionSearchPage transactionSearchPage;
    private CommonObjectPage commonObjectPage;

    public TransactionSearchSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @Given("^I log in as a vendor with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iLogInAsAVendorWithAnd(String vendorUsername, String vendorPassword) throws Throwable {
        vendorUsername = TestData.getValue(vendorUsername);
        vendorPassword = TestData.getValue(vendorPassword);
        homePage = helper.getBasePage().loginWith(vendorUsername, vendorPassword);
    }

    @When("^I select the transaction search link$")
    public void iSelectTheTransactionSearchLink() throws Throwable {
        transactionSearchPage = helper.getAccountHomePage().goToTransactionSearchPage();
    }

    @And("^I enter the \"([^\"]*)\" on the search field$")
    public void iEnterTheOnTheSearchField(String transactionId) throws Throwable {
        transactionId = TestData.getValue(transactionId);
        transactionSearchPage.searchForTransaction(transactionId);

    }

    @And("^I enter Date Type Corporate and Status$")
    public void iEnterDateTypeCorporateAndStatus() throws Throwable {
        transactionSearchPage.otherFields();
        commonObjectPage = helper.getCommonObjectPage().clickSearchButton();

    }

    @Then("^I should be able to see the \"([^\"]*)\" and the transaction details$")
    public void iShouldBeAbleToSeeTheAndTheTransactionDetails(String transaction) throws Throwable {
        transaction = TestData.getValue(transaction);
        Assert.assertTrue(transactionSearchPage.validateTransactionSearch(transaction));
    }

    @And("^I enter Transaction number$")
    public void iEnterTransactionNumber() throws Throwable {
        transactionSearchPage = helper.getTransactionSearchPage().transactionNumber();

    }

    @And("^I click on Create Custom Export File$")
    public void iClickOnCreateCustomExportFile() throws Throwable {
        transactionSearchPage = helper.getTransactionSearchPage().customExport();
    }


    @And("^I enter File description and File Type$")
    public void iEnterFileDescriptionAndFileType() throws Throwable {
        transactionSearchPage = helper.getTransactionSearchPage().exportDescription();

    }

    @Then("^I can see a \"([^\"]*)\" message$")
    public void iCanSeeAdisplayedMessage(String custom) throws Throwable {
        WebWaits.waitForNoOfSeconds(5);
        Assert.assertEquals("Error", custom, transactionSearchPage.actualResultCustom());
    }
}
