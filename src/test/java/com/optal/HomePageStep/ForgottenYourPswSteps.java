package com.optal.HomePageStep;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.HomePage.ForgottenYourPswPage;
import com.optal.support.WorldHelper;
import com.optal.utilities.TestData;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class ForgottenYourPswSteps {

    private WorldHelper helper;
    private ForgottenYourPswPage forgottenYourPswPage;
    private CommonObjectPage commonObjectPage;

    public ForgottenYourPswSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @Given("^I am on the Landing Page$")
    public void iAmOnTheLandingPage() throws Throwable {
        forgottenYourPswPage = helper.getForgottenYourPswPage().landingPage();
    }

    @Given("^I click on Forgotten your password link$")
    public void iClickOnForgottenYourPasswordLink() throws Throwable {
        forgottenYourPswPage = helper.getForgottenYourPswPage().clickForgottenYourPsw();
    }


//    @Given("^I enter \"([^\"]*)\" on the search field$")
//    public void iEnterUsername(String buyerUsername) throws Throwable {
//        buyerUsername = TestData.getValue(buyerUsername);
//        forgottenYourPswPage.enterUsername(buyerUsername);
//    }

    @Given("^I enter username on the search field$")
    public void iEnterUsername() throws Throwable {
        forgottenYourPswPage.enterUsername();
    }

    /*
        @Then("I should see \"([^\"]*)\" displayed")
        public void iShouldSeeConfirmationText(String validateTextDisplayed) {
            Assert.assertTrue(forgottenYourPswPage.validateEmailSentMsg(validateTextDisplayed));
        }
     */
    @Then("I should see password reset request sent to the registered email address massage$")
    public void iShouldSeePasswordResetRequestSentToTheRegisteredEmailAddressMassage() {
        forgottenYourPswPage.validateInformationEmailSentMassage();
    }

    @Given("^I sign with valid credential$")
    public void iSignWithValidCredential() throws Throwable {
        forgottenYourPswPage = helper.getForgottenYourPswPage().loginGmailAccount();
    }

    @Given("^I click on \"([^\"]*)\" email$")
    public void iClickOnEmail(String emailSubject) throws Throwable {
        forgottenYourPswPage.clickEmail(emailSubject);
    }

    @Given("^I validate email logo and header content$")
    public void iValidateEmailRLogoAndHeaderContent() throws Throwable {
        forgottenYourPswPage.validateR2Logo();
        forgottenYourPswPage.validateHeader();
    }

    @Given("^I validate email body content$")
    public void iValidateEmailBodyContent() throws Throwable {
        forgottenYourPswPage.validateBody();
    }

    @Given("^I validate email footer content$")
    public void iValidateEmailFooterContent() throws Throwable {
        forgottenYourPswPage.validateEmailFooter();
    }


    @And("^I completed rest user journey through link$")
    public void iCompletedRestUserJourneyThroughLink() throws Throwable {
        forgottenYourPswPage.clickOnLinkWithToken();
    }

    @And("^I validate that link not work again$")
    public void iValidateThatLinkNotWorkAgain() throws Throwable {
        forgottenYourPswPage = helper.getForgottenYourPswPage().clickLinkAgain();
    }

    @And("^I delete all email$")
    public void iDeleteAllEmail() throws Throwable {
        forgottenYourPswPage = helper.getForgottenYourPswPage().deleteAllEmail();
    }

    @And("^I login with new password$")
    public void iLoginWithNewPassword() throws Throwable {
        forgottenYourPswPage = helper.getForgottenYourPswPage().loginWithNewPassword();
    }

    @And("^I verify my account$")
    public void iVerifyMyAccount() throws Throwable {
        forgottenYourPswPage = helper.getForgottenYourPswPage().verifyMyAccount();
    }
}

