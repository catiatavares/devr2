package com.optal.HomePageStep;

import com.optal.HomePage.HoverPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import static junit.framework.TestCase.assertTrue;

public class HoverSteps {

    private WorldHelper helper;
    private HoverPage hoverPage;

    public HoverSteps(WorldHelper helper) {
        this.helper = helper;
    }

    //Transaction Search
    @Then("^I hover mouse on Search scheme and click on Transaction Search$")
    public void iHoverMouseonSearchSchemeAndClickOnTransactionSearch() throws Throwable {
        hoverPage = helper.getHoverPage().transactionSearchHover();
    }

    @Then("^I should see \"([^\"]*)\" text")
    public void iShouldSeeSearchText(String searchDisplayed) throws Throwable {
        assertTrue(hoverPage.validateTranSearchHover(searchDisplayed));
    }

    //Data File Search
    @Then("^I hover mouse on Search scheme and click on Data File Search$")
    public void iHoverMouseOnSearchSchemeAndClickOnDataFileSearch() throws Throwable {
        hoverPage = helper.getHoverPage().DataFileSearchHover();
    }

    @And("^I should see \"([^\"]*)\" msg on screen$")
    public void iShouldSeeExportFilesMsg(String exportFileDisplayed) throws Throwable {
        Assert.assertTrue(hoverPage.validateDataFileSearch(exportFileDisplayed));
    }

    //Vendor search
    @When("^I hover mouse on Search scheme and click on Vendor Search$")
    public void iHoverMouseOnSearchSchemeAndClickOnVendorSearch() throws Throwable {
        hoverPage = helper.getHoverPage().VendorSearchHover();
    }

    @And("^I should see \"([^\"]*)\" page displayed$")
    public void iShouldSeeVendorSearchPageDisplayed(String vendorSearch) throws Throwable {
        Assert.assertTrue(hoverPage.validateVendorSearch(vendorSearch));
    }

    //Create a new order
    @Then("^I hover mouse on Order scheme and click on Create a new order$")
    public void iHoverMouseOnOrderSchemeAndClickOnCreateANewOrder() throws Throwable {
        hoverPage = helper.getHoverPage().createANewOrderHover();
    }

    @Then("^I should see \"([^\"]*)\" message")
    public void iShouldSeeVendorSearchMessage(String vendorSearchDisplayed) throws Throwable {
        Assert.assertTrue(hoverPage.validateCreateOrderText(vendorSearchDisplayed));
    }

    //Request a new quotation
    @Then("^I hover mouse on Order scheme and click on Request a new quotation$")
    public void iHoverMouseOnOrderSchemeAndClickOnRequestANewQuotation() throws Throwable {
        hoverPage = helper.getHoverPage().requestNewQuoteHover();
    }

    @And("^I should see \"([^\"]*)\" page$")
    public void iShouldSeeVendorSelectionPage(String vendorSelectionTextDisplayed) throws Throwable {
        Assert.assertTrue(hoverPage.validateRequestNewQuoteText(vendorSelectionTextDisplayed));
    }

    //Create an order with unregistered vendor
    @Then("^I hover mouse on Order scheme and click on Create order with an unregistered vendor$")
    public void iHoverMouseOnOrderSchemeAndClickOnCreateOrderWithAnUnregisteredVendor() throws Throwable {
        hoverPage = helper.getHoverPage().createOrderUnregVendorHover();
    }

    @Then("^I should see \"([^\"]*)\" on screen$")
    public void iShouldSeeCreateAnOrderWithANewVendorOnScreen(String noVendorDisplayed) throws Throwable {
      //  Assert.assertTrue(hoverPage.validateOrderNoVendor(noVendorDisplayed));
        hoverPage.assertMessage();
    }
}
