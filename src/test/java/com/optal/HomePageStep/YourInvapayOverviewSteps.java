package com.optal.HomePageStep;

import com.optal.End2EndPages.CreateANewOrderPage;
import com.optal.HomePage.YourInvapayOverviewPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class YourInvapayOverviewSteps {

    private WorldHelper helper;
    private YourInvapayOverviewPage yourInvapayOverviewPage;
    private CreateANewOrderPage createANewOrderPage;

    public YourInvapayOverviewSteps(WorldHelper helper) {
        this.helper = helper;
    }

    //Transaction to Goods Receipts
    @Given("^I click on transaction to goods receipt from Your Invapay Overview$")
    public void iClickOnTransactionToGoodsRecieptFromYourInvapayOverview() throws Throwable {
        yourInvapayOverviewPage = helper.getYourInvapayOverview().setTransGoodReceipt();
    }

    @Given("^I click the order$")
    public void iClickTheOrder() throws Throwable {
        yourInvapayOverviewPage.setclickOrder();
    }

    @Then("^I should see status as \"([^\"]*)\"$")
    public void iShouldSeeStatusAs(String checkStatus) throws Throwable {
        createANewOrderPage = helper.getCreateANewOrderPage();
        Assert.assertTrue(createANewOrderPage.validateOrderStatus(checkStatus));
    }

    //Draft Transaction
    @Given("^I click on draft transaction from Your Invapay Overview$")
    public void iClickOnDraftTransactionFromYourInvapayOverview() throws Throwable {
        yourInvapayOverviewPage = helper.getYourInvapayOverview().setDraftTransaction();
    }

    @Given("^I click on the order$")
    public void iClickOnOrder() throws Throwable {
        yourInvapayOverviewPage = helper.getYourInvapayOverview().setclickOrder();
    }

    //Transaction Awaiting Your Action
    @Given("^I click on transaction awaiting your action$")
    public void iClickOnTransactionAwaitingYourAction() throws Throwable {
        yourInvapayOverviewPage = helper.getYourInvapayOverview().setAwaitingYourAction();
    }

    //In Progress Transactions Awaiting Action From Other Users
    @Given("^I click on in progress transactions awaiting action from other users$")
    public void iClickOnInProgressTransactionAwaitingActionFromOtherUsers() throws Throwable {
        yourInvapayOverviewPage = helper.getYourInvapayOverview().setInProgressTran();
    }

    //Transaction Awaiting Your Authorisation
    @Given("^I click on transactions waiting your authorisation$")
    public void iClickOntransactionsAwaitingYourauthorisation() throws Throwable {
        yourInvapayOverviewPage = helper.getYourInvapayOverview().setTransAwaitingAuth();
    }

    //Recently Archived Transaction
    @And("^I click on archived transaction$")
    public void iClickOnArchivedTransaction() throws Throwable {
        yourInvapayOverviewPage = helper.getYourInvapayOverview().setArchivedTransaction();
    }

    //Transaction Error
    @And("^I click on transactions in error$")
    public void iClickOnTransactionInError() throws Throwable {
        yourInvapayOverviewPage = helper.getYourInvapayOverview().setInErrorTransaction();
    }

    //Rejected and Declined Transaction
    @And("^I click on Recently rejected & declined transactions$")
    public void iClickOnRecentlyRejectedDeclinedTransactions() throws Throwable {
        yourInvapayOverviewPage = helper.getYourInvapayOverview().setRejectedDeclinedTrans();
    }

    @And("^I reject the order$")
    public void irejectTheOrder() throws Throwable {
        yourInvapayOverviewPage = helper.getYourInvapayOverview().rejectOrder();
    }

    @And("^I click date to get latest order$")
    public void iclickDateToGetLatestOrder() throws Throwable {
        yourInvapayOverviewPage = helper.getYourInvapayOverview().getLatestDate();
    }


}
