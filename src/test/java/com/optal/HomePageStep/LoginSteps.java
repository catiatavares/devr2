package com.optal.HomePageStep;

import com.optal.HomePage.BasePage;
import com.optal.HomePage.HomePage;
import com.optal.support.WorldHelper;
import com.optal.utilities.TestData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class LoginSteps {

    private WorldHelper helper;
    private BasePage basePage;
    private HomePage homePage;

    public LoginSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @Given("^I am on the login page$")
    public void iAmOnTheLoginPage() throws Throwable {
        basePage = helper.getBasePage();
    }

    @When("^I login in as a corporate buyer with \"([^\"]*)\" & \"([^\"]*)\"$")
    public void iLoginInAsACorporateBuyerWith(String username, String password) throws Throwable {
        username = TestData.getValue(username);
        password = TestData.getValue(password);
        homePage = basePage.loginWith(username, password);
    }

    @Then("^I should be able to see the \"([^\"]*)\" text$")
    public void iShouldBeAbleToSeeTheText(String loginMessage) throws Throwable {
        loginMessage = TestData.getValue(loginMessage);
        assertThat(homePage.validateLogin(loginMessage), is(true));
    }

    @Then("^the home page would contain \"([^\"]*)\"$")
    public void theHomePageWouldContain(String appUrl) throws Throwable {
        appUrl = TestData.getValue(appUrl);
        Assert.assertTrue(basePage.validateAppUrl(appUrl));
    }
}
