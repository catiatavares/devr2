package com.optal.HomePageStep;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.End2EndPages.CreateANewOrderPage;
import com.optal.HomePage.CreateOrderNoVendorPage;
import com.optal.support.WorldHelper;
import com.optal.waits.WebWaits;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.List;

public class CreateOrderNoVendorSteps {

    private WorldHelper helper;
    private CreateOrderNoVendorPage createOrderNoVendorPage;
    private CreateANewOrderPage createANewOrderPage;
    private CommonObjectPage commonObjectPage;

    public CreateOrderNoVendorSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @Given("^I click on Create order with an unregistered vendor from Homepage$")
    public void iClickOnCreateOrderWithAnUnregisteredVendorFromHomepage() throws Throwable {
        createOrderNoVendorPage = helper.getCreateANewOrderNoVendorPage().clickCreateOrderNoVendorLink();
    }

    @Then("^I enter text in textfields$")
    public void iEnterTextInTextFields(DataTable enterForm) throws Throwable {
        List<List<String>> formObjects = enterForm.raw();
        String nameField = formObjects.get(1).get(1);
        String add1 = formObjects.get(2).get(1);
        String add2 = formObjects.get(3).get(1);
        String cty = formObjects.get(4).get(1);
        String pcode = formObjects.get(5).get(1);
        String area = formObjects.get(6).get(1);
        String telNum = formObjects.get(7).get(1);
        String mobNum = formObjects.get(8).get(1);
        String faxNum = formObjects.get(9).get(1);
        String emailAdd = formObjects.get(10).get(1);
        String confEmail = formObjects.get(11).get(1);
        createOrderNoVendorPage.fillFormText(nameField, add1, add2, cty, pcode, area, telNum, mobNum, faxNum, emailAdd, confEmail);
    }

    @And("^I select dropdown in form$")
    public void iSelectDropdownInForm() {
        createOrderNoVendorPage.selectDropdowndetail("UNITED KINGDOM", "UNITED KINGDOM");
    }

    @When("^I click on Create vendor and place order button$")
    public void iClickOnCreateVendorAndPlaceOrderButton() throws Throwable {
        createOrderNoVendorPage.enterFaxAndClickButton("GB928644589");

    }

    @Then("^it should navigate to create new order and call create order tests$")
    public void itShouldNavigateToCreateNewOrderAndCallCreateOrderTests() throws Throwable {
        createANewOrderPage = helper.getCreateANewOrderPage().fillRequiredOrderFields("testing", "0.80", "REGRESSION2");
        commonObjectPage = helper.getCommonObjectPage().clickOKButton();
        createANewOrderPage = helper.getCreateANewOrderPage().moveToSplitLineCodingModal();
        createANewOrderPage = helper.getCreateANewOrderPage().splitLineAndClickOk("regression", "regression", "regression", "REGRESSION2", "REGRESSION2", "REGRESSION2");
        createANewOrderPage = helper.getCreateANewOrderPage().selectDateAndPlaceOrder();
        commonObjectPage = helper.getCommonObjectPage().clickPlaceOrderButton();
        createANewOrderPage = helper.getCreateANewOrderPage().selectCard();
        WebWaits.waitForNoOfSeconds(2);
        createANewOrderPage = helper.getCreateANewOrderPage().selectAddress();
        createANewOrderPage = helper.getCreateANewOrderPage().clickOrderNumber();
        WebWaits.waitForNoOfSeconds(5);
        helper.getCreateANewOrderPage().validateOrderStatus("Awaiting Vendor Confirmation");
    }

    //delete vendor
    @Given("^I loop into Delete Vendor Column to delete and click OK$")
    public void iLoopIntoDeleteVendorColumnToDelete() throws Throwable {
        createOrderNoVendorPage = helper.getCreateANewOrderNoVendorPage().deleteVendor();
        commonObjectPage = helper.getCommonObjectPage().clickOKButton();
    }

    //place order
    @Then("^I click on place order no vendor button$")
    public void iClickOnPlaceOrderNoVendorButton() throws Throwable {
        createOrderNoVendorPage = helper.getCreateANewOrderNoVendorPage().placeOrderNoVendorButton();
    }

    @And("^I should see \"([^\"]*)\" screen")
    public void iShouldSeeCreateOrderScreen(String createorder) throws Throwable {
        Assert.assertTrue(createOrderNoVendorPage.validateCreateOrder(createorder));
    }
}
