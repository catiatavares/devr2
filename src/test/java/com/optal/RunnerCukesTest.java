package com.optal;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;

//The below is PDF Config
@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(
        jsonReport = "target/test-report/report-json.json",
        //retryCount = 2,
        detailedReport = true,
        detailedAggregatedReport =true,
        overviewReport = true,
        coverageReport = true,
        jsonUsageReport = "target/test-report/report-json.json",
        toPDF = true,
        pdfPageSize = "A4 Landscape",
        outputFolder = "target/createPDF")

@CucumberOptions(
        dryRun = false
        ,monochrome = true
        ,plugin = {"pretty",
        "html:target/test-report/report-html",
        "json:target/test-report/report-json.json",
        "junit:target/test-report/report-xml.xml",}
        ,strict = false
        ,features = {"src/test/resources/"}
        ,snippets = SnippetType.CAMELCASE
        ,glue = {"com.optal"}
        ,tags = {"@forgottenPasswordResetEmail"}
)

public class RunnerCukesTest {
/*
@forgottenPasswordResetEmailOpenEmail
@psw
@forgottenPasswordResetEmail


 */


}


/**   In other to get cucumber Advance report similar to jenkins - run verify suing the below config
 @RunWith(Cucumber.class)
@CucumberOptions(
        dryRun = false
        ,monochrome = true
        ,plugin = {"pretty",
        "html:target/test-report/report-html",
        "json:target/test-report/report-json.json",
        "junit:target/test-report/report-xml.xml",

        "json:target/cucumber-reports/CucumberTestReport.json"}
        ,strict = false
        ,features = {"src/test/resources/"}
        ,snippets = SnippetType.CAMELCASE
        ,glue = {"com.optal"}
        ,tags = {"@login"}
)

public class RunnerCukesTest extends AbstractTestNGCucumberTests {

}
**/