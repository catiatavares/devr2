package com.optal.End2EndSteps;

import com.optal.End2EndPages.AlignFRecordWithIRecordPage;
import com.optal.End2EndPages.AmendOrderByBuyerVendorPage;
import com.optal.support.WorldHelper;
import com.optal.utilities.TestData;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;


public class AmendOrderByBuyerVendorSteps {
    private WorldHelper helper;
    private AmendOrderByBuyerVendorPage amendOrderByBuyerVendorPage;
    private AlignFRecordWithIRecordPage alignFRecordWithIRecordPage;

    public AmendOrderByBuyerVendorSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @And("^I add new line$")
    public void iAddNewLine(DataTable dataTable) {
        List<List<String>> batchCardOption = dataTable.raw();
        String itemName = TestData.getValue(batchCardOption.get(1).get(1));
        String itemCode = TestData.getValue(batchCardOption.get(2).get(1));
        String orderPrice = TestData.getValue(batchCardOption.get(3).get(1));
        amendOrderByBuyerVendorPage = helper.getAmendOrderByBuyerVendorPage().vendorAddNewOrder(itemName, itemCode, orderPrice);
    }

    @And("^I Change currency and submit change$")
    public void iChangeCurrencyAndSubmitChange() {
        amendOrderByBuyerVendorPage.changeCurrency();
        amendOrderByBuyerVendorPage.submitChange();
    }

    @Given("^I should fill up split line coding form by vendor$")
    public void iShouldFillUpSplitLineCodingFormByVendor() {
        amendOrderByBuyerVendorPage.splitLine();
    }

    @And("^I Update auxiliary details$")
    public void iUpdateAuxiliaryDetails() {
        alignFRecordWithIRecordPage = helper.getAlignFRecordWithIRecordPage().selectDateAddLineAuxiliaryDetails();
    }

    @When("^I update line coding custom fields$")
    public void iUpdateLineCodingCustomFields() {
        amendOrderByBuyerVendorPage.updateSubsidyChange();
    }

    @Then("^I accept full order and dispatch good$")
    public void iAcceptFullOrderAndDispatchGood() {
        amendOrderByBuyerVendorPage.acceptFullOrderAndDispatchGood();
    }

    @When("^I accept full order and receipt goods & authorise payment$")
    public void iAcceptFullOrderAndReceiptGoodsAuthorisePayment() {
        amendOrderByBuyerVendorPage.acceptAndReceiptGoodsAndAuthorisePayment();
    }

    @And("^I should fill up split line coding form$")
    public void iShouldFillUpSplitLineCodingForm() {
        amendOrderByBuyerVendorPage = helper.getAmendOrderByBuyerVendorPage().splitFirstLine();
        amendOrderByBuyerVendorPage.splitSecondLine();
    }

    @And("^I should add line & remove line$")
    public void iShouldAddLineRemoveLine() {
        amendOrderByBuyerVendorPage.addRemoveLine();
    }

    @And("^I should add Comment$")
    public void iShouldAddComment() {
        amendOrderByBuyerVendorPage.addComment();
    }
}



