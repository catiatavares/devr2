package com.optal.End2EndSteps;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.CorporateSettingsPage.FinancialSectionCS.PCardManagementLodgeCardPage;
import com.optal.End2EndPages.CreateANewOrderPage;
import com.optal.HomePage.VendorSearchPage;
import com.optal.support.WorldHelper;
import com.optal.utilities.TestData;
import com.optal.waits.WebWaits;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.List;

public class CreateANewOrderSteps {

    private WorldHelper helper;
    private VendorSearchPage vendorSearchPage;
    private CreateANewOrderPage createAnewOrderPage;
    private CommonObjectPage commonObjectPage;
    private PCardManagementLodgeCardPage pCardManagementLodgeCardPage;

    public CreateANewOrderSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I call Add PCard lodge test")
    public void iCallAddPCardLodgeTest() throws Throwable {
        pCardManagementLodgeCardPage = helper.getPCardManagementLodgeCardPage().clickPCardMgtLodgeCardsLink();
        pCardManagementLodgeCardPage = helper.getPCardManagementLodgeCardPage().clickAddANewCard();
        pCardManagementLodgeCardPage = helper.getPCardManagementLodgeCardPage().addCardDetailsForm("4111111111111111", "767", "Dollars (United States)", "Visa",
                "Mr", "Test", "Tesing", "4", "2025", "30", "800", "My TestCard");
//        TODO -  testData property file not able to pick values - would be nice to use this option
    }

    @When("^I select the create a new order link$")
    public void iSelectTheCreateANewOrderLink() throws Throwable {
        vendorSearchPage = helper.getAccountHomePage().selectCreateANewOrderLink();
    }

    @When("^I click on P-Card Management - Lodge Cards Assignment$")
    public void iClickPCardLink() throws Throwable {
        createAnewOrderPage = helper.getCreateANewOrderPage().clickPCardLink();
    }

    @When("^I select the \"([^\"]*)\" button for the selected vendor$")
    public void iSelectTheButtonForTheSelectedVendor(String order) throws Throwable {
        createAnewOrderPage = helper.getVendorSearchPage().clickPlaceOrderButton(order);
    }

    @And("^I fill the required fields as below: and click OK button$")
    public void iFillTheRequiredFieldsAsBelowAndClickOkButton(DataTable dataTable) throws Throwable {
        List<List<String>> batchCardOption = dataTable.raw();
        String orderName = TestData.getValue(batchCardOption.get(1).get(1));
        String price = TestData.getValue(batchCardOption.get(2).get(1));
        String code = TestData.getValue(batchCardOption.get(3).get(1));
        WebWaits.waitForNoOfSeconds(5);
        createAnewOrderPage = helper.getCreateANewOrderPage().fillRequiredOrderFields(orderName, price, code);
        commonObjectPage = helper.getCommonObjectPage().clickOKButton();
    }

    @And("^I select a commodity code$")
    public void iSelectACommodityCode() throws Throwable {

    }

    @And("^I select a \"([^\"]*)\"$")
    public void iSelectA(String commCode) throws Throwable {
//        createOrderPage.selectCommodityCode(commCode);
    }

    @And("^I select to split the order$")
    public void iSelectToSplitTheOrder() throws Throwable {
        createAnewOrderPage.moveToSplitLineCodingModal();
    }

    @And("^I fill all required fields as below:$")
    public void iFillAllRequiredFieldsAsBelow(DataTable dataTable) throws Throwable {
        List<List<String>> splitOption = dataTable.raw();
        String budget = TestData.getValue(splitOption.get(1).get(1));
        String ledger = TestData.getValue(splitOption.get(2).get(1));
        String cost = TestData.getValue(splitOption.get(3).get(1));
        String budget1 = TestData.getValue(splitOption.get(4).get(1));
        String ledger1 = TestData.getValue(splitOption.get(5).get(1));
        String cost1 = TestData.getValue(splitOption.get(6).get(1));
        createAnewOrderPage.splitLineAndClickOk(budget, ledger, cost, budget1, ledger1, cost1);
    }

    @And("^I enter the date and click place order button$")
    public void iEnterTheDate() throws Throwable {
        createAnewOrderPage.selectDateAndPlaceOrder();
        commonObjectPage = helper.getCommonObjectPage().clickPlaceOrderButton();
    }

    @And("^I select Payment Card$")
    public void iSelectPaymentCard() throws Throwable {
        createAnewOrderPage.selectCard();
    }

    @When("^I select Address$")
    public void iSelectAddress() throws Throwable {
        createAnewOrderPage.selectAddress();
    }

    @Then("^I should successfully place an order with an order status of \"([^\"]*)\"$")
    public void iShouldSuccessfullyPlaceAnOrderWithAnOrderStatusOf(String awaitingVendorStatus) throws Throwable {
        createAnewOrderPage.clickOrderNumber();
        Assert.assertTrue(createAnewOrderPage.validateOrderStatus(awaitingVendorStatus));
    }
}
