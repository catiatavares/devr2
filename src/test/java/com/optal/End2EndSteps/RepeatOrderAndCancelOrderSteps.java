package com.optal.End2EndSteps;

import com.optal.End2EndPages.RepeatOrderAndCancelOrderPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RepeatOrderAndCancelOrderSteps {
    private WorldHelper helper;
    private RepeatOrderAndCancelOrderPage repeatOrderAndCancelOrderPage;

    public RepeatOrderAndCancelOrderSteps(WorldHelper helper) {
        this.helper = helper;
    }
    @When("^I place repeat order and submit changes$")
    public void iPlaceRepeatOrderAndSubmitChanges() {
        repeatOrderAndCancelOrderPage = helper.getRepeatOrderAndCancelOrderPage().clickOnRepeatOrder();
    }

    @Then("^I cancel order$")
    public void iCancelOrder() {
        repeatOrderAndCancelOrderPage=helper.getRepeatOrderAndCancelOrderPage().cancelOrder();
    }

    @And("^I click on transaction search and select Order Cancelled status$")
    public void iClickOnTransactionSearchAndSelectOrderCancelledStatus() {
        repeatOrderAndCancelOrderPage.getOrderCancelled();
    }
}
