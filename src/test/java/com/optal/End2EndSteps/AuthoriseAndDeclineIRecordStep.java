package com.optal.End2EndSteps;

import com.optal.End2EndPages.AuthoriseAndDeclineIRecordPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;

public class AuthoriseAndDeclineIRecordStep {
    private WorldHelper helper;
    private AuthoriseAndDeclineIRecordPage authoriseAndCancelIRecordPage;
    public AuthoriseAndDeclineIRecordStep(WorldHelper helper) {
        this.helper = helper;}

    @And("^I click on Search Criteria$")
    public void iClickOnSearchCriteria() throws Throwable {
        authoriseAndCancelIRecordPage = helper.getAuthoriseAndCancelIRecordPage().searchCriteria();
    }

    @And("^I select corporate user as All Users$")
    public void iSelectCorporateUserAsAllUsers() throws Throwable {
        authoriseAndCancelIRecordPage = helper.getAuthoriseAndCancelIRecordPage().allUsers();
    }

    @And("^I click on Associated Records$")
    public void iClickOnAssociatedRecords() throws Throwable {
        authoriseAndCancelIRecordPage = helper.getAuthoriseAndCancelIRecordPage().associatedTransaction();
    }

    @And("^I click on the Irecord Transaction$")
    public void iClickOnTheIrecordTransaction() throws Throwable {
        authoriseAndCancelIRecordPage = helper.getAuthoriseAndCancelIRecordPage().iRecord();
    }
}
