package com.optal.End2EndSteps;

import com.optal.End2EndPages.AuthoriseOrderFlowPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class AuthoriseOrderFlowSteps {
    private WorldHelper helper;
    private AuthoriseOrderFlowPage authoriseOrderFlowPage;
    public AuthoriseOrderFlowSteps (WorldHelper helper) {
        this.helper = helper;
    }

    @And("^I click on the transaction number$")
    public void iClickOnTheTransactionNumber() throws Throwable {
      authoriseOrderFlowPage = helper.getAuthoriseOrderFlowPage().clickTransactionNos();
    }

    @And("^I click on Update Subsidy changes$")
    public void iClickOnUpdateSubsidyChanges() throws Throwable {
        authoriseOrderFlowPage= helper.getAuthoriseOrderFlowPage().clickSubsidyChanges();
    }

    @And("^I click on Authorise button$")
    public void iClickOnAuthoriseButton() throws Throwable {
        authoriseOrderFlowPage= helper.getAuthoriseOrderFlowPage().clickAuthorise();
    }

    @And("^I click on Decline button$")
    public void iClickOnDeclineButton() throws Throwable {
        authoriseOrderFlowPage = helper.getAuthoriseOrderFlowPage().clickDecline();
    }

    @Then("^I comment reason for decline and click reject order$")
    public void iCommentReasonForDeclineAndClickRejectOrder() throws Throwable {
        authoriseOrderFlowPage = helper.getAuthoriseOrderFlowPage().clickReject();
    }
}
