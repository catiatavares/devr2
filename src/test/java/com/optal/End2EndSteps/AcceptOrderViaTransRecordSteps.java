package com.optal.End2EndSteps;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.End2EndPages.AcceptOrderViaTransRecordPage;
import com.optal.End2EndPages.CreateANewOrderPage;
import com.optal.HomePage.BasePage;
import com.optal.HomePage.HomePage;
import com.optal.HomePage.TransactionSearchPage;
import com.optal.support.WorldHelper;
import com.optal.utilities.TestData;
import com.optal.waits.WebWaits;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.assertTrue;

public class AcceptOrderViaTransRecordSteps {

    private WorldHelper helper;
    private AcceptOrderViaTransRecordPage acceptOrderViaTransRecordPage;
    private CreateANewOrderPage createANewOrderPage;
    private BasePage basePage;
    private HomePage homePage;
    private TransactionSearchPage transactionSearchPage;
    private CommonObjectPage commonObjectPage;


    public AcceptOrderViaTransRecordSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I select awaiting transactions$")
    public void iSelectAwaitingTransactions() throws Throwable {
        acceptOrderViaTransRecordPage = helper.getAccountHomePage().selectTransactionsAwaitingYourActions();
    }

    @And("^I select the new order$")
    public void iSelectTheNewOrder() throws Throwable {
        acceptOrderViaTransRecordPage = acceptOrderViaTransRecordPage.selectARow();
    }

    @When("^I Accept the order$")
    public void iAcceptTheOrder() throws Throwable {
        acceptOrderViaTransRecordPage = helper.getAcceptOrderPage().acceptNewOrder();
    }

    @Then("^I should be able to see the new order status of \"([^\"]*)\"$")
    public void iShouldBeAbleToSeeOrderStatusAs(String awaitingDeliveryStatus) throws Throwable {
        assertTrue(acceptOrderViaTransRecordPage.validateOrderStatus(awaitingDeliveryStatus));
    }

    @Then("^I should be able to successfully place and get an order number$")
    public void iShouldBeAbleToSuccessfullyPlaceAndGetAnOrderNumber() throws Throwable {
        helper.getCreateANewOrderPage().getOrderNumber();
    }

    @When("^I click the order number$")
    public void iClickTheOrderNumber() throws Throwable {
        createANewOrderPage = helper.getCreateANewOrderPage().clickOrderNumber();
    }

    @Then("^I can get the order status as \"([^\"]*)\"$")
    public void iCanGetTheOrderStatusAs(String awaitingVendorStatus) throws Throwable {
        assertTrue(createANewOrderPage.validateOrderStatus(awaitingVendorStatus));
    }

    @And("^I can successfully log out$")
    public void iCanSuccessfullyLogOut() throws Throwable {
        basePage = helper.getAccountHomePage().logOut();
    }

    @When("^I log in with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iLogInWithAnd(String vendorUsername, String vendorPassword) throws Throwable {
        vendorUsername = TestData.getValue(vendorUsername);
        vendorPassword = TestData.getValue(vendorPassword);
        homePage = basePage.loginWith(vendorUsername, vendorPassword);
    }

    @And("^I search for the new order$")
    public void iSearchForTheNewOrder() throws Throwable {
        transactionSearchPage = homePage.goToTransactionSearchPage().searchNewOrder();
        commonObjectPage = helper.getCommonObjectPage().clickSearchButton();
    }

    @When("^I select the order$")
    public void iSelectTheOrder() throws Throwable {
        acceptOrderViaTransRecordPage = transactionSearchPage.selectNewOrder();
    }

    @Then("^I can see order status as \"([^\"]*)\"$")
    public void iCanSeeOrderStatusAs(String newStatus) throws Throwable {
        assertTrue(acceptOrderViaTransRecordPage.validateOrderStatus(newStatus));
    }

    @When("^I Accept order$")
    public void iAcceptOrder() throws Throwable {
        acceptOrderViaTransRecordPage.acceptNewOrder();
    }

    @Then("^Order status will change to \"([^\"]*)\"$")
    public void orderStatusWillChangeTo(String awaitingDeliveryStatus) throws Throwable {
        assertTrue(acceptOrderViaTransRecordPage.validateOrderStatus(awaitingDeliveryStatus));
    }

    @When("^I login in with \"([^\"]*)\" & \"([^\"]*)\"$")
    public void iLoginInWith(String username, String password) throws Throwable {
        username = TestData.getValue(username);
        password = TestData.getValue(password);
        homePage = helper.getBasePage().loginWith(username, password);
    }

    @And("^I search for the specific order$")
    public void iSearchForTheSpecificOrder() throws Throwable {
        transactionSearchPage = homePage.goToTransactionSearchPage().searchNewOrder();
        commonObjectPage = helper.getCommonObjectPage().clickSearchButton();
    }

    @And("^I select the specific order$")
    public void iSelectTheSpecificOrder() throws Throwable {
        createANewOrderPage = transactionSearchPage.selectAcceptedOrder();
    }

    @Then("^I can see the order status as \"([^\"]*)\"$")
    public void iCanSeeTheOrderStatusAs(String goodReceiptStatus) throws Throwable {
        assertTrue(createANewOrderPage.validateOrderStatus(goodReceiptStatus));
    }

    @And("^I Receipt Goods and Authorise Payment$")
    public void iReceiptGoodsAndAuthorisePayment() throws Throwable {
        createANewOrderPage.authorisePayment();
    }

    @Then("^the Order status will change to \"([^\"]*)\"$")
    public void theOrderStatusWillChangeTo(String archivedStatus) throws Throwable {
        assertTrue(createANewOrderPage.validateOrderStatus(archivedStatus));
    }
}

