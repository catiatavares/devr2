package com.optal.End2EndSteps;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.End2EndPages.AlignFRecordWithIRecordPage;
import com.optal.End2EndPages.AmendOrderByBuyerVendorPage;
import com.optal.End2EndPages.CreateANewOrderPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AlignFRecordWithIRecordSteps {
    private WorldHelper helper;
    private AlignFRecordWithIRecordPage alignFRecordWithIRecordPage;
    private CreateANewOrderPage createANewOrderPage;
    private AmendOrderByBuyerVendorPage amendOrderByBuyerVendorPage;
    private CommonObjectPage commonObjectPage;

    public AlignFRecordWithIRecordSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @And("^I enter the date$")
    public void iEnterTheDate() {
        createANewOrderPage = helper.getCreateANewOrderPage().selectDateAndPlaceOrder();
    }

    @And("^I add GLCode$")
    public void iAddGLCode() throws Throwable {
        alignFRecordWithIRecordPage = helper.getAlignFRecordWithIRecordPage().addGLCode();
    }

    @And("^I enter the date in new line Auxiliary Details$")
    public void iEnterTheDateInNewLineAuxiliaryDetails() throws Throwable {
        alignFRecordWithIRecordPage.selectDateAddLineAuxiliaryDetails();
    }

    @And("^I Change currency$")
    public void iChangeCurrency() {
        amendOrderByBuyerVendorPage = helper.getAmendOrderByBuyerVendorPage().changeCurrency();
    }

    @When("^I click on place order$")
    public void iClickOnPlaceOrder() {
        commonObjectPage = helper.getCommonObjectPage().clickPlaceOrderButton();
    }

    @And("^I accept first partial order and receipt goods & authorise payment$")
    public void iAcceptFirstPartialOrderAndReceiptGoodsAuthorisePayment() throws Throwable {
        alignFRecordWithIRecordPage.acceptFirstPartialOrder();
        alignFRecordWithIRecordPage.issueReceiptGoodsAndAuthorisePayment();
    }

    @And("^I accept second partial order and receipt goods & authorise payment$")
    public void iAcceptSecondPartialOrderAndReceiptGoodsAuthorisePayment() throws Throwable {
        alignFRecordWithIRecordPage.acceptSecondPartialOrder();
        alignFRecordWithIRecordPage.issueReceiptGoodsAndAuthorisePayment();
    }

    @Then("^I verify I and F record$")
    public void iVerifyIAndFRecord() {
        alignFRecordWithIRecordPage.verifyingFIRecordForFirstOrder();
        alignFRecordWithIRecordPage.verifyingFIRecordForSecondOrder();
    }

}
