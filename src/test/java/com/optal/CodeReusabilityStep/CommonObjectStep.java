package com.optal.CodeReusabilityStep;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class CommonObjectStep {

    private WorldHelper helper;
    private CommonObjectPage commonObjectPage;

    public CommonObjectStep(WorldHelper helper) {
        this.helper = helper;
    }

    @Then("^I click on OK button$")
    public void iClickOnOKButton() throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickOKButton();
    }

    @Then("^I click on the place order button$")
    public void iClickOnThePlaceOrderButton() throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickOKButton();
    }

    @And("^I click on save button$")
    public void iClickOnSaveButton() throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickSaveButton();
    }

    @And("^I click on submit type button$")
    public void iClickOnSubmitTypeButton() throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickSubmitTypeButton();
    }

    @Given("^I click on Delete button$")
    public void iClickOnDelete() throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickDeleteButton();
    }

    @Given("^I click the Edit button$")
    public void iClickTheEditButton() throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickEditButton();
    }

    @Then("^I click on search button$")
    public void iClickOnSearchButton() throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickSearchButton();
    }

    @Then("^I click Ok button$")
    public void iClickOnOkButton() throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickOkButton();
    }

    @And("^I click on First Transaction from search result$")
    public void IclickOnFirstTransactionFromSearchResult() throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickFirstTransaction();
    }

    @And("^I click continue$")
    public void iClickContinue() throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickContinue();
    }

    @And("^I click on back button$")
    public void iClickOnBack()  throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickOnBackButton();
    }

    @And("^I click on home scheme$")
    public void iClickOnHomeScheme() throws Throwable {
        commonObjectPage=helper.getCommonObjectPage().clickHomeScheme();
    }
}
