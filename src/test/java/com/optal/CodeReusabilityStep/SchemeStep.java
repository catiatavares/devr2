package com.optal.CodeReusabilityStep;

import com.optal.CodeReusabilityPage.SchemesPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class SchemeStep {

    private WorldHelper helper;
    private SchemesPage schemePage;

    public SchemeStep(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I click on corporate settings$")
    public void iClickOnCorporateSettings() throws Throwable {
        schemePage = helper.getSchemesPage().clickCorporateSettings();
    }

    @And("^I click on settings$")
    public void iClickOnSettings() throws Throwable {
        schemePage = helper.getSchemesPage().clickSettingsScheme();
    }

    @When("^I click on Customer Support scheme$")
    public void iClickOnCustomerSupportScheme() throws Throwable {
        schemePage = helper.getSchemesPage().customerSupport();
    }
}
