package com.optal.InviteANewVendorStep;

import com.optal.InviteANewVendorPage.InviteANewVendorPage;
import com.optal.support.WorldHelper;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.List;

public class InviteANewVendorSteps {

    private WorldHelper helper;
    private InviteANewVendorPage inviteANewVendorPage;

    public InviteANewVendorSteps(WorldHelper helper) {
        this.helper = helper;
    }

    //invite a new vendor
    @When("^I click on invite a new vendor$")
    public void iClickOnInviteANewVendor() throws Throwable {
        inviteANewVendorPage = helper.getRequestANewVendorpage().clickInviteVendor();
    }

    @And("^I fill the required fields below:$")
    public void iFillTheRequiredFieldsBelow(DataTable vendorForm) throws Throwable {
        System.out.println(vendorForm);
        List<List<String>> formObjects = vendorForm.raw();
        String nameField = formObjects.get(1).get(1);
        String address1 = formObjects.get(2).get(1);
        String address2 = formObjects.get(3).get(1);
        String city = formObjects.get(4).get(1);
        String postcode = formObjects.get(5).get(1);
        String area = formObjects.get(6).get(1);
        String telephoneNumber = formObjects.get(7).get(1);
        String mobileNumber = formObjects.get(8).get(1);
        String faxNumber = formObjects.get(9).get(1);
        String emailAddress = formObjects.get(10).get(1);
        String confirmEmailAddress = formObjects.get(11).get(1);
        inviteANewVendorPage.vendorDetailsForm(nameField, address1, address2, city, postcode, area, telephoneNumber, mobileNumber, faxNumber, emailAddress, confirmEmailAddress);
    }

    @And("^I select dropdown in Vendorform$")
    public void iSelectDropdownInVendorForm(DataTable vendorFormCountry) {
        List<List<String>> formObjects = vendorFormCountry.raw();
        String contry = formObjects.get(0).get(1);
        String contry2 = formObjects.get(1).get(1);
        inviteANewVendorPage.selectDropdowndetails(contry, contry2);
    }

    @And("^I click on Send Invitation$")
    public void iClickOnSendInvitation() throws Throwable {
        inviteANewVendorPage.clickSendInvitation();
    }

    @Then("^I should be able to see a new page with a message \"([^\"]*)\"$")
    public void iShouldBeAbleToSeeANewPageWithAMessage(String validateInviteSent) throws Throwable {
        Assert.assertTrue(inviteANewVendorPage.validateInvitationSent(validateInviteSent));
    }

    //cancel Invitation
    @Given("^I loop into Cancel Invitation to cancel pending invitation$")
    public void iLoopIntoCancelInvitationToCancelPendingInvitation() throws Throwable {
        inviteANewVendorPage = helper.getRequestANewVendorpage().cancelInvitation();
    }

    //resend vendor
    @Given("^I loop into Resend Invitation to resend pending invitation$")
    public void iLoopIntorESENDInvitationToCancelPendingInvitation() throws Throwable {
        inviteANewVendorPage = helper.getRequestANewVendorpage().resendInvitation();
    }
}



