package com.optal.WebAppStep;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.End2EndPages.AcceptOrderViaTransRecordPage;
import com.optal.End2EndPages.CreateANewOrderPage;
import com.optal.HomePage.HomePage;
import com.optal.HomePage.TransactionSearchPage;
import com.optal.WebAppPage.AuthWebAppAcceptOrderPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class AuthWebAppAcceptOrderSteps {

    private WorldHelper helper;
    private AuthWebAppAcceptOrderPage authWebAppAcceptOrderPage;
    private TransactionSearchPage transactionSearchPage;
    private HomePage homePage;
    private CommonObjectPage commonObjectPage;
    private AcceptOrderViaTransRecordPage acceptOrderViaTransRecordPage;
    private CreateANewOrderPage createANewOrderPage;

    public AuthWebAppAcceptOrderSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @Given("^I navigate to \"(.*)\" url$")
    public void iNavigateToUrl(String url) throws Throwable {
        authWebAppAcceptOrderPage = helper.getAuthWebAppAcceptOrderPage().navigateToHomePage(url);
    }

    @And("^I sign in as Authoriser with$")
    public void iSignInAsAuthoriserWith() {
        authWebAppAcceptOrderPage.loginWebApp();
    }

    @And("^I select pins$")
    public void iSelectPins() {
        authWebAppAcceptOrderPage.enterPin();
    }

    @Given("^I should be able to see \"([^\"]*)\" text$")
    public void iShouldBeAbleToSeeText(String arg1) throws Throwable {
        Assert.assertEquals(arg1, authWebAppAcceptOrderPage.validateInvapayAuthorisations(), "Error Massage: User not at home page");
        System.out.println(authWebAppAcceptOrderPage.validateInvapayAuthorisations());
    }

    @And("^I change emails settings$")
    public void iChangeEmailsSettings() throws Throwable {
        authWebAppAcceptOrderPage.selectionOfEmail();
    }

    @And("^I accept order$")
    public void iAcceptOrder() throws Throwable {
        authWebAppAcceptOrderPage.acceptOrder();
    }

    @And("^I log out$")
    public void iLogOut() throws Throwable {
        authWebAppAcceptOrderPage.logOut();
    }

    @When("^I search for the new order and select the order$")
    public void iSearchForTheNewOrderAndSelectTheOrder() throws Throwable {
        authWebAppAcceptOrderPage = helper.getAuthWebAppAcceptOrderPage().clickOnTransactionSearch();
        transactionSearchPage = helper.getTransactionSearchPage().searchNewOrder();
        commonObjectPage = helper.getCommonObjectPage().clickSearchButton();
        acceptOrderViaTransRecordPage = transactionSearchPage.selectNewOrder();
    }

    @Then("^I can get order status as \"([^\"]*)\"$")
    public void iCanGetOrderStatusAs(String awaitingVendorStatus) throws Throwable {
        Assert.assertTrue(authWebAppAcceptOrderPage.validateOrderStatus(awaitingVendorStatus), awaitingVendorStatus);
    }

}
