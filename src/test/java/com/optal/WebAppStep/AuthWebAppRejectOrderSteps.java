package com.optal.WebAppStep;

import com.optal.WebAppPage.AuthWebAppRejectOrderPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.Given;

public class AuthWebAppRejectOrderSteps {
    private WorldHelper helper;
    private AuthWebAppRejectOrderPage authWebAppRejectOrderPage;

    public AuthWebAppRejectOrderSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @Given("^I reject order$")
    public void iRejectOrder() throws Throwable {
        authWebAppRejectOrderPage = helper.getAuthWebAppRejectOrderPage().rejectOrder();
    }

}
