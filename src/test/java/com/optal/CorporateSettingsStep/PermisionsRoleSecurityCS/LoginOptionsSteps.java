package com.optal.CorporateSettingsStep.PermisionsRoleSecurityCS;

import com.optal.CorporateSettingsPage.PermissionRoleSecurityCS.LoginOptionsPage;
import com.optal.support.WorldHelper;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import java.util.List;

public class LoginOptionsSteps {

    private WorldHelper helper;
    private LoginOptionsPage loginOptionsPage;

    public LoginOptionsSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @And("^I click on Login Options$")
    public void iClickOnLoginOptions() throws Throwable {
        loginOptionsPage = helper.getLoginOptionsPage().clickLoginOptions();
    }

    @And("^I tick the checkbox$")
    public void iTickTheCheckbox() throws Throwable {
        loginOptionsPage.clickCheckBox();
    }

    @And("^I enter figure in the table below$")
    public void iEnterFigureInTheTableBelow(DataTable expiryalertform) throws Throwable {
        System.out.println(expiryalertform);
        List<List<String>> formObjects = expiryalertform.raw();
        String expireOlder = formObjects.get(0).get(1);
        String emailAlertAfte = formObjects.get(1).get(1);
        loginOptionsPage.addPasswordExpForm(expireOlder, emailAlertAfte);
    }

    @And("^I tick the lock accounts with unchanged passwords$")
    public void iTickTheLockAccountsWithUnchangedPasswords() throws Throwable {
        loginOptionsPage.clickLockAccount();
        loginOptionsPage.dailyAlert();
    }

    @Then("^I should see login options settings successfully updated$")
    public void iShouldSeeLoginOptionsSettingsSuccessfullyUpdated() throws Throwable {
        loginOptionsPage.validateLoginOptionMessage();
    }

    @And("^I tick and untick the extra security checkbox$")
    public void iTickTheExtraSecurityCheckbox() throws Throwable {
        loginOptionsPage = helper.getLoginOptionsPage().clickExtraSecurity();
    }

    @And("^I tick checkbox for Email to go out until password expires$")
    public void iTickTheEmailsDailyAlert() throws Throwable {
        loginOptionsPage = helper.getLoginOptionsPage().clickExtraSecurity();
    }

    @And("^I untick the checkbox to disable feature$")
    public void iUnTickTheCheckbox() throws Throwable {
        loginOptionsPage.unTickEnableAutoPassCheckbox();
    }
}
