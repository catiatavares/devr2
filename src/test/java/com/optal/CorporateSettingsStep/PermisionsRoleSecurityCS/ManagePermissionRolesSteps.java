package com.optal.CorporateSettingsStep.PermisionsRoleSecurityCS;

import com.optal.CorporateSettingsPage.PermissionRoleSecurityCS.ManagePermissionRolePage;
import com.optal.support.WorldHelper;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.List;

public class ManagePermissionRolesSteps {
    private WorldHelper helper;
    private ManagePermissionRolePage managePermissionRolePage;

    public ManagePermissionRolesSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I click on Manage permission roles$")
    public void iClickOnManagePermissionRoles() throws Throwable {
        managePermissionRolePage = helper.getPermissionRolePage().clickManagePermissionRole();
    }

    @When("^I click on Create a new role$")
    public void iClickOnCreateANewRole() throws Throwable {
        managePermissionRolePage.clickCreateANewRole();
    }

    @When("^I enter text in the field below$")
    public void iEnterTextInTheFieldBelow(DataTable discriptionform) throws Throwable {
        System.out.println(discriptionform);
        List<List<String>> formObjects = discriptionform.raw();
        String description = formObjects.get(0).get(1);
        managePermissionRolePage.descriptform(description);
    }

    @When("^I tick checkbox Allow the user to raise a quote$")
    public void iTickCheckboxAllowTheUserToRaiseAQuote() throws Throwable {
        managePermissionRolePage.clickRoleBox();
    }

      @Then("^I should see permission roles updated successfully$")
    public void iShouldSeePermissionRolesUpdatedSuccessfully() throws Throwable {
        managePermissionRolePage.validateSuccessMessage();
    }

    @Given("^I tick to Allow the user to search for an existing vendor$")
    public void iTickToAllowTheUserToSearchForAnExistingVendor() throws Throwable {
        managePermissionRolePage.clickOnSearchExVendor();
    }

    @Given("^I change description$")
    public void iChangeDescription() throws Throwable {
        managePermissionRolePage.changeDescField();
    }

}
