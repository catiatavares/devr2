package com.optal.CorporateSettingsStep.UserManagementSection;

import com.optal.CorporateSettingsPage.UserManagementSection.AddNewUserPage;
import com.optal.support.WorldHelper;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

public class AddNewUserStep {

    private WorldHelper helper;
    private AddNewUserPage addNewUserPage;

    public AddNewUserStep(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I click on Add New User$")
    public void iClickOnAddNewUser() throws Throwable {
        addNewUserPage = helper.getAddNewUserPage().clickAddNewUser();
    }

    @And("^I entered username as below:$")
    public void iEnteredUsernameAsBelow(DataTable usernameform) throws Throwable {
        System.out.println(usernameform);
        List<List<String>> formObjects = usernameform.raw();
        String usernameline = formObjects.get(0).get(1);
        addNewUserPage.addusernameform();
    }

    @And("^I click on Next Step$")
    public void iClickOnNextStep() throws Throwable {
        addNewUserPage.clickNextStep();
    }

    @Then("^my username and password login details should display$")
    public void myUsernameAndPasswordLoginDetailsShouldDisplay() throws Throwable {
        addNewUserPage.validateLoginDetailsDisplay();
    }

    @And("^I enter the details below$")
    public void iEnterTheDetailsBelow(DataTable addNewUserFormm) throws Throwable {
        System.out.println(addNewUserFormm);
        List<List<String>> formObjects = addNewUserFormm.raw();
        String firstnameField = formObjects.get(0).get(1);
        String surnameField = formObjects.get(1).get(1);
        String address1 = formObjects.get(2).get(1);
        String address2 = formObjects.get(3).get(1);
        String city = formObjects.get(4).get(1);
        String postcode = formObjects.get(5).get(1);
        String countryy = formObjects.get(6).get(1);
        String area = formObjects.get(7).get(1);
        String emailAddress = formObjects.get(8).get(1);
        String confirmEmailAddress = formObjects.get(9).get(1);
        String telephoneNumber = formObjects.get(10).get(1);
        String mobileNumber = formObjects.get(11).get(1);
        String faxNumber = formObjects.get(12).get(1);
        addNewUserPage.userDetailsForm(firstnameField, surnameField, address1, address2, city, postcode, countryy, area, emailAddress,
                confirmEmailAddress, telephoneNumber, mobileNumber, faxNumber);
    }

    @And("^I click on Add User$")
    public void iClickOnAddUser() throws Throwable {
        addNewUserPage.clickAddUser();
    }

    @Then("^user should be added and page redirected to User Settings$")
    public void userShouldBeAddedAndPageRedirectedToUserSettings() throws Throwable {
        addNewUserPage.validateUserRedirection();
    }
}
