package com.optal.CorporateSettingsStep.UserManagementSection;

import com.optal.CorporateSettingsPage.UserManagementSection.ModifyCurrentUserPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class ModifyCurrentUserStep {

    private WorldHelper helper;
    private ModifyCurrentUserPage modifyCurrentUserPage;

    public ModifyCurrentUserStep(WorldHelper helper) {
        this.helper = helper;
    }

    @Then("^I click on Modify User$")
    public void iClickOnModifyUser() throws Throwable {
        modifyCurrentUserPage = helper.getModifyCurrentUserPage().clickModifyUser();
    }

    @Then("^I should see \"([^\"]*)\"$")
    public void iShouldSeeModifyCurrentSystemUsers(String textOnScreen) throws Throwable {
        Assert.assertTrue(modifyCurrentUserPage.validateModifyCurrentUser(textOnScreen));
    }

    @Then("^I select user with Area Manager as name to Edit$")
    public void iSelectUserWithAsNameToEdit() throws Throwable {
        modifyCurrentUserPage.clickModifyUser();
    }
}
