package com.optal.CorporateSettingsStep.CorporateProfileSection;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.CorporateSettingsPage.CorporateProfileSection.CorporateMessagesPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import org.junit.Assert;

public class CorporateMessagesSteps {

    private WorldHelper helper;
    private CorporateMessagesPage corporateMessagesPage;
    private CommonObjectPage commonObjectPage;


    public CorporateMessagesSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @And("I click on Corporate Messages")
    public void iClickOnCorporateMessages() throws Throwable {
        corporateMessagesPage = helper.getCorporateMessagePage().clickCorp();
    }

    @And("Add Message and tick Corporate and Orders")
    public void addMessageAndTickCorporateAndOrders() throws Throwable {
        corporateMessagesPage = helper.getCorporateMessagePage().clickAdd();
    }

    @And("I can see \"([^\"]*)\" on Corporate Homepage")
    public void iCanSeeDisplayedMessageOnCorporateHomepage(String textFound) throws Throwable {
        corporateMessagesPage.displayMgs();
        Assert.assertEquals("Error msg: Test not displayed", textFound, corporateMessagesPage.displayedMsg());

    }

    @And("I search for an order")
    public void iSearchForAnOrder() throws Throwable {
        corporateMessagesPage = helper.getCorporateMessagePage().searchTransaction();
        commonObjectPage = helper.getCommonObjectPage().clickSearchButton();
    }

    @Then("I can see \"([^\"]*)\" on Order footer")
    public void iCanSeeDisplayedMessageOnOrderFooter(String textFound) throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickFirstTransaction();
        corporateMessagesPage = helper.getCorporateMessagePage().viewMgs();
        Assert.assertEquals("Error msg: Test not displayed", textFound, corporateMessagesPage.displayedMsg());
    }

    @And("^I can see message \"([^\"]*)\" announcement box$")
    public void iCanSeeMessageDisplayedOnAnnouncementBox(String message) throws Throwable {
        Assert.assertEquals("Error msg: Test not displayed", message, corporateMessagesPage.displayedMsg());
    }

    @And("^I can see message \"([^\"]*)\" Transaction Footer$")
    public void iCanSeeMessageDisplayedOnTransactionFooter(String transaction) throws Throwable {
        Assert.assertEquals("Error msg: Test not displayed", transaction, corporateMessagesPage.displayedMsg());

    }
}















