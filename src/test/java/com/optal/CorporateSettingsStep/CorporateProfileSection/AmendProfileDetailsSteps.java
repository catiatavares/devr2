package com.optal.CorporateSettingsStep.CorporateProfileSection;

import com.optal.CorporateSettingsPage.CorporateProfileSection.AmendProfileDetailsPage;
import com.optal.support.WorldHelper;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import java.util.List;

public class AmendProfileDetailsSteps {

    private WorldHelper helper;
    private AmendProfileDetailsPage amendProfileDetailsPage;

    public AmendProfileDetailsSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @Then("^I click on Amend Profile Details$")
    public void iClickOnAmendProfileDetails() throws Throwable {
        amendProfileDetailsPage = helper.getAmendProfDetailsPage().setAmendProfile();
    }

    @Given("^I edit profile detail$")
    public void iEditProfileDetail(DataTable getData) throws Throwable {
        System.out.println(getData);
        List<List<String>> getDataObject = getData.raw();
        String displayName = getDataObject.get(1).get(1);
        String tradeName = getDataObject.get(2).get(1);
        String add1 = getDataObject.get(3).get(1);
        String add2 = getDataObject.get(4).get(1);
        String town = getDataObject.get(5).get(1);
        String pCode = getDataObject.get(6).get(1);
        String county = getDataObject.get(7).get(1);
        String mainContact = getDataObject.get(8).get(1);
        String contactEmail = getDataObject.get(9).get(1);
        String confirmEmail = getDataObject.get(10).get(1);
        String telNum = getDataObject.get(11).get(1);
        String mobNum = getDataObject.get(12).get(1);
        String faxNum = getDataObject.get(13).get(1);
        String vatTaxRegCtry = getDataObject.get(14).get(1);
        String vatTaxRegNum = getDataObject.get(15).get(1);
        String compRegNum = getDataObject.get(16).get(1);
        String currency = getDataObject.get(17).get(1);
        String country1 = getDataObject.get(18).get(1);
        String country2 = getDataObject.get(19).get(1);
        amendProfileDetailsPage.enterDetail(displayName, tradeName, add1, add2, town, pCode, county, mainContact, contactEmail, confirmEmail, telNum, mobNum, faxNum,
                vatTaxRegCtry, vatTaxRegNum, compRegNum, currency, country1, country2);
    }

    @Then("^I click Save Profile button$")
    public void iClickSaveProfileButton() throws Throwable {
        amendProfileDetailsPage.clickSaveButton();
    }
}

