package com.optal.CorporateSettingsStep.CorporateProfileSection;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.CorporateSettingsPage.CorporateProfileSection.AmendCustomFieldsPage;
import com.optal.CorporateSettingsPage.PermissionRoleSecurityCS.LoginOptionsPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;

public class AmendCustomFieldsStep {
    private WorldHelper helper;
    private AmendCustomFieldsPage amendCustomFieldsPage;
    private LoginOptionsPage loginOptionsPage;
    private CommonObjectPage commonObjectPage;

    public AmendCustomFieldsStep(WorldHelper helper) {
        this.helper = helper;
    }

    @And("^I amend custom fields$")
    public void iAmendCustomFields() {
        amendCustomFieldsPage = helper.getamendCustomFieldsPage().clickOnAmendCustomFields();
    }

    @Then("^I click on add field$")
    public void iClickOnAddField() {
        amendCustomFieldsPage.clickOnAddField();
    }

    @And("^I click on fields$")
    public void iClickOnFields() {
        amendCustomFieldsPage.clickOnAddField();
    }

    @And("^I enter text in name$")
    public void iEnterTextInName() {
        amendCustomFieldsPage.enterName();
    }

    @And("^I select type$")
    public void iSelectType() {
        amendCustomFieldsPage.typeSelect();
    }

    @And("^I select level$")
    public void iSelectLevel() {
        amendCustomFieldsPage.levelSelect();
    }

    @Then("^I should see \"([^\"]*)\" massage$")
    public void iShouldSeeCustomFieldAddedSuccessfullyMassage(String customAddMsg) {
        Assert.assertEquals("Error msg: Error message: Custom field not added successfully", customAddMsg, amendCustomFieldsPage.validateCustomAddMsg());
        System.out.println("Test");
    }

    @Then("I should see \"([^\"]*)\" successfully")
    public void iShouldSeeCustomFieldVisibilityUpdatedSuccessfully(String customVisibleMsg) {
        amendCustomFieldsPage.customFieldVisibilityUpdatedSuccessfully();
        commonObjectPage = helper.getCommonObjectPage().clickSaveButton();
        Assert.assertEquals("Error message: Untick visible not save", customVisibleMsg, amendCustomFieldsPage.validateCustomVisibility());
    }
}
