package com.optal.CorporateSettingsStep.AccountingCodesSectionCS;

import com.optal.CorporateSettingsPage.AccountingCodesSectionCS.AmendCostCentreCodePage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class AmendCostCentreCodeStep {

    private WorldHelper helper;
    private AmendCostCentreCodePage amendCostCentreCodePage;

    public AmendCostCentreCodeStep(WorldHelper helper) {
        this.helper = helper;
    }

    @Given("^I click on Cost centre Code link$")
    public void iClickOnCostCentreCodeLink() throws Throwable {
        amendCostCentreCodePage = helper.getAmendCostCentreCodePage().clickCostCentreCodeLink();
    }

    @When("^I click Add Cost centre Code button$")
    public void iClickAddCostCentreCodeButton() throws Throwable {
        amendCostCentreCodePage = helper.getAmendCostCentreCodePage().clickAddCostCentreCode();
    }

    @Then("Added \"([^\"]*)\" should be displayed$")
    public void AddedMessage(String addText) throws Throwable {
        Assert.assertTrue(amendCostCentreCodePage.validateAddMsg(addText));
    }

    @Then("Edited \"([^\"]*)\" should be displayed$")
    public void EditedMessage(String editText) throws Throwable {
        Assert.assertTrue(amendCostCentreCodePage.validateEditMessage(editText));
    }

    @Then("Deleted \"([^\"]*)\" should be displayed$")
    public void DeletedMessage(String deleteText) throws Throwable {
        Assert.assertTrue(amendCostCentreCodePage.validateDeleteMessage(deleteText));
    }
}
