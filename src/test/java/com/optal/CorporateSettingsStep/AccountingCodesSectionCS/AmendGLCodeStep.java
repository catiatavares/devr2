package com.optal.CorporateSettingsStep.AccountingCodesSectionCS;

import com.optal.CorporateSettingsPage.AccountingCodesSectionCS.AmendGLCodePage;
import com.optal.support.WorldHelper;
import com.optal.utilities.TestData;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.List;

public class AmendGLCodeStep {

    private WorldHelper helper;
    private AmendGLCodePage amendGLCodePage;

    public AmendGLCodeStep(WorldHelper helper) {
        this.helper = helper;
    }

    @And("^I click on Amend GL Code link$")
    public void iClickOnAmendGLCodelink() throws Throwable {
        amendGLCodePage = helper.getAmendGLcodePage().clickAmendGLCodeLink();
    }

    @When("^I click Add GL Code button$")
    public void iClickAmendGLCodebutton() throws Throwable {
        amendGLCodePage = helper.getAmendGLcodePage().clickAddGlCode();
    }

    @And("^I enter text in the popup$")
    public void iEnterTextInThePopup(DataTable glCodePopupModal) throws Throwable {
        System.out.println(glCodePopupModal);
        List<List<String>> addInformation = glCodePopupModal.raw();
        String desc = TestData.getValue(addInformation.get(1).get(1));
        String codeField = TestData.getValue(addInformation.get(2).get(1));
        String popUpMsg = TestData.getValue(addInformation.get(3).get(1));
        amendGLCodePage.fillGlDetails(desc, codeField, popUpMsg);
    }

    @When("^I can see Add \"([^\"]*)\" displayed$")
    public void iCanSeeAddMessageDisplayed(String addMessage) throws Throwable {
        Assert.assertEquals("Error message: Add message not found", addMessage, amendGLCodePage.validateAddMsg());
    }

    @When("^I can see Edit \"([^\"]*)\" displayed$")
    public void iCanSeeEditMessageDisplayed(String editMessage) throws Throwable {
        Assert.assertEquals("Error message: Add message not found", editMessage, amendGLCodePage.validateEditMessage());
    }

    @Then("^I edit details of glCode$")
    public void iEditDetailOfGlCode(DataTable editTable) throws Throwable {
        System.out.println(editTable);
        List<List<String>> changeTableDetails = editTable.raw();
        String desc = changeTableDetails.get(0).get(1);
        String code = changeTableDetails.get(1).get(1);
        String popUpMsg = changeTableDetails.get(2).get(1);
        amendGLCodePage = helper.getAmendGLcodePage().fillGlDetails(desc, code, popUpMsg);
    }

    @When("^I can see Delete \"([^\"]*)\" displayed$")
    public void iCanSeeDeleteMessageDisplayed(String deleteMessage) throws Throwable {
        Assert.assertEquals("Error message: Add message not found", deleteMessage, amendGLCodePage.validateDeleteMessage());

    }
}
