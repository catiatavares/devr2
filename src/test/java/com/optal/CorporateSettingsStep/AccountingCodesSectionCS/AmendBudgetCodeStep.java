package com.optal.CorporateSettingsStep.AccountingCodesSectionCS;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.CorporateSettingsPage.AccountingCodesSectionCS.AmendBudgetCodePage;
import com.optal.support.WorldHelper;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.List;

public class AmendBudgetCodeStep {

    private WorldHelper helper;
    private AmendBudgetCodePage amendBudgetCodePage;
    private CommonObjectPage commonObjectPage;

    public AmendBudgetCodeStep(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I click on Amend Budget Code link$")
    public void iClickOnAmendBudgetCodeLink() throws Throwable {
        amendBudgetCodePage = helper.getAmendBudgetCodePage().clickBudgetCodeLink();
    }

    @When("^I click Add Budget Code button$")
    public void iClickAddBudgetCodeButton() throws Throwable {
        amendBudgetCodePage.clickAddBudgetCode();
    }

    @When("^I enter text in the popup window and click save code$")
    public void iEnterTextInThePopupWindowAndClickSaveCode(DataTable budgetCodeData) throws Throwable {
        System.out.println(budgetCodeData);
        List<List<String>> addinfo = budgetCodeData.raw();
        String desc = addinfo.get(1).get(1);
        String code = addinfo.get(2).get(1);
        amendBudgetCodePage = helper.getAmendBudgetCodePage().fillGlDetails(desc, code);
        commonObjectPage = helper.getCommonObjectPage().clickSaveButton();
    }

    @Then("^Text \"([^\"]*)\" should be displayed$")
    public void iShouldSeeScreenText(String addText) throws Throwable {
        Assert.assertTrue(amendBudgetCodePage.validateAddMsg(addText));
    }

    @Then("^I edit details of Code$")
    public void iEditDetailsOfBudgetCode(DataTable editBudgetCode) throws Throwable {
        System.out.println(editBudgetCode);
        List<List<String>> addinfo = editBudgetCode.raw();
        String desc2 = addinfo.get(1).get(1);
        String code2 = addinfo.get(2).get(1);
        amendBudgetCodePage = helper.getAmendBudgetCodePage().fillGlDetails(desc2, code2);
    }

    @Then("^I can see \"([^\"]*)\" displayed on screen$")
    public void iCanSeeMessageDisplayedOnScreen(String editText) throws Throwable {
        Assert.assertTrue(amendBudgetCodePage.validateEditMessage(editText));
    }

    @Then("^I can see \"([^\"]*)\" displayed$")
    public void iCanSeeDisplayed(String deleteText) throws Throwable {
        Assert.assertTrue(amendBudgetCodePage.validateDeleteMessage(deleteText));
    }
}
