package com.optal.CorporateSettingsStep.FinancialSectionCS;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.CorporateSettingsPage.FinancialSectionCS.PCardManagementLodgeCardPage;
import com.optal.support.WorldHelper;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import org.junit.Assert;

import java.util.List;

public class PCardManagementLodgeCardSteps {

    private WorldHelper helper;
    private PCardManagementLodgeCardPage pCardManagementLodgeCardPage;
    private CommonObjectPage commonObjectPage;

    public PCardManagementLodgeCardSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @And("^I click on PCard Management lodge cards under financial section$")
    public void iClickOnPCardManagementLodgeCardsUnderFinancialSection() throws Throwable {
        pCardManagementLodgeCardPage = helper.getPCardManagementLodgeCardPage().clickPCardMgtLodgeCardsLink();
    }

    @And("^I click on Add a New Card button$")
    public void iClickOnAddANewCardButton() throws Throwable {
        pCardManagementLodgeCardPage = helper.getPCardManagementLodgeCardPage().clickAddANewCard();
    }

    @And("^I fill the form fields below and click on submit type button$")
    public void iFillTheFormFieldsBelowandClickonSubmitButton(DataTable addLodgeCardForm) throws Throwable {
        System.out.println(addLodgeCardForm);
        List<List<String>> formObjects = addLodgeCardForm.raw();
        String cardNumber = formObjects.get(0).get(1);
        String cardCvv = formObjects.get(1).get(1);
        String cardCurrency = formObjects.get(2).get(1);
        String cardIssuerType = formObjects.get(3).get(1);
        String cardHolderTitle = formObjects.get(4).get(1);
        String cardholderFirstName = formObjects.get(5).get(1);
        String cardHolderSurname = formObjects.get(6).get(1);
        String expiryMonth = formObjects.get(7).get(1);
        String expiryYear = formObjects.get(8).get(1);
        String cardIssueNumber = formObjects.get(9).get(1);
        String cardLimit = formObjects.get(10).get(1);
        String cardDescription = formObjects.get(11).get(1);
        pCardManagementLodgeCardPage.addCardDetailsForm(cardNumber, cardCvv, cardCurrency, cardIssuerType, cardHolderTitle, cardholderFirstName, cardHolderSurname, expiryMonth, expiryYear, cardIssueNumber, cardLimit, cardDescription);
        commonObjectPage = helper.getCommonObjectPage().clickSubmitTypeButton();
    }

    @And("^I should see PCard Added \"([^\"]*)\" message$")
    public void iShouldSeePCardAddPcardMessage(String addMsg) throws Throwable {
        Assert.assertEquals("Error message: New Card not adde", addMsg, pCardManagementLodgeCardPage.actualResultForNewAddedCard());

    }

    @And("^I edit the below details and click on submit type button$")
    public void iEditTheBelowDetails(DataTable editCardForm) throws Throwable {
        System.out.println(editCardForm);
        List<List<String>> formObjects = editCardForm.raw();
        String cardNumber = formObjects.get(0).get(1);
        pCardManagementLodgeCardPage = helper.getPCardManagementLodgeCardPage().editCardDetailsForm(cardNumber);
        commonObjectPage = helper.getCommonObjectPage().clickSubmitTypeButton();
    }

    @And("^I should see Pcard Edited \"([^\"]*)\" message$")
    public void iShouldSeePcardEditedMessage(String editMsg) throws Throwable {
        Assert.assertEquals("Error message: Successfully not edited", editMsg, pCardManagementLodgeCardPage.actualResultForEditedCard());
    }

    @And("^I should see Pcard Deleted \"([^\"]*)\" message$")
    public void iShouldSeePcardDeletedMessage(String deleteMsg) throws Throwable {
        Assert.assertEquals("Error message: Card is not deleted", deleteMsg, pCardManagementLodgeCardPage.actualResultForDeleteMessage());
    }
}