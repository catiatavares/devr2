package com.optal.SettingsStep.PermisionsRolesSection;

import com.optal.SettingsPage.PermissionsRoles.AssignUserPermissionsPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class AssignUserPermissionSteps {
    private WorldHelper helper;
    private AssignUserPermissionsPage assignUserPermissionsPage;

    public AssignUserPermissionSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @And("^I click on assign user permissions$")
    public void iClickOnAssignUserPermissions() {
        assignUserPermissionsPage=helper.getAssignUserPermissionsPage().clickOnAssignUserPermissionsButton();
    }

    @When("^I am able to unassigned administration permission and save$")
    public void iAmAbleToUnassignedAdministrationPermissionAndSave() {
        assignUserPermissionsPage=helper.getAssignUserPermissionsPage().unAssignAdministrationPermission();
    }

    @And("^I am able to move all available roles permission and save$")
    public void iAmAbleToMoveAllAvailableRolesPermissionAndSave() {
        assignUserPermissionsPage=helper.getAssignUserPermissionsPage().clickOnAddAllRollsArrow();
    }

    @And("^I am able to unassigned all permission and save$")
    public void iAmAbleToUnassignedAllPermissionAndSave() {
        assignUserPermissionsPage=helper.getAssignUserPermissionsPage().clickOnRemoveAllRolesArrows();
    }

    @And("^I am able to assign administrator permission$")
    public void iAmAbleToAssignAdministratorPermission() {
        assignUserPermissionsPage.addAdministratorPermissions();
    }

    @Then("^I should see information massage \"([^\"]*)\"$")
    public void iShouldSeeInformationMassageSettingsSavedSuccessfully(String savedMsg) {
        Assert.assertEquals("Error msg: Permission not saved ", savedMsg, assignUserPermissionsPage.settingsSavedSuccessfully());
    }
}


