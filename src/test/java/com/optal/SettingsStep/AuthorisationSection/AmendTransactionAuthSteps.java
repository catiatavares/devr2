package com.optal.SettingsStep.AuthorisationSection;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.CorporateSettingsPage.AccountingCodesSectionCS.AmendGLCodePage;
import com.optal.CorporateSettingsPage.PermissionRoleSecurityCS.LoginOptionsPage;
import com.optal.HomePage.RequestANewQuotationpage;
import com.optal.SettingsPage.Authorisation.AmendTransactionAuthPage;
import com.optal.support.WorldHelper;
import com.optal.utilities.TestData;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.List;

public class AmendTransactionAuthSteps {

    private WorldHelper helper;
    private AmendTransactionAuthPage amendTransactionAuthPage;
    private AmendGLCodePage amendGLCodePage;
    private LoginOptionsPage loginOptionsPage;
    private RequestANewQuotationpage requestANewQuotationpage;
    private CommonObjectPage commonObjectPage;

    public AmendTransactionAuthSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I select to amend transaction authorisation$")
    public void iSelectToAmendTransactionAuthorisation() throws Throwable {
        amendTransactionAuthPage = helper.getTransactionAuthPage().goToTransactionAuthPage();
    }

    @When("^I add a User Authorisation$")
    public void iAddAUserAuthorisation() throws Throwable {
        amendTransactionAuthPage.selectAddAuthButton();
    }

    @When("^I select options as below:$")
    public void iSelectOptionsAsBelow(DataTable dataTable) throws Throwable {
        List<List<String>> addAuthOption = dataTable.raw();
        String authUser = TestData.getValue(addAuthOption.get(1).get(1));
        String authType = TestData.getValue(addAuthOption.get(2).get(1));
        String currency = TestData.getValue(addAuthOption.get(3).get(1));
        String threshold = TestData.getValue(addAuthOption.get(4).get(1));
        amendTransactionAuthPage.addUserAuthorisation(authUser, authType, currency, threshold);
    }

    @Then("^I can see the user added as an authoriser with a message displayed \"([^\"]*)\"$")
    public void iCanSeeTheUserAddedAsAnAuthoriserWithAMessageDisplayed(String addMessage) throws Throwable {
        Assert.assertTrue(amendTransactionAuthPage.validateAddedAuthorisationUser(addMessage));

    }

    @Then("^I click edit")
    public void iClickEdit() throws Throwable {
        commonObjectPage = helper.getCommonObjectPage().clickEditButton();
    }

    @When("^I select the delete the added authorisation user$")
    public void iSelectTheDeleteTheAddedAuthorisationUser() throws Throwable {
        amendTransactionAuthPage.deleteAuthorisationUser();
        commonObjectPage = helper.getCommonObjectPage().clickOKButton();
    }

    @Then("^I should be able to see the authorised user deleted with a message \"([^\"]*)\"$")
    public void iShouldBeAbleToSeeTheAuthorisedUserDeletedWithAMessage(String deleteMessage) throws Throwable {
        Assert.assertTrue(amendTransactionAuthPage.validateAddedAuthorisationUser(deleteMessage));
    }

    @And("^I edit threshold value as below: and click save$")
    public void iEditThresholdValueAsBelowAndClickSave(DataTable editTable) throws Throwable {
        List<List<String>> editAuthOption = editTable.raw();
        String authUser = TestData.getValue(editAuthOption.get(1).get(1));
        amendTransactionAuthPage.editUserAuthorisation(authUser);
        commonObjectPage = helper.getCommonObjectPage().clickSaveButton();
    }

    @Then("^I can see the user edited message displayed \"([^\"]*)\"$")
    public void iCanSeeTheUserEditedMessageDisplayed(String editMessage) throws Throwable {
        amendTransactionAuthPage.validateAddedAuthorisationUser(editMessage);
    }
}
