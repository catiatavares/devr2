package com.optal.SettingsStep.AuthorisationSection;

import com.optal.SettingsPage.Authorisation.AmendTransactionAuthPage;
import com.optal.SettingsPage.Authorisation.AuthorisationSummaryPage;
import com.optal.support.WorldHelper;
import com.optal.utilities.TestData;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

import java.util.List;

public class AuthorisationSummarySteps {
    private WorldHelper helper;
    private AuthorisationSummaryPage authorisationSummaryPage;
    private AmendTransactionAuthPage amendTransactionAuthPage;

    public AuthorisationSummarySteps(WorldHelper helper) {
        this.helper = helper;
    }

    //Assign and unassign authorisation user
     @And("^I click on Select All button and should see bottons checked$")
    public void iClickOnSelectAllButtonAndShouldSeeButtonChecked() throws Throwable {
        authorisationSummaryPage.selectALLButtons();
    }

    @Given("^I click on UnSelectAll button and should see bottons unchecked$")
    public void iClickOnUnSelectAllButtonAndShouldSeeButtonsUnchecked() throws Throwable {
        authorisationSummaryPage.unSelectALLButtons();
    }

    @And("^I click on Unassign All Pre Authorised Users$")
    public void iClickOnUnassignAllPreAuthorisedUsers() throws Throwable {
        authorisationSummaryPage.unAssignAllPreAuthUser();
    }

    @And("^I click on Unassign All Post Authorised Users$")
    public void iClickOnUnassignAllPostAuthorisedUsers() throws Throwable {
        authorisationSummaryPage.unAssignAllPostAuthUser();
    }

    @Given("^I select Modify Current user link$")
    public void iSelectModifyCurrentUserLink() throws Throwable {
        authorisationSummaryPage = helper.getAuthSummaryPage().setModifyCurrentUsers();
    }

    @Given("^I enter user detail in the search fields$")
    public void iEnterOnSearchFields(DataTable data) throws Throwable {
        System.out.println(data);
        List<List<String>> enterNewDetails = data.raw();
        String firstName = enterNewDetails.get(1).get(1);
        String lastName = enterNewDetails.get(2).get(1);
        authorisationSummaryPage.userDetails(firstName, lastName);
    }

    @Given("^I check if the default checkbox is ticked$")
    public void iAssertTheDefaultCheckboxIsTicked() throws Throwable {
        authorisationSummaryPage.defaultCheckBox();
    }

    // Reassign authorisation user
    @And("^I click on Authorisation Summary link$")
    public void iClickOnAuthorisationSummaryLink() throws Throwable {
        authorisationSummaryPage.addNewUser();
    }

    @When("^I click amend transaction authorisation link$")
    public void iClickAmendTransactionAuthorisationLink() throws Throwable {
        authorisationSummaryPage=helper.getAuthSummaryPage().amendTransaction();
    }

    @Given("^I call add authorisation user tests$")
    public void iCallAddAuthorisationUsertests() throws Throwable {
        amendTransactionAuthPage = helper.getTransactionAuthPage().selectAddAuthButton();
        amendTransactionAuthPage = helper.getTransactionAuthPage().addUserAuthorisation("testing team", "PRE", "GBP", "100");
        helper.getTransactionAuthPage().validateAddedAuthorisationUser("authorisation added successfully");
    }

    @Given("^I tick a checkbox$")
    public void iTickACheckbox() throws Throwable {
        authorisationSummaryPage = helper.getAuthSummaryPage();
        authorisationSummaryPage.tickCheckbox();
    }

    @Given("^I click on Reassign Selected Users button$")
    public void iClickOnReassignSelectedUsersButton() throws Throwable {
        authorisationSummaryPage.reAssignSelectedUsers();
    }

    @Given("^I select new authorisation user as below:$")
    public void iSelectNewAuthorisationUserAsBelow(DataTable data) throws Throwable {
        List<List<String>> selectNewAuthUser = data.raw();
        String authNewUser = TestData.getValue(selectNewAuthUser.get(1).get(1));
        authorisationSummaryPage.selectNewAuthUser(authNewUser);
    }
}

