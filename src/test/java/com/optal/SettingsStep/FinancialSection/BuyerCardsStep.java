package com.optal.SettingsStep.FinancialSection;

import com.optal.SettingsPage.Financial.BuyerCardsPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.When;

public class BuyerCardsStep {

    private WorldHelper helper;
    private BuyerCardsPage buyerCardsPage ;

    public BuyerCardsStep(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I click on PCard Management buyer cards under financial section$")
    public void iClickOnPCardManagementBuyerCardsUnderFinancialSection()  {
       buyerCardsPage =helper.getBuyerCardsPage().clickOnPCardManagementBuyerCards();
    }

}
