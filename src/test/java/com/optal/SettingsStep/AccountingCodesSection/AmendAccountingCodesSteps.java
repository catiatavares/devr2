package com.optal.SettingsStep.AccountingCodesSection;

import com.optal.SettingsPage.AccountingCodes.AmendAccountingCodesPage;
import com.optal.support.WorldHelper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class AmendAccountingCodesSteps {
    private WorldHelper helper;
    private AmendAccountingCodesPage amendAccountingCodesPage;

    public AmendAccountingCodesSteps(WorldHelper helper) {
        this.helper = helper;
    }

    @When("^I click on Amend Accounting Codes$")
    public void iClickOnAmendAccountingCodes() {
        amendAccountingCodesPage = helper.getAmendAccountingCodesPage().clickOnAmendAccountingCodes();
    }

    @And("^I click on budget code$")
    public void iClickOnBudgetCode() {
        amendAccountingCodesPage.clickOnBudgetCode();
    }

    @And("^I click on double forward arrow and save$")
    public void iClickOnDoubleForwardArrowAndSave() {
        amendAccountingCodesPage.clickOnAddAll();
    }

    @And("I click on double backward arrow and save")
    public void iClickOnDoubleBackwardArrowAndSave() {
        amendAccountingCodesPage.clickOnRemoveAllBudgetCodes();
    }

    @And("I click on forward arrow and save for budget code")
    public void iClickOnForwardArrowAndSaveForBudgetCode() {
        amendAccountingCodesPage.moveBackOldCodes();
    }

    @Then("^I should be able to see \"([^\"]*)\"$")
    public void iShouldBeAbleToSeeAccountingCodesUpdatedSuccessfully(String textDisplayed) {
        Assert.assertEquals("Error msg: Successfully not saved", textDisplayed, amendAccountingCodesPage.validateMsgDisplayed());
    }

    @And("I click on cost centre")
    public void iClickOnCostCentre() {
        amendAccountingCodesPage.clickOnCostCentre();
    }

    @And("^I click on forward arrow and save for Cost Centre$")
    public void iClickOnForwardArrowAndSaveForCostCentre() {
        amendAccountingCodesPage.moveBackOldCodeForCostCentre();
    }

    @And("^I click on GL Code$")
    public void iClickOnGLCode() {
        amendAccountingCodesPage.clickOnGLCode();
    }

    @And("^I click on forward arrow and save for Gl Code$")
    public void iClickOnForwardArrowAndSaveForGlCode() {
        amendAccountingCodesPage.moveBackOldForGLCode();
    }
}
