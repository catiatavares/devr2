
package com.optal.SettingsStep.CommunicationSection;

import com.optal.SettingsPage.Communication.AmendCommunicationSettingsPage;
import com.optal.support.WorldHelper;
import com.optal.waits.WebWaits;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class AmendCommunicationSettingsSteps {

    private WorldHelper helper;
    private AmendCommunicationSettingsPage amendCommunicationSettingsPage;

public AmendCommunicationSettingsSteps (WorldHelper helper) {
    this.helper = helper;
}


    @And("^I select Amend Communication settings link$")
    public void iSelectAmendCommunicationSettingsLink() throws Throwable {
        WebWaits.waitForNoOfSeconds(5);
       amendCommunicationSettingsPage = helper.getAmendCommunicationSettingsPage().clickComSettings();
    }

    @When("^I deselect any tick boxes$")
    public void iDeselectAnyTickBoxes() throws Throwable {
        amendCommunicationSettingsPage.clickCheckbox1();
        amendCommunicationSettingsPage.clickCheckbox2();
    }

    @Then("^A successful updated message is displayed$")
    public void aSuccessfulUpdatedMessageIsDisplayed(){
    amendCommunicationSettingsPage.assertMessage();
    }
}
