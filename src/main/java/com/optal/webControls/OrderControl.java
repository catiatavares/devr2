package com.optal.webControls;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class OrderControl extends BaseControl {

    private static String orderNumber;

    public static String getOrderNumber(WebElement orderDetails) {
        for (WebElement tableRowElement : orderDetails.findElements(By.tagName("tr"))) {
            if (!tableRowElement.getText().toLowerCase().contains("order number")) continue;
            List<WebElement> tdElements = tableRowElement.findElements(By.tagName("a"));
            orderNumber = tdElements.stream().filter(x -> !x.getText().equalsIgnoreCase("order number")).distinct().findFirst().get().getText();
            System.out.println(orderNumber);
            return orderNumber;
        }
        return null;
    }

    public static void storeAndSearchOrderNumber(WebElement txtSearch) {
        txtSearch.sendKeys(orderNumber);
    }

    public static String storeAndGetOrderNumber() {
        return orderNumber;
    }

    public static String transactionNo;

    public static String getTransactionNo(WebElement elementOfTransactionNo) {
        transactionNo = elementOfTransactionNo.getText().replaceAll("Transaction No: ", "");
        System.out.println(transactionNo);
        return transactionNo;
    }

}
