package com.optal.webControls;

import org.testng.Assert;

public class AssertControl extends BaseControl {

//    public static void assertByGetText(String expectedResult, WebElement element, String errorMessage) {
//        Assert.assertEquals(expectedResult, CheckBoxControl.getByText(element), errorMessage);
//    }

    public static void assertByGetTextString(String actualResult, String expectedResult, String errorMessage) {
        Assert.assertEquals(actualResult, expectedResult, errorMessage);
    }

}
