package com.optal.webControls;

import com.optal.waits.WebWaits;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TextFieldsControl extends BaseControl {

    private static Logger logger = LogManager.getLogger("TestLogger");


    public static void enterText(WebElement element, String requiredText) {
        for (int counter = 0; counter <= time; counter++) {
            try {
                if (isElementDisplayed(element) && isElementEnabled(element)) {
                    element.clear();
                    element.sendKeys(requiredText);
                    logger.info("Has entered " + requiredText);
                    return;
                } else {
                    WebWaits.waitForNoOfSeconds(2);
                }
                logger.trace("Unable to enter text " + requiredText);
                return;
            } catch (Exception e) {
                WebWaits.waitForNoOfSeconds(1);
                return;
            }
        }
    }

    public static String dateStamp() {
        DateFormat dateFormat = new SimpleDateFormat("DDMMYYYYSSmmHH");
        Date date = new Date();
        String date1 = dateFormat.format(date);
        return date1;
    }

}
