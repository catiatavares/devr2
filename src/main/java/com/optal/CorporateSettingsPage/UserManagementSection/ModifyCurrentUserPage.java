package com.optal.CorporateSettingsPage.UserManagementSection;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ModifyCurrentUserPage extends BasePage {

    public ModifyCurrentUserPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Add New User")
    private WebElement addnewuser;
    @FindBy(name = "sysName")
    private WebElement usernamebox;
    @FindBy(xpath = "//span[contains (text(), 'Next Step')]")
    private WebElement nextstep;
    @FindBy(className = "login-table")
    private WebElement logindetails;
    @FindBy(xpath = "//span[contains (text(), 'Continue')]")
    private WebElement continueButton;
    @FindBy(name = "firstName")
    private WebElement firstnamefield;
    @FindBy(name = "surname")
    private WebElement surnamefield;
    @FindBy(name = "address1")
    private WebElement addy1;
    @FindBy(name = "address2")
    private WebElement addy2;
    @FindBy(name = "city")
    private WebElement cities;
    @FindBy(name = "postcode")
    private WebElement postalcode;
    @FindBy(name = "country")
    private WebElement contry;
    @FindBy(name = "county")
    private WebElement areastate;
    @FindBy(name = "contactEmail")
    private WebElement emailaddy;
    @FindBy(name = "confirmEmail")
    private WebElement emailconfirm;
    @FindBy(name = "phone")
    private WebElement telephone;
    @FindBy(name = "mobile")
    private WebElement mobilenos;
    @FindBy(name = "fax")
    private WebElement faxnos;
    @FindBy(xpath = "//span[contains (text(), 'Add User')]")
    private WebElement adduser;
    @FindBy(className = "linkclass")
    private WebElement userredirection;
    @FindBy(linkText = "Modify Current Users")
    private WebElement modifyCurrentUserLink;
    @FindBy(xpath = "/html/body/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr[1]/td/h3")
    private WebElement assertText;
    @FindBy(xpath = "//*[@id=\"highlightRow\"]/td[1]")
    private WebElement tableColumn;
    @FindBy(xpath = "//span[contains(text(),'Edit User')]")
    private WebElement editUserButton;
    @FindBy(xpath = "//a[@href='/s/userManagement/profile/editUserProfile.do?action=reset']")
    private WebElement amendProfDetailsDisplayed;

    public boolean validateModifyCurrentUser(String textDisplayed) {
        String text = assertText.getText();
        return text.toLowerCase().contains(textDisplayed.toLowerCase());
    }

    public ModifyCurrentUserPage clickModifyUser() {
        ClickControl.click(modifyCurrentUserLink);
        return PageFactory.initElements(webDriver, ModifyCurrentUserPage.class);
    }
}