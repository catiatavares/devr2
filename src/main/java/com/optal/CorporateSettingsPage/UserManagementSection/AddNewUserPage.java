package com.optal.CorporateSettingsPage.UserManagementSection;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddNewUserPage extends BasePage {

    public AddNewUserPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Add New User")
    private WebElement addnewuser;
    @FindBy(name = "sysName")
    private WebElement usernamebox;
    @FindBy(xpath = "//span[contains (text(), 'Next Step')]")
    private WebElement nextstep;
    @FindBy(className = "login-table")
    private WebElement logindetails;
    @FindBy(name = "firstName")
    private WebElement firstnamefield;
    @FindBy(name = "surname")
    private WebElement surnamefield;
    @FindBy(name = "address1")
    private WebElement addy1;
    @FindBy(name = "address2")
    private WebElement addy2;
    @FindBy(name = "city")
    private WebElement cities;
    @FindBy(name = "postcode")
    private WebElement postalcode;
    @FindBy(name = "country")
    private WebElement contry;
    @FindBy(name = "county")
    private WebElement areastate;
    @FindBy(name = "contactEmail")
    private WebElement emailaddy;
    @FindBy(name = "confirmEmail")
    private WebElement emailconfirm;
    @FindBy(name = "phone")
    private WebElement telephone;
    @FindBy(name = "mobile")
    private WebElement mobilenos;
    @FindBy(name = "fax")
    private WebElement faxnos;
    @FindBy(xpath = "//span[contains (text(), 'Add User')]")
    private WebElement adduser;
    @FindBy(className = "linkclass")
    private WebElement userredirection;
    @FindBy(linkText = "Modify Current Users")
    private WebElement modifyCurrentUserLink;
    @FindBy(xpath = "/html/body/table/tbody/tr[3]/td/table/tbody/tr/td/table/tbody/tr[1]/td/h3")
    private WebElement assertText;
    @FindBy(xpath = "//*[@id=\"highlightRow\"]/td[1]")
    private WebElement tableColumn;
    @FindBy(xpath = "//span[contains(text(),'Edit User')]")
    private WebElement editUserButton;
    @FindBy(xpath = "//a[@href='/s/userManagement/profile/editUserProfile.do?action=reset']")
    private WebElement amendProfDetailsDisplayed;

    public AddNewUserPage clickAddNewUser() {
        ClickControl.click(addnewuser);
        return PageFactory.initElements(webDriver, AddNewUserPage.class);
    }

    public AddNewUserPage addusernameform() {
        TextFieldsControl.enterText(usernamebox, "name" + TextFieldsControl.dateStamp());
        return PageFactory.initElements(webDriver, AddNewUserPage.class);
    }

    public AddNewUserPage clickNextStep() {
        ClickControl.click(nextstep);
        return PageFactory.initElements(webDriver, AddNewUserPage.class);
    }

    public void validateLoginDetailsDisplay() {
        if (logindetails.isDisplayed()) {
            System.out.println(logindetails.getText());
        } else System.out.println("NO DETAILS");
    }

     public ModifyCurrentUserPage userDetailsForm(String firstname, String surname, String addyy1, String addyy2,
                                                 String citty, String postcd, String contryy, String areast,
                                                 String emailAdd, String confirmEmailAdd, String teleNum, String mobiNum, String faxNumb) {
        TextFieldsControl.enterText(firstnamefield, firstname);
        TextFieldsControl.enterText(surnamefield, surname);
        TextFieldsControl.enterText(addy1, addyy1);
        TextFieldsControl.enterText(addy2, addyy2);
        TextFieldsControl.enterText(cities, citty);
        TextFieldsControl.enterText(postalcode, postcd);
        DropDownControl.selectDropDownByVisibleText(contry, contryy);
        TextFieldsControl.enterText(areastate, areast);
        TextFieldsControl.enterText(emailaddy, emailAdd);
        TextFieldsControl.enterText(emailconfirm, confirmEmailAdd);
        TextFieldsControl.enterText(telephone, teleNum);
        TextFieldsControl.enterText(mobilenos, mobiNum);
        TextFieldsControl.enterText(faxnos, faxNumb);
        return PageFactory.initElements(webDriver, ModifyCurrentUserPage.class);
    }

    public ModifyCurrentUserPage clickAddUser() {
        ClickControl.click(adduser);
        return PageFactory.initElements(webDriver, ModifyCurrentUserPage.class);
    }

    public void validateUserRedirection() {
        if (userredirection.isDisplayed()) {
            System.out.println(userredirection.getText());
        } else System.out.println("FAILED TO REDIRECT");
    }
}
