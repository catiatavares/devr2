package com.optal.CorporateSettingsPage.AccountingCodesSectionCS;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmendGLCodePage extends BasePage {

    public AmendGLCodePage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Amend General Ledger Codes")
    private WebElement glCodeLink;
    @FindBy(id = "addNewCode")
    private WebElement addNewCode;
    @FindBy(id = "description")
    private WebElement descriptionField;
    @FindBy(id = "code")
    private WebElement codeField;
    @FindBy(id = "popupMessage")
    private WebElement popupMessageField;
    @FindBy(xpath = "//*[@id=\"informationMessage\"]/div/li")
    private WebElement deleteMessage;
    @FindBy(xpath = "//li[contains(text(),'General Ledger Code edited successfully')]")
    private WebElement editMessage;
    @FindBy(xpath = "//li[contains(text(),'General Ledger Code added successfully')]")
    private WebElement addMessage;

    public AmendGLCodePage clickAmendGLCodeLink() {
        ClickControl.click(glCodeLink);
        return this;
    }

    public AmendGLCodePage clickAddGlCode() {
        ClickControl.click(addNewCode);
        return PageFactory.initElements(webDriver, AmendGLCodePage.class);
    }

    public AmendGLCodePage fillGlDetails(String desc, String code, String popUpMsg) {
        WebWaits.waitForNoOfSeconds(2);
        TextFieldsControl.enterText(descriptionField, desc);
        TextFieldsControl.enterText(codeField, code);
        TextFieldsControl.enterText(popupMessageField, popUpMsg);
        return this;
    }

    public String validateAddMsg() {
        return addMessage.getText();
    }

    public String validateEditMessage() {
        return editMessage.getText();
    }

    public String validateDeleteMessage() {
        WebWaits.waitForNoOfSeconds(2);
        return deleteMessage.getText();
    }
}
