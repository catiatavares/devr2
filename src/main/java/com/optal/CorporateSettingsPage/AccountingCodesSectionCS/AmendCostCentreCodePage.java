package com.optal.CorporateSettingsPage.AccountingCodesSectionCS;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmendCostCentreCodePage extends BasePage {

    public AmendCostCentreCodePage(WebDriver webDriver) {
        super(webDriver);
    }


    @FindBy(linkText = "Amend Cost Centre Codes")
    private WebElement costCentreLink;
    @FindBy(id = "addNewCode")
    private WebElement addNewCode;
    @FindBy(id = "description")
    private WebElement descriptionField;
    @FindBy(id = "code")
    private WebElement codeField;
    @FindBy(xpath = "//li[contains(text(),'Cost Centre deleted')]")
    private WebElement deleteMessage;
    @FindBy(xpath = "//li[contains(text(),'Cost Centre edited successfully')]")
    private WebElement editMessage;
    @FindBy(xpath = "//li[contains(text(),'Cost Centre added successfully')]")
    private WebElement addMessage;

    public AmendCostCentreCodePage clickCostCentreCodeLink() {
        ClickControl.click(costCentreLink);
        return this;
    }

    public AmendCostCentreCodePage clickAddCostCentreCode() {
        ClickControl.click(addNewCode);
        return PageFactory.initElements(webDriver, AmendCostCentreCodePage.class);
    }

    public AmendCostCentreCodePage fillCostCentreDetails(String desc, String code) {
        WebWaits.waitForNoOfSeconds(2);
        TextFieldsControl.enterText(descriptionField, desc);
        TextFieldsControl.enterText(codeField, code);
        return this;
    }

    public boolean validateAddMsg(String addText) {
        WebWaits.waitForNoOfSeconds(5);
        String getAddText = addMessage.getText();
        return getAddText.toLowerCase().contains(addText.toLowerCase());
    }

    public boolean validateEditMessage(String editText) {
        WebWaits.waitForNoOfSeconds(5);
        String getEditText = editMessage.getText();
        return getEditText.toLowerCase().contains(editText.toLowerCase());
    }

    public boolean validateDeleteMessage(String deleteText) {
        WebWaits.waitForNoOfSeconds(5);
        String getDeleteText = deleteMessage.getText();
        return getDeleteText.toLowerCase().contains(deleteText.toLowerCase());
    }
}
