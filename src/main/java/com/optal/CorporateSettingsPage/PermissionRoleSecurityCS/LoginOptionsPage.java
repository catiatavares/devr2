package com.optal.CorporateSettingsPage.PermissionRoleSecurityCS;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginOptionsPage extends BasePage {

    public LoginOptionsPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Login Options")
    private WebElement loginoption;
    @FindBy(name = "passwordExpiryEnabled")
    private WebElement enableautopass;
    @FindBy(name = "expireOlderThanDays")
    private WebElement exppassolderthan;
    @FindBy(name = "emailAlertAfterDays")
    private WebElement emailalertafter;
    @FindBy(name = "accountLockingEnabled")
    private WebElement lockaccount;
    @FindBy(name = "alertEveryday")
    private WebElement dailyAlert;
    @FindBy(xpath = "//*[@id=\"informationMessage\"]/div/li")
    private WebElement loginOptionMessage;
    @FindBy(name = "requirePinLogin")
    private WebElement pinRequired;

    public LoginOptionsPage clickLoginOptions() {
        ClickControl.click(loginoption);
        return PageFactory.initElements(webDriver, LoginOptionsPage.class);
    }

    public LoginOptionsPage clickCheckBox() {
        if (!enableautopass.isSelected()) {
            ClickControl.click(enableautopass);
        } else if
        (enableautopass.isSelected()) {
            ClickControl.click(enableautopass);
            ClickControl.click(enableautopass);
        }

        return PageFactory.initElements(webDriver, LoginOptionsPage.class);
    }

    public LoginOptionsPage addPasswordExpForm(String expireOlderthan, String emailAfter) {
        TextFieldsControl.enterText(exppassolderthan, expireOlderthan);
        TextFieldsControl.enterText(emailalertafter, emailAfter);
        return PageFactory.initElements(webDriver, LoginOptionsPage.class);
    }

    public LoginOptionsPage clickLockAccount() {
        if (!lockaccount.isSelected()) {
            ClickControl.click(lockaccount);
        } else if (lockaccount.isSelected()) {
            ClickControl.click(lockaccount);
            ClickControl.click(lockaccount);
        }
        return PageFactory.initElements(webDriver, LoginOptionsPage.class);
    }

    public LoginOptionsPage dailyAlert() {
        if (!dailyAlert.isSelected()) {
            ClickControl.click(lockaccount);
        } else if (dailyAlert.isSelected()) {
            ClickControl.click(dailyAlert);
            ClickControl.click(dailyAlert);
        }
        return PageFactory.initElements(webDriver, LoginOptionsPage.class);
    }

    public LoginOptionsPage clickExtraSecurity() {

        if (pinRequired.isSelected()) {
            ClickControl.click(pinRequired);
        } else if (!pinRequired.isSelected()) {
            ClickControl.click(pinRequired);
            ClickControl.click(pinRequired);
        }
        return PageFactory.initElements(webDriver, LoginOptionsPage.class);
    }

    public void validateLoginOptionMessage() {
        WebWaits.waitForNoOfSeconds(5);
        if (loginOptionMessage.isDisplayed()) {
            System.out.println(loginOptionMessage.getText());
        }
    }

    public LoginOptionsPage unTickEnableAutoPassCheckbox() {
            ClickControl.click(enableautopass);
        return PageFactory.initElements(webDriver, LoginOptionsPage.class);
        }
}
