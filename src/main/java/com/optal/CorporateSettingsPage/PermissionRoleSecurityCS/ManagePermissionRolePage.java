package com.optal.CorporateSettingsPage.PermissionRoleSecurityCS;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManagePermissionRolePage extends BasePage {

    public ManagePermissionRolePage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Manage Permissions/Roles")
    private WebElement managepermroles;
    @FindBy(xpath = "//span[contains (text(), 'Create a new role')]")
    private WebElement createnewrole;
    @FindBy(name = "editRole.name")
    private WebElement descriptext;
    @FindBy(name = "feature[1].checked")
    private WebElement allowuserquote;
    @FindBy(xpath = "//*[@id=\"informationMessage\"]/div/li")
    private WebElement successmessage;
    @FindBy(name = "feature[12].checked")
    private WebElement allowusersearch;

    public ManagePermissionRolePage clickManagePermissionRole() {
        ClickControl.click(managepermroles);
        return PageFactory.initElements(webDriver, ManagePermissionRolePage.class);
    }

    public ManagePermissionRolePage clickCreateANewRole() {
        ClickControl.click(createnewrole);
        return PageFactory.initElements(webDriver, ManagePermissionRolePage.class);
    }

    public ManagePermissionRolePage descriptform(String descriptionField) {
        TextFieldsControl.enterText(descriptext, descriptionField);
        return PageFactory.initElements(webDriver, ManagePermissionRolePage.class);
    }

    public ManagePermissionRolePage clickRoleBox() {
        ClickControl.click(allowuserquote);
        return PageFactory.initElements(webDriver, ManagePermissionRolePage.class);
    }

    public ManagePermissionRolePage clickOnSearchExVendor() {
        ClickControl.click(allowusersearch);
        return PageFactory.initElements(webDriver, ManagePermissionRolePage.class);
    }

    public void validateSuccessMessage() {
        WebWaits.waitForNoOfSeconds(5);
        if (successmessage.isDisplayed()) {
            System.out.println(successmessage.getText());
        } else {
            System.out.println("FAILED");
        }
    }

    public ManagePermissionRolePage changeDescField() {
        ClickControl.click(descriptext);
        return PageFactory.initElements(webDriver, ManagePermissionRolePage.class);
    }

}
