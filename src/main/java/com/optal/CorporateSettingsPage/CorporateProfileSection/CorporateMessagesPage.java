package com.optal.CorporateSettingsPage.CorporateProfileSection;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import com.optal.webControls.TextFieldsControl;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CorporateMessagesPage extends BasePage {

    public CorporateMessagesPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Corporate Messages")
    private WebElement corporateMgsLink;
    @FindBy(name = "validFromDate")
    private WebElement validFromDate;
    @FindBy(linkText = "Add Message")
    private WebElement addMessage;
    @FindBy(name = "validToDate")
    private WebElement validToDate;
    @FindBy(name = "message")
    private WebElement messageBody;
    @FindBy(name = "visibleToCorporateUsers")
    private WebElement corpBox;
    @FindBy(name = "visibleToOrders")
    private WebElement orderBox;
    @FindBy(name = "message")
    private WebElement message;
    @FindBy(xpath = "//*[@id=\"sendMessage\"]")
    private WebElement save;
    @FindBy(xpath = "//*[@id=\"informationMessage\"]/div/li")
    private WebElement information;
    @FindBy(xpath = "//*[@id=\"maintab\"]/li[1]/a")
    private WebElement home;
    @FindBy(xpath = "//*[@id=\"highlightRowSubtle\"]/td/div/font[1]")
    private WebElement notification1;
    @FindBy(linkText = "Transaction Search")
    private WebElement searchTrans;
    @FindBy(id = "validToTimeHH")
    private WebElement timehour;
    @FindBy(xpath = "//*[@id=\"validToTimeHH\"]/option[24]")
    private WebElement time;
    @FindBy(xpath = "//*[@id=\"statusSelection\"]")
    private WebElement status;
    @FindBy(xpath = "//*[@id=\"statusSelection\"]/option[7]")
    private WebElement auth;
    @FindBy(name = "visibleToVendors")
    private WebElement vendorBox;

    public CorporateMessagesPage clickCorp() {
        ClickControl.click(corporateMgsLink);
        return PageFactory.initElements(webDriver, CorporateMessagesPage.class);
    }

    public CorporateMessagesPage selectDateAndPlaceOrder() {
        String valueFrom = "arguments[0].value = '" + DateTime.now().toString("dd/MM/yyyy") + "'";
        String valueTo = valueFrom;
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) webDriver;
        System.out.print(String.valueOf(LocalDate.now()));
        javascriptExecutor.executeScript(valueFrom, validFromDate);
        javascriptExecutor.executeScript(valueTo, validToDate);
        return PageFactory.initElements(webDriver, CorporateMessagesPage.class);
    }

    public CorporateMessagesPage clickAdd() {
        ClickControl.click(addMessage);
        this.selectDateAndPlaceOrder();
        ClickControl.click(timehour);
        ClickControl.click(time);
        ClickControl.click(corpBox);
        ClickControl.click(orderBox);
        ClickControl.click(vendorBox);
        ClickControl.click(message);
        TextFieldsControl.enterText(message, "Ade Testing");
        ClickControl.click(save);
        return PageFactory.initElements(webDriver, CorporateMessagesPage.class);
    }

    public CorporateMessagesPage displayMgs() {
        ClickControl.click(home);
        return PageFactory.initElements(webDriver, CorporateMessagesPage.class);
    }

    public CorporateMessagesPage searchTransaction() {
        ClickControl.click(searchTrans);
        ClickControl.click(status);
        ClickControl.click(auth);
        scrollToBottomOfPage();
        return PageFactory.initElements(webDriver, CorporateMessagesPage.class);
    }

    public CorporateMessagesPage viewMgs() {
        scrollToBottomOfPage();
        return PageFactory.initElements(webDriver, CorporateMessagesPage.class);
    }

    public String displayedMsg() {
        return notification1.getText();
    }
}



