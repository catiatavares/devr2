package com.optal.CorporateSettingsPage.CorporateProfileSection;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmendCustomFieldsPage extends BasePage {

    @FindBy(linkText = "Amend Custom Fields")
    private WebElement _amendCustomFields;
    @FindBy(id = "addNewMessage")
    private WebElement _addField;
    @FindBy(id = "name")
    private WebElement _name;
    @FindBy(id = "typeSelect")
    private WebElement _typeSelect;
    @FindBy(id = "levelSelect")
    private WebElement _levelSelect;
    @FindBy(xpath = "//li[contains(text(),'Custom Field added successfully')]")
    private WebElement _customFieldAddedSuccessfully;
    @FindBy(xpath = "//a[@class=\"linkclass-columnHeader\"]")
    private WebElement _nameColumnHeader;
    @FindBy(xpath = "//input[@name=\"customField[7].visible\"]")
    private WebElement _visible;
    @FindBy(xpath = "//li[contains(text(),'Custom Field Visibility Updated successfully')]")
    private WebElement _customFieldVisibilityUpdatedSuccessfully;

    public AmendCustomFieldsPage(WebDriver webDriver) {
        super(webDriver);
    }

    public AmendCustomFieldsPage clickOnAmendCustomFields() {
        ClickControl.click(_amendCustomFields);
        return PageFactory.initElements(webDriver, AmendCustomFieldsPage.class);
    }

    public AmendCustomFieldsPage clickOnAddField() {
        ClickControl.click(_addField);
        return PageFactory.initElements(webDriver, AmendCustomFieldsPage.class);
    }

    public AmendCustomFieldsPage enterName() {
        TextFieldsControl.enterText(_name, "Tester" + TextFieldsControl.dateStamp());
        return PageFactory.initElements(webDriver, AmendCustomFieldsPage.class);
    }

    public AmendCustomFieldsPage typeSelect() {
        DropDownControl.selectDropDownByVisibleText(_typeSelect, "Free Text");
        return PageFactory.initElements(webDriver, AmendCustomFieldsPage.class);
    }

    public AmendCustomFieldsPage levelSelect() {
        DropDownControl.selectDropDownByVisibleText(_levelSelect, "HEADER");
        return PageFactory.initElements(webDriver, AmendCustomFieldsPage.class);
    }

    public AmendCustomFieldsPage customFieldVisibilityUpdatedSuccessfully() {
        ClickControl.click(_nameColumnHeader);
        ClickControl.click(_visible);
        return PageFactory.initElements(webDriver, AmendCustomFieldsPage.class);
    }

    public String validateCustomVisibility() {
        return _customFieldVisibilityUpdatedSuccessfully.getText();
    }

    public String validateCustomAddMsg() {
        return _customFieldAddedSuccessfully.getText();
    }

}
