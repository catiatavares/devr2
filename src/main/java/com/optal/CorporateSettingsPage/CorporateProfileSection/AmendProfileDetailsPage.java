package com.optal.CorporateSettingsPage.CorporateProfileSection;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmendProfileDetailsPage extends BasePage {

    public AmendProfileDetailsPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Amend Profile Details")
    private WebElement amendProfile;
    @FindBy(id = "displayName")
    private WebElement displayNameField;
    @FindBy(id = "legalName")
    private WebElement tradeNameField;
    @FindBy(name = "address1")
    private WebElement add1Field;
    @FindBy(name = "address2")
    private WebElement add2Field;
    @FindBy(id = "city")
    private WebElement townField;
    @FindBy(id = "postcode")
    private WebElement pCodeField;
    @FindBy(id = "countrySelection")
    private WebElement country1Field;
    @FindBy(id = "county")
    private WebElement countyField;
    @FindBy(id = "mainContact")
    private WebElement mainContactField;
    @FindBy(id = "contactEmail")
    private WebElement contactEmailField;
    @FindBy(id = "confirmEmail")
    private WebElement confirmEmailField;
    @FindBy(id = "phone")
    private WebElement telNumField;
    @FindBy(id = "mobile")
    private WebElement mobNumField;
    @FindBy(id = "fax")
    private WebElement faxNumField;
    @FindBy(id = "taxCountrySelection")
    private WebElement vatTaxRegCountry;
    @FindBy(id = "taxNo")
    private WebElement vatTaxRegNumField;
    @FindBy(id = "companyNo")
    private WebElement compRegNumField;
    @FindBy(id = "currency")
    private WebElement currencyDropdown;
    @FindBy(id = "countrySelection")
    private WebElement country2;
    @FindBy(xpath = "//span[contains(text(), 'Save Profile')]")
    private WebElement saveProfile;

    public AmendProfileDetailsPage setAmendProfile() {
        ClickControl.click(amendProfile);
        return this;
    }

    public AmendProfileDetailsPage enterDetail(String displayName, String tradeName, String add1, String add2, String town, String pCode,
                                               String county, String mainContact, String contactEmail, String confirmEmail, String telNum, String mobNum, String faxNum, String vatTaxRegCtry,
                                               String vatTaxRegNum, String compRegNum, String currency, String country1, String country2) {
        TextFieldsControl.enterText(displayNameField, displayName);
        TextFieldsControl.enterText(tradeNameField, tradeName);
        TextFieldsControl.enterText(add1Field, add1);
        TextFieldsControl.enterText(add2Field, add2);
        TextFieldsControl.enterText(townField, town);
        TextFieldsControl.enterText(pCodeField, pCode);
        TextFieldsControl.enterText(countyField, county);
        TextFieldsControl.enterText(mainContactField, mainContact);
        TextFieldsControl.enterText(contactEmailField, contactEmail);
        TextFieldsControl.enterText(confirmEmailField, confirmEmail);
        TextFieldsControl.enterText(telNumField, telNum);
        TextFieldsControl.enterText(mobNumField, mobNum);
        TextFieldsControl.enterText(confirmEmailField, confirmEmail);
        TextFieldsControl.enterText(faxNumField, faxNum);
        TextFieldsControl.enterText(confirmEmailField, confirmEmail);
        TextFieldsControl.enterText(telNumField, telNum);
        TextFieldsControl.enterText(mobNumField, mobNum);
        TextFieldsControl.enterText(confirmEmailField, confirmEmail);
        TextFieldsControl.enterText(vatTaxRegCountry, vatTaxRegCtry);
        TextFieldsControl.enterText(vatTaxRegNumField, vatTaxRegNum);
        TextFieldsControl.enterText(compRegNumField, compRegNum);
        DropDownControl.selectDropDownByVisibleText(currencyDropdown, currency);
        DropDownControl.selectDropDownByVisibleText(country1Field, country1);
        DropDownControl.selectDropDownByVisibleText(country1Field, country2);
        return PageFactory.initElements(webDriver, AmendProfileDetailsPage.class);
    }

    public AmendProfileDetailsPage clickSaveButton() {
        ClickControl.click(saveProfile);
        return PageFactory.initElements(webDriver, AmendProfileDetailsPage.class);
    }
}
