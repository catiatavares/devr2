package com.optal.CorporateSettingsPage.FinancialSectionCS;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PCardManagementLodgeCardPage extends BasePage {

    public PCardManagementLodgeCardPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "P-Card Management - Lodge Cards")
    private WebElement pcardmgtlodgecards;
    @FindBy(xpath = "//span[contains(text(),'Add a New Card')]")
    private WebElement addanewcard;
    @FindBy(id = "cardNumber")
    private WebElement cardnos;
    @FindBy(id = "cardCvvNumber")
    private WebElement cvv;
    @FindBy(id = "cardCurrency")
    private WebElement currencyname;
    @FindBy(id = "cardIssuingType")
    private WebElement cardtype;
    @FindBy(id = "cardHolderTitle")
    private WebElement title;
    @FindBy(id = "cardHolderFirstname")
    private WebElement firstname;
    @FindBy(id = "cardHolderSurname")
    private WebElement surname;
    @FindBy(id = "cardExpiryMonth")
    private WebElement expirymonth;
    @FindBy(id = "cardExpiryYear")
    private WebElement expiryyear;
    @FindBy(id = "cardIssueNumber")
    private WebElement issuenos;
    @FindBy(id = "cardLimit")
    private WebElement cclimit;
    @FindBy(id = "description")
    private WebElement nickname;
    @FindBy(id = "informationMessage")
    private WebElement infoMessage;
    @FindBy(xpath = "//li[contains(text(),'P-Card added')]")
    private WebElement addMessage;
    @FindBy(xpath = "//li[contains(text(),'P-Card edited successfully')]")
    private WebElement editMessage;
    @FindBy(xpath = "//li[contains(text(),'P-Card deleted')]")
    private WebElement deleteMessage;

    public PCardManagementLodgeCardPage clickPCardMgtLodgeCardsLink() {
        WebWaits.waitUntilLocatorToBeClickable(pcardmgtlodgecards, 5);
        ClickControl.click(pcardmgtlodgecards);
        return PageFactory.initElements(webDriver, PCardManagementLodgeCardPage.class);
    }

    public PCardManagementLodgeCardPage clickAddANewCard() {
        ClickControl.click(addanewcard);
        return PageFactory.initElements(webDriver, PCardManagementLodgeCardPage.class);
    }

    public String actualResultForNewAddedCard() {
        return addMessage.getText();
    }

    public PCardManagementLodgeCardPage addCardDetailsForm(String cardNosField, String cvvField, String cardCurren, String typeCard, String holderTitle,
                                   String holderFirstname, String holderSurname, String expireMonth, String expireYear,
                                   String issNumber, String limitCard, String describe) {
        TextFieldsControl.enterText(cardnos, cardNosField);
        TextFieldsControl.enterText(cvv, cvvField);
        DropDownControl.selectDropDownByVisibleText(currencyname, cardCurren);
        DropDownControl.selectDropDownByVisibleText(cardtype, typeCard);
        DropDownControl.selectDropDownByVisibleText(title, holderTitle);
        TextFieldsControl.enterText(firstname, holderFirstname);
        TextFieldsControl.enterText(surname, holderSurname);
        DropDownControl.selectDropDownByVisibleText(expirymonth, expireMonth);
        DropDownControl.selectDropDownByVisibleText(expiryyear, expireYear);
        TextFieldsControl.enterText(issuenos, issNumber);
        TextFieldsControl.enterText(cclimit, limitCard);
        TextFieldsControl.enterText(nickname, describe);
        PageFactory.initElements(webDriver, PCardManagementLodgeCardPage.class);
        return PageFactory.initElements(webDriver, PCardManagementLodgeCardPage.class);
    }

    public String actualResultForEditedCard() {
        return editMessage.getText();
    }

    public PCardManagementLodgeCardPage editCardDetailsForm(String cardNosField) {
        TextFieldsControl.enterText(cardnos, cardNosField);
        TextFieldsControl.enterText(cvv, "123");
        DropDownControl.selectDropDownByVisibleText(cardtype, "MasterCard, EuroCard");
        TextFieldsControl.enterText(firstname, "Optal");
        return PageFactory.initElements(webDriver, PCardManagementLodgeCardPage.class);
    }

    public String actualResultForDeleteMessage() {
        return deleteMessage.getText();
    }
}


