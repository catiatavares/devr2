package com.optal.SettingsPage.Financial;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BuyerCardsPage extends BasePage {

    public BuyerCardsPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "P-Card Management - Buyer Cards")
    private WebElement _clickOnPCardManagementBuyerCards;

    public BuyerCardsPage clickOnPCardManagementBuyerCards(){
        ClickControl.click(_clickOnPCardManagementBuyerCards);
        return PageFactory.initElements(webDriver, BuyerCardsPage.class);
    }

}
