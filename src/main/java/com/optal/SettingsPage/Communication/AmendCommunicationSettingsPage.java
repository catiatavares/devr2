package com.optal.SettingsPage.Communication;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmendCommunicationSettingsPage extends BasePage {

    public AmendCommunicationSettingsPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Amend Communication Settings")
    private WebElement comSettings;
    @FindBy(name = "communication[7].emailChecked")
    private WebElement checkbox1;
    @FindBy(name = "communication[12].emailChecked")
    private WebElement checkbox2;
    @FindBy(xpath = "//*[@id=\"informationMessage\"]/div/li")
    private WebElement infoSucess;

    public AmendCommunicationSettingsPage clickComSettings() {
        ClickControl.click(comSettings);
        return PageFactory.initElements(webDriver, AmendCommunicationSettingsPage.class);
    }

    public AmendCommunicationSettingsPage clickCheckbox1() {
        ClickControl.click(checkbox1);
        return PageFactory.initElements(webDriver, AmendCommunicationSettingsPage.class);
    }

    public AmendCommunicationSettingsPage clickCheckbox2() {
        ClickControl.click(checkbox2);
        return PageFactory.initElements(webDriver, AmendCommunicationSettingsPage.class);
    }

    public void assertMessage() {
        WebWaits.waitForNoOfSeconds(5);
        if (infoSucess.isDisplayed()) {
            System.out.println(infoSucess.getText());
        } else {
            System.out.println("FAILED");
        }

    }
}

