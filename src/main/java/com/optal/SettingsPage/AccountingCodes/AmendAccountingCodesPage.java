package com.optal.SettingsPage.AccountingCodes;

import com.optal.HomePage.BasePage;
import com.optal.webControls.BaseControl;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmendAccountingCodesPage extends BasePage {

    public AmendAccountingCodesPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Amend Accounting Codes")
    private WebElement _AmendAccountingCodes;
    @FindBy(id = "cancel")
    private WebElement _Back;
    @FindBy(xpath = "//span[contains(text(),'>>')]//ancestor::div[@id=\"tabs\"]")
    private WebElement _addAll;
    //    @FindBy(xpath = "//h3[contains(text(),'Accounting Codes - testing team')]")
//    public WebElement _AccountingCodesTestingTeam;
    @FindBy(linkText = "Budget Code")
    private WebElement _budgetCode;
    @FindBy(id = "addBudgetCode")
    private WebElement _addBudgetCode;
    @FindBy(id = "addCostCenter")
    private WebElement _addCostCenter;
    @FindBy(id = "addGLCode")
    private WebElement _addGLCode;
    @FindBy(id = "removeAllBudgetCodes")
    private WebElement _removeAllBudgetCodes;
    @FindBy(id = "removeAllCostCenters")
    private WebElement _removeAllCostCenters;
    @FindBy(xpath = "//*[@id=\"removeAllGLCodes\"]")
    private WebElement _removeAllGLCodes;
    @FindBy(id = "save")
    private WebElement _save;
    @FindBy(xpath = "//option[contains(text(),'old codes')]")
    private WebElement _oldCodes;
    @FindBy(xpath = "//li[contains(text(),'Accounting Codes updated successfully')]")
    private WebElement _AccountingCodesUpdatedSuccessfully;
    @FindBy(linkText = "Cost Centre")
    private WebElement _costCentre;
    @FindBy(linkText = "GL Code")
    private WebElement _GLCode;
    @FindBy(xpath = "//*[@id=\"costCenters\"]")
    private WebElement _REGRESSION2CostCentre;
    @FindBy(xpath = "//*[@id=\"glCodes\"]/option[contains(text(),'REGRESSION2')]")
    private WebElement _REGRESSION2GLCodes;
    @FindBy(xpath = "//*[@id=\"costCenters\"]/option[contains(text(),'regression')]")
    private WebElement _regressionCostCentres;
    @FindBy(xpath = "//*[@id=\"glCodes\"]/option[contains(text(),'regression')]")
    private WebElement _regressionGlCodes;

    public AmendAccountingCodesPage clickOnAmendAccountingCodes() {
        ClickControl.click(_AmendAccountingCodes);
        ClickControl.click(_Back);
        ClickControl.click(_AmendAccountingCodes);
        return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);
    }

    public AmendAccountingCodesPage clickOnBudgetCode() {
        ClickControl.click(_budgetCode);
        return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);
    }

    public AmendAccountingCodesPage clickOnAddAll() {
        ClickControl.click(_addAll);
        ClickControl.click(_save);
        return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);
    }

    public AmendAccountingCodesPage clickOnRemoveAllBudgetCodes() {
        if (BaseControl.isElementDisplayed(_removeAllBudgetCodes)) {
            ClickControl.click(_removeAllBudgetCodes);
            ClickControl.click(_save);
            return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);

        } else if (BaseControl.isElementDisplayed(_removeAllCostCenters)) {
            ClickControl.click(_removeAllCostCenters);
            ClickControl.click(_save);
            return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);

        } else if (BaseControl.isElementDisplayed(_removeAllGLCodes)) {
            ClickControl.click(_removeAllGLCodes);
            ClickControl.click(_save);
            return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);

        } else {
            System.out.println("RemoveAll >> element not found");
        }
        return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);
    }

    public AmendAccountingCodesPage moveBackOldCodes() {
        ClickControl.click(_oldCodes);
        ClickControl.click(_addBudgetCode);
        ClickControl.click(_save);
        return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);
    }

    public AmendAccountingCodesPage clickOnCostCentre() {
        ClickControl.click(_costCentre);
        return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);
    }

    public AmendAccountingCodesPage moveBackOldCodeForCostCentre() {
        ClickControl.click(_REGRESSION2CostCentre);
        ClickControl.click(_addCostCenter);
        ClickControl.click(_regressionCostCentres);
        ClickControl.click(_addCostCenter);
        ClickControl.click(_save);
        return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);
    }

    public AmendAccountingCodesPage clickOnGLCode() {
        ClickControl.click(_GLCode);
        return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);
    }

    public AmendAccountingCodesPage moveBackOldForGLCode() {
        ClickControl.click(_regressionGlCodes);
        ClickControl.click(_addGLCode);
        ClickControl.click(_REGRESSION2GLCodes);
        ClickControl.click(_addGLCode);
        ClickControl.click(_save);
        return PageFactory.initElements(webDriver, AmendAccountingCodesPage.class);
    }

    public String validateMsgDisplayed() {
        return _AccountingCodesUpdatedSuccessfully.getText();
    }

}










