package com.optal.SettingsPage.Authorisation;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AuthorisationSummaryPage extends BasePage {

    public AuthorisationSummaryPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Authorisation Summary")
    private WebElement authSummaryLink;
    @FindBy(id = "selectAllAuthorisedUser")
    private WebElement selectAllButton;
    @FindBy(id = "unSelectAllAuthorisedUser")
    private WebElement unSelectAllButton;
    @FindBy(id = "unAssignAllPreAuthorisedUsers")
    private WebElement unAssignAllPreAuthUsers;
    @FindBy(id = "unAssignAllPostAuthorisedUsers")
    private WebElement unAssignAllPostAuthUsers;
    @FindBy(id = "reAssignSelectedUsers")
    private WebElement reAssignSelectedUsers;
    @FindBy(xpath = "//input[@type='checkbox']")
    private WebElement checkbox;
    @FindBy(id = "authorisationUser")
    private WebElement selectUserFromDropdown;
    @FindBy(id = "authorisationUser")
    private WebElement authorisationUser;
    @FindBy(name = "firstNameSearch")
    private WebElement firstNameElement;
    @FindBy(name = "surnameSearch")
    private WebElement lastNameElement;
    @FindBy(linkText = "Modify Current Users")
    private WebElement modifyCurrentUsers;
    @FindBy(linkText = "Amend Transaction Authorisation")
    private WebElement amendTranAuth;

    public AuthorisationSummaryPage addNewUser() {
        ClickControl.click(authSummaryLink);
        return PageFactory.initElements(webDriver, AuthorisationSummaryPage.class);
    }

    public AuthorisationSummaryPage selectALLButtons() {
        ClickControl.click(selectAllButton);
        if (checkbox.isSelected()) {
            System.out.println("Checkbox ticked");
        }
        return PageFactory.initElements(webDriver, AuthorisationSummaryPage.class);
    }

    public AuthorisationSummaryPage unSelectALLButtons() {
        ClickControl.click(unSelectAllButton);
        if (checkbox.isSelected()) {
            System.out.println("Checkbox ticked");
        } else {
            System.out.println("Checkbox unchecked");
        }
        return PageFactory.initElements(webDriver, AuthorisationSummaryPage.class);
    }

    public AuthorisationSummaryPage unAssignAllPreAuthUser() {
        ClickControl.click(unAssignAllPreAuthUsers);

        return PageFactory.initElements(webDriver, AuthorisationSummaryPage.class);
    }

    public AuthorisationSummaryPage unAssignAllPostAuthUser() {
        ClickControl.click(unAssignAllPostAuthUsers);
        return PageFactory.initElements(webDriver, AuthorisationSummaryPage.class);
    }

    public AuthorisationSummaryPage tickCheckbox() {
        ClickControl.click(checkbox);
        return PageFactory.initElements(webDriver, AuthorisationSummaryPage.class);
    }

    public AuthorisationSummaryPage reAssignSelectedUsers() {
        ClickControl.click(reAssignSelectedUsers);
        return PageFactory.initElements(webDriver, AuthorisationSummaryPage.class);
    }

    public AuthorisationSummaryPage selectNewAuthUser(String authUser) {
        DropDownControl.selectDropDownByVisibleText(authorisationUser, authUser);
        return PageFactory.initElements(webDriver, AuthorisationSummaryPage.class);
    }

    public AuthorisationSummaryPage defaultCheckBox() {
        if (checkbox.isSelected()) {
            System.out.println("Default Checkbox is ticked");
        } else {
            System.out.println("Checkbox unchecked");
        }
        return this;
    }

    public AuthorisationSummaryPage userDetails(String firstname, String lastname) {
        TextFieldsControl.enterText(firstNameElement, firstname);
        TextFieldsControl.enterText(lastNameElement, lastname);
        return this;
    }

    public AuthorisationSummaryPage setModifyCurrentUsers() {
        ClickControl.click(modifyCurrentUsers);
        return this;
    }

    public AuthorisationSummaryPage amendTransaction() {
        ClickControl.click(amendTranAuth);
        return this;
    }
}