package com.optal.SettingsPage.Authorisation;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmendTransactionAuthPage extends BasePage {

    @FindBy(id = "addNewAuthorisation")
    private WebElement addAuthorisation;
    @FindBy(id = "authorisationUser")
    private WebElement authorisationUser;
    @FindBy(id = "authorisationType")
    private WebElement authorisationType;
    @FindBy(id = "currencyCode")
    private WebElement currencyCode;
    @FindBy(id = "threshold")
    private WebElement threshold;
    @FindBy(id = "submitAuthorisation")
    private WebElement submitAuthorisation;
    @FindBy(id = "informationMessage")
    private WebElement informationMessage;
    @FindBy(css = "tr:nth-child(3) > td:nth-child(2) > table > tbody > tr:nth-child(2) > td > ul > li:nth-child(1) > a")
    private WebElement transactionAutho;
    @FindBy(xpath = "//*[@id='editAuthorisation459]")
    private WebElement tableXpath;

    public AmendTransactionAuthPage goToTransactionAuthPage() {
        ClickControl.click(transactionAutho);
       // transactionAutho.click();
        return PageFactory.initElements(webDriver, AmendTransactionAuthPage.class);
    }

    public AmendTransactionAuthPage(WebDriver webDriver) {
        super(webDriver);
    }

    public AmendTransactionAuthPage selectAddAuthButton() {
        ButtonControl.clickButton(addAuthorisation);
        return this;
    }

    public AmendTransactionAuthPage addUserAuthorisation(String authUser, String authType, String currency, String cashLimit) {
        DropDownControl.selectDropDownByVisibleText(authorisationUser, authUser);
        DropDownControl.selectDropDownByVisibleText(authorisationType, authType);
        DropDownControl.selectDropDownByVisibleText(currencyCode, currency);
        TextFieldsControl.enterText(threshold, cashLimit);
        ButtonControl.clickButton(submitAuthorisation);
        return this;
    }

    public AmendTransactionAuthPage editUserAuthorisation(String authUser) {
        WebWaits.waitForNoOfSeconds(2);
        DropDownControl.selectDropDownByVisibleText(authorisationUser, authUser);
        return this;
    }

    public boolean validateAddedAuthorisationUser(String message) {
        String addSuccessText = informationMessage.getText();
        return addSuccessText.toLowerCase().contains(message.toLowerCase());
    }

    public AmendTransactionAuthPage deleteAuthorisationUser() {
        for (WebElement closeElement : webDriver.findElements(By.className("button-link-class"))) {
            if (closeElement.getAttribute("id").contains("deleteAuthorisation")) {
                closeElement.click();
                break;
            }
        }
        return this;
    }
}
