package com.optal.SettingsPage.PermissionsRoles;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AssignUserPermissionsPage extends BasePage {

    @FindBy(linkText = "Assign User Permissions")
    private WebElement _assignUserPermissions;
    @FindBy(id = "addAllRoles")
    private WebElement _addAllRoles;
    @FindBy(id = "addRole")
    private WebElement _addRole;
    @FindBy(css = "button#removeAllRoles")
    private WebElement _removeAllRoles;
    @FindBy(xpath = "//span[contains(text(),\"Save\")]")
    private WebElement _save;
    @FindBy(xpath = "//option[contains(text(),\"Administrator\")]")
    private WebElement _administrator;
    @FindBy(xpath = "//li[contains(text(),\"Settings saved successfully\")]")
    private WebElement _successfullyMassage;
    @FindBy(id = "removeRole")
    private WebElement _removeRole;

    public AssignUserPermissionsPage(WebDriver webDriver) {
        super(webDriver);
    }

    public AssignUserPermissionsPage clickOnAssignUserPermissionsButton() {
        ClickControl.click(_assignUserPermissions);
        return this;
    }

    public AssignUserPermissionsPage unAssignAdministrationPermission() {
        ClickControl.click(_administrator);
        ClickControl.click(_removeRole);
        ClickControl.click(_save);
        return this;
    }

    public AssignUserPermissionsPage clickOnAddAllRollsArrow() {
        ClickControl.click(_removeAllRoles);
        ClickControl.click(_save);
        return this;
    }

    public AssignUserPermissionsPage clickOnRemoveAllRolesArrows() {
        ClickControl.click(_addAllRoles);
        ClickControl.click(_save);
        return this;
    }

    public AssignUserPermissionsPage addAdministratorPermissions() {
        ClickControl.click(_addRole);
        ClickControl.click(_save);
        return this;
    }

    public String settingsSavedSuccessfully() {
        return _successfullyMassage.getText();
    }
}
