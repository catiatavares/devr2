package com.optal.CustomerSupportPage.TicketSection;

import com.optal.HomePage.BasePage;
import com.optal.webControls.CheckBoxControl;
import com.optal.webControls.ClickControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RaiseASupportTicketPage extends BasePage {

    public RaiseASupportTicketPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Raise a Support Ticket")
    private WebElement raiseASupportLink;
    @FindBy(id = "subject")
    private WebElement subjectField;
    @FindBy(id = "message")
    private WebElement messageField;
    @FindBy(id = "sendToUser")
    private WebElement checkbox;
    @FindBy(id = "submitTicket")
    private WebElement submitTicket;
    @FindBy(xpath = "//h3[contains(text(),'Ticket Raised')]")
    private WebElement assertText;

    public RaiseASupportTicketPage clickRaiseASupportLink() {
        ClickControl.click(raiseASupportLink);
        return PageFactory.initElements(webDriver, RaiseASupportTicketPage.class);
    }

    public RaiseASupportTicketPage fillTicketDetails(String subjectText, String messageText) {
        TextFieldsControl.enterText(subjectField, subjectText);
        TextFieldsControl.enterText(messageField, messageText);
        CheckBoxControl.clickOnElement(checkbox);
        ClickControl.click(submitTicket);
        return PageFactory.initElements(webDriver, RaiseASupportTicketPage.class);
    }

    public RaiseASupportTicketPage tickCheckboxSumbit() {
        CheckBoxControl.clickOnElement(checkbox);
        ClickControl.click(submitTicket);
        return PageFactory.initElements(webDriver, RaiseASupportTicketPage.class);
    }

    public String assertMessage() {
        return assertText.getText();
    }
}