package com.optal.CustomerSupportPage.FAQSection;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FrequentlyAskedQuestionPage extends BasePage {

    public FrequentlyAskedQuestionPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//th[contains(text(),'FAQ')]/ following ::a[contains(text(),'Frequently Asked Questions')]")
    private WebElement FaqLink;
    @FindBy(xpath = "//h3[contains(text(),'Payment Process')] / following :: li[1]")
    private WebElement howItWorksSection;
    @FindBy(xpath = "//h3[contains(text(),'Payment Process')] / following :: li[3]")
    private WebElement howTtWorksActualText;
    @FindBy(xpath = "//h3[contains(text(),'Payment Process')] / following :: li[6]")
    private WebElement howDoIAcceptOrderSection;
    @FindBy(xpath = "//h3[contains(text(),'Payment Process')] / following :: li[10]")
    public WebElement howDoIAcceptOrderActualText;
    @FindBy(xpath = "//h3[contains(text(),'Payment Process')] / following :: li[14]")
    private WebElement howLongPaymentTakesSection;
    @FindBy(xpath = "//h3[contains(text(),'Payment Process')] / following :: li[15]")
    private WebElement howLongPaymentTakesActualText;
    @FindBy(xpath = "//h3[contains(text(),'Payment Process')] / following :: li[16]")
    private WebElement whereFindDatePaymentAuthorisedSection;
    @FindBy(xpath = "//h3[contains(text(),'Payment Process')] / following :: li[17]")
    private WebElement whereFindDatePaymentAuthorisedActualText;
    @FindBy(xpath = "//h3[contains(text(),'Registration')] / following :: li[1]")
    private WebElement systemDoesNotAcceptTaxSection;
    @FindBy(xpath = "//h3[contains(text(),'Registration')] / following :: li[2]")
    private WebElement systemDoesNotAcceptTaxActualText;
    @FindBy(xpath = "//h3[contains(text(),'Registration')] / following :: li[3]")
    private WebElement iDoNotPayVatTaxSection;
    @FindBy(xpath = "//h3[contains(text(),'Registration')] / following :: li[6]")
    private WebElement iDoNotPayVatTaxActualText;
    @FindBy(xpath = "//h3[contains(text(),'Registration')] / following :: li[9]")
    private WebElement whatIsVendorCatCodeSection;
    @FindBy(xpath = "//h3[contains(text(),'Registration')] / following :: li[13]")
    private WebElement whatIsVendorCatCodeActualText;
    @FindBy(xpath = "//h3[contains(text(),'Payment Status')] / following :: li[1]")
    private WebElement whatDoesNewOrderStatusMeanSection;
    @FindBy(xpath = "//h3[contains(text(),'Payment Status')] / following :: li[2]")
    private WebElement whatDoesNewOrderStatusMeanActualText;
    @FindBy(xpath = "//h3[contains(text(),'Payment Status')] / following :: li[3]")
    private WebElement whatDoesAwaitingDelConfStatusMeanSection;
    @FindBy(xpath = "//h3[contains(text(),'Payment Status')] / following :: li[4]")
    private WebElement whatDoesAwaitingDelConfStatusMeanActualText;
    @FindBy(xpath = "//h3[contains(text(),'Payment Status')] / following :: li[5]")
    private WebElement whatDoesPaymentAprovedMeanSection;
    @FindBy(xpath = "//h3[contains(text(),'Payment Status')] / following :: li[6]")
    private WebElement whatDoesPaymentAprovedMeanActualText;
    @FindBy(xpath = "//h3[contains(text(),'Payment Status')] / following :: li[7]")
    private WebElement whatDoesAwaitingBuyerConfStatusMeanSection;
    @FindBy(xpath = "//h3[contains(text(),'Payment Status')] / following :: li[8]")
    private WebElement whatDoesAwaitingBuyerConfStatusMeanActualText;
    @FindBy(xpath = "//h3[contains(text(),'Other')] / following :: li[1]")
    private WebElement whoIsInvapaySection;
    @FindBy(xpath = "//h3[contains(text(),'Other')] / following :: li[2]")
    private WebElement whoIsIvapayActualText;
    @FindBy(xpath = "//h3[contains(text(),'Other')] / following :: li[3]")
    private WebElement whyPayingfeeSection;
    @FindBy(xpath = "//h3[contains(text(),'Other')] / following :: li[4]")
    private WebElement whyPayingFeeActualText;

    public FrequentlyAskedQuestionPage iClickOnFaqLink() {
        ClickControl.click(FaqLink);
        return this;
    }

    //Payment Process
    public FrequentlyAskedQuestionPage howItWorksSection() {
        ClickControl.click(howItWorksSection);
        if (howTtWorksActualText.isDisplayed()) {
            System.out.println(howTtWorksActualText.getText());
        }
        return this;
    }


    public FrequentlyAskedQuestionPage howDoIAcceptOrderSection() {
        ClickControl.click(howDoIAcceptOrderSection);
        if (howDoIAcceptOrderActualText.isDisplayed()) {
            System.out.println(howDoIAcceptOrderActualText.getText());
        }
        return this;
    }

    public FrequentlyAskedQuestionPage howLongPaymentTakesSection() {
        ClickControl.click(howLongPaymentTakesSection);
        if (howLongPaymentTakesActualText.isDisplayed()) {
            System.out.println(howLongPaymentTakesActualText.getText());
        }
        return this;
    }

    public FrequentlyAskedQuestionPage whereFindDatePaymentAuthorisedSection() {
        ClickControl.click(whereFindDatePaymentAuthorisedSection);
        if (whereFindDatePaymentAuthorisedActualText.isDisplayed()) {
            System.out.println(whereFindDatePaymentAuthorisedActualText.getText());
        }
        return this;
    }

    //Registration
    public FrequentlyAskedQuestionPage systemDoesNotAcceptTaxSection() {
        ClickControl.click(systemDoesNotAcceptTaxSection);
        if (systemDoesNotAcceptTaxActualText.isDisplayed()) {
            System.out.println(systemDoesNotAcceptTaxActualText.getText());
        }
        return this;
    }

    public FrequentlyAskedQuestionPage iDoNotPayVatTaxSection() {
        ClickControl.click(iDoNotPayVatTaxSection);
        if (iDoNotPayVatTaxActualText.isDisplayed()) {
            System.out.println(iDoNotPayVatTaxActualText.getText());
        }
        return this;
    }

    public FrequentlyAskedQuestionPage whatIsVendorCatCodeSection() {
        ClickControl.click(whatIsVendorCatCodeSection);
        if (whatIsVendorCatCodeActualText.isDisplayed()) {
            System.out.println(howLongPaymentTakesActualText.getText());
        }
        return this;
    }

    //Payment Status
    public FrequentlyAskedQuestionPage whatDoesNewOrderStatusMeanSection() {
        ClickControl.click(whatDoesNewOrderStatusMeanSection);
        if (whatDoesNewOrderStatusMeanActualText.isDisplayed()) {
            System.out.println(whatDoesNewOrderStatusMeanActualText.getText());
        }
        return this;
    }

    public FrequentlyAskedQuestionPage whatDoesAwaitingDelConfStatusMeanSection() {
        ClickControl.click(whatDoesAwaitingDelConfStatusMeanSection);
        if (whatDoesAwaitingDelConfStatusMeanActualText.isDisplayed()) {
            System.out.println(whatDoesAwaitingDelConfStatusMeanActualText.getText());
        }
        return this;
    }

    public FrequentlyAskedQuestionPage whatDoesPaymentAprovedMeanSection() {
        ClickControl.click(whatDoesPaymentAprovedMeanSection);
        if (whatDoesPaymentAprovedMeanActualText.isDisplayed()) {
            System.out.println(whatDoesPaymentAprovedMeanActualText.getText());
        }
        return this;
    }

    public FrequentlyAskedQuestionPage whatDoesAwaitingBuyerConfStatusMeanSection() {
        ClickControl.click(whatDoesAwaitingBuyerConfStatusMeanSection);
        if (whatDoesAwaitingBuyerConfStatusMeanActualText.isDisplayed()) {
            System.out.println(whatDoesAwaitingBuyerConfStatusMeanActualText.getText());
        }
        return this;
    }

    //Other
    public FrequentlyAskedQuestionPage whoIsInvapaySection() {
        ClickControl.click(whoIsInvapaySection);
        if (whoIsIvapayActualText.isDisplayed()) {
            System.out.println(whoIsIvapayActualText.getText());
        }
        return this;
    }

    public FrequentlyAskedQuestionPage whyPayingfeeActualSection() {
        ClickControl.click(whyPayingfeeSection);
        if (whyPayingFeeActualText.isDisplayed()) {
            System.out.println(whyPayingFeeActualText.getText());
        }
        return this;
    }
}

