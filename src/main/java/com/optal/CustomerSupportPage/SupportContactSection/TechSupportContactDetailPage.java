package com.optal.CustomerSupportPage.SupportContactSection;

        import com.optal.HomePage.BasePage;
        import com.optal.webControls.ClickControl;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.support.FindBy;

public class TechSupportContactDetailPage extends BasePage {

    public TechSupportContactDetailPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Technical Support Contact Details")
    private WebElement TechnicalSupportLink;
    @FindBy(xpath = "//*[contains(text(), 'Technical Support Contact Details')]")
    public WebElement title;


    public TechSupportContactDetailPage clickSupportLink() {
        ClickControl.click(TechnicalSupportLink);
        return this;
    }

    public String verifyTitle(){
        return title.getText();
    }
}
