package com.optal.CustomerSupportPage.SupportContactSection;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PaymentSupportContactDetailPage extends BasePage {

    public PaymentSupportContactDetailPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Payment Support Contact Details")
    private WebElement PaymentSupportLink;
    @FindBy(xpath = "//*[contains(text(), 'Payment Support Contact Details')]")
    public WebElement title2;

    public PaymentSupportContactDetailPage clickSupportLink() {
        ClickControl.click(PaymentSupportLink);
        return this;
    }

    public String verifyText() {
        return title2.getText();
    }
}

