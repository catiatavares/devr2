package com.optal.CustomerSupportPage.SupportContactSection;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GeneralSupportContactDetailPage extends BasePage{

    public GeneralSupportContactDetailPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "General Support Contact Details")
    private WebElement generalSupportLink;
    @FindBy(xpath = "//*[contains(text(), 'General Support Contact Details')]")
    private WebElement title1;

    public GeneralSupportContactDetailPage clickGeneralLink() {
            ClickControl.click(generalSupportLink);
            return this;
    }

    public String verityText() {
        return title1.getText();
    }
}

