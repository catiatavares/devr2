package com.optal.InviteANewVendorPage;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InviteANewVendorPage extends BasePage {

    public InviteANewVendorPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Invite a New Vendor")
    private WebElement invitevendor;
    @FindBy(id = "vendorName")
    private WebElement name;
    @FindBy(id = "vendorAddress1")
    private WebElement address1;
    @FindBy(id = "vendorAddress2")
    private WebElement address2;
    @FindBy(id = "vendorCity")
    private WebElement city;
    @FindBy(id = "postalCode")
    private WebElement postcode;
    @FindBy(id = "countrySelection")
    private WebElement country1;
    @FindBy(id = "county")
    private WebElement areaState;
    @FindBy(id = "phoneNumber")
    private WebElement telnumber;
    @FindBy(id = "mobileNumber")
    private WebElement mobileNum;
    @FindBy(id = "faxNumber")
    private WebElement faxNumber;
    @FindBy(id = "emailAddress")
    private WebElement email;
    @FindBy(id = "confirmEmail")
    private WebElement emailconfirm;
    @FindBy(id = "inviteVendor")
    private WebElement sendinvitation;
    @FindBy(className = "comment-normal-row")
    private WebElement invitationsent;
    @FindBy(id = "sendInvitation")
    private WebElement contSentInv;
    @FindBy(id = "countrySelection")
    private WebElement country2;
    @FindBy(id = "taxNo")
    private WebElement taxNum;
    @FindBy(xpath = "//span[contains(text(),'Cancel Invitation')]")
    private WebElement cancelInvitation;
    @FindBy(xpath = "//span[contains(text(), 'OK')]")
    private WebElement okButton;
    @FindBy(xpath = "//span[contains(text(),'Resend Invitation')]")
    private WebElement resendInvitation;
    @FindBy(xpath = "//*[@id=\"highlightRow\"]/td[1]")
    private WebElement nameAttribute;
    @FindBy(name = "confirmEmail")
    private WebElement enterEmail;
    @FindBy(xpath = "//*[@id=\"highlightRow\"]/td[9]")
    private WebElement buyerName;

    public InviteANewVendorPage clickInviteVendor() {
        ClickControl.click(invitevendor);
        return PageFactory.initElements(webDriver, InviteANewVendorPage.class);
    }

    public InviteANewVendorPage vendorDetailsForm(String nameField, String addy1, String addy2,
                                                  String cty, String pc, String area,
                                                  String telephoneNum, String mobileNumber, String faxNum,
                                                  String emailAddress, String confirmEmailAddress) {
        TextFieldsControl.enterText(name, nameField);
        TextFieldsControl.enterText(address1, addy1);
        TextFieldsControl.enterText(address2, addy2);
        TextFieldsControl.enterText(city, cty);
        TextFieldsControl.enterText(postcode, pc);
        TextFieldsControl.enterText(areaState, area);
        TextFieldsControl.enterText(telnumber, telephoneNum);
        TextFieldsControl.enterText(mobileNum, mobileNumber);
        TextFieldsControl.enterText(faxNumber, faxNum);
        TextFieldsControl.enterText(email, emailAddress);
        TextFieldsControl.enterText(emailconfirm, confirmEmailAddress);
        return PageFactory.initElements(webDriver, InviteANewVendorPage.class);
    }

    public InviteANewVendorPage selectDropdowndetails(String contry, String contry2) {
        DropDownControl.selectDropDownByVisibleText(country1, contry);
        DropDownControl.selectDropDownByVisibleText(country2, contry2);
        return PageFactory.initElements(webDriver, InviteANewVendorPage.class);
    }

    public InviteANewVendorPage clickSendInvitation() {
        ClickControl.click(sendinvitation);
        ClickControl.click(contSentInv);
        return PageFactory.initElements(webDriver, InviteANewVendorPage.class);
    }

    public boolean validateInvitationSent(String inviteSent) {
        WebWaits.waitForNoOfSeconds(5);
        String InvitationSentMsg = invitationsent.getText();
        return InvitationSentMsg.toLowerCase().contains(inviteSent.toLowerCase());
    }

    public InviteANewVendorPage cancelInvitation() {
        for (int deleteRow = 1; deleteRow <= 1; deleteRow++) {
            cancelInvitation.click();
            okButton.click();
        }
        return PageFactory.initElements(webDriver, InviteANewVendorPage.class);
    }

    public InviteANewVendorPage resendInvitation() {
        for (int deleteRow = 1; deleteRow <= 10; deleteRow++) {
            if (buyerName.getText().contains("Ade Adekugbe")) {
                try {
                    WebWaits.waitForNoOfSeconds(5);
                    ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", resendInvitation);
                    System.out.println("button clicked");
                } catch (Exception e) {
                    System.out.println("button not clicked");
                }
            }
        }
        return PageFactory.initElements(webDriver, InviteANewVendorPage.class);
    }
}

