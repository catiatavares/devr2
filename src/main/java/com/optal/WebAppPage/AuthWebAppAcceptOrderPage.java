package com.optal.WebAppPage;

import com.optal.HomePage.BasePage;
import com.optal.utilities.TestData;
import com.optal.waits.WebWaits;
import com.optal.webControls.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

public class AuthWebAppAcceptOrderPage extends BasePage {

    public AuthWebAppAcceptOrderPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(id = "j_username")
    private WebElement _usernameElement;
    @FindBy(id = "j_password")
    private WebElement _passwordElement;
    @FindBy(id = "bnxt")
    private WebElement _loginButton;
    @FindBy(id = "welcomeMessage")
    private WebElement _welcomeElement;
    @FindBy(css = "td.top.right.pad-5-right > a")
    private WebElement _logOut;
    @FindBy(xpath = "//div[@class=\"clear pent\"]/div/div[1]/div/select")
    private WebElement _firstDigit;
    @FindBy(xpath = "//div[@class=\"clear pent\"]/div/div[2]/div/select")
    private WebElement _secondDigit;
    @FindBy(xpath = "//div[@class=\"navbar-header\"]/a")
    private WebElement _verifyInvapayAuthorisations;
    @FindBy(xpath = " //div[@class=\"transaction-list ng-scope\"]/div[last()-1]//div[7]/button[contains(text(),'Accept')]")
    private WebElement _clickOnAccept;
    @FindBy(xpath = "//div[@class=\"transaction-list ng-scope\"]/div[last()-1]//div[7]/button[@aria-label=\"Accept\" and contains(text(),'Confirm')]")
    private WebElement _clickOnAcceptConfirm;
    @FindBy(xpath = "//div[@class=\"transaction-list ng-scope\"]/div[last()-1]/div[7]/button[@class=\"btn btn-default btn-text ng-hide\" and contains(text(),'Cancel')]")
    private WebElement _clickOnCancel;
    @FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[2]/a")
    private WebElement _clickOnSettings;
    @FindBy(xpath = "//form/div/input[1]")
    private WebElement _clickOnConsolidatedEmails;
    @FindBy(xpath = "//form/div/input[2]")
    private WebElement _clickOnSingleEmails;
    @FindBy(id = "saveSettings")
    private WebElement _clickOnSave;
    @FindBy(xpath = "//*[@id=\"saved\"]/div/strong")
    private WebElement _settingSavedSuccessfully;
    @FindBy(xpath = "//*[@id=\"navbar\"]/ul/li[1]/a")
    private WebElement _clickOnHome;
    @FindBy(xpath = "//div[@id=\"navbar\"]/ul/li[3]/a")
    private WebElement _clickOnLogOut;
    @FindBy(xpath = "//*[@id=\"hoverRow\"]/td[2]/a[contains(text(),'Transaction Search')]")
    private WebElement _clickOnTransactionSearch;

    public AuthWebAppAcceptOrderPage navigateToHomePage(String url) {
        webDriver.get(url);
        System.out.println(webDriver.getTitle());
        return PageFactory.initElements(webDriver, AuthWebAppAcceptOrderPage.class);
    }

    public AuthWebAppAcceptOrderPage loginWebApp() {
        TextFieldsControl.enterText(_usernameElement, TestData.getValue("PreAuthoriser Username"));
        TextFieldsControl.enterText(_passwordElement, TestData.getValue("PreAuthoriser Password"));
        ClickControl.click(_loginButton);
        return PageFactory.initElements(webDriver, AuthWebAppAcceptOrderPage.class);
    }

    public AuthWebAppAcceptOrderPage enterPin() {
        DropDownControl.selectDropDownByValue(_firstDigit,TestData.getValue("PreAuthoriser Pin1"));
        DropDownControl.selectDropDownByValue(_secondDigit, TestData.getValue("PreAuthoriser Pin2"));
        return PageFactory.initElements(webDriver, AuthWebAppAcceptOrderPage.class);
    }

    public String validateInvapayAuthorisations() {
        return CheckBoxControl.getByText(_verifyInvapayAuthorisations);
    }

    public AuthWebAppAcceptOrderPage selectionOfEmail() {
        SoftAssert softAssert = new SoftAssert();
        WebWaits.waitUntilLocatorToBeClickable(_clickOnSettings,5);
        ClickControl.click(_clickOnSettings);
        ClickControl.click(_clickOnSingleEmails);
        ClickControl.click(_clickOnSave);
        WebWaits.waitForTextToBePresent(_settingSavedSuccessfully,"Setting saved successfully");
        softAssert.assertEquals("Setting saved successfully", CheckBoxControl.getByText(_settingSavedSuccessfully), "Error: Single emails not saved.");
        ClickControl.click(_clickOnConsolidatedEmails);
        ClickControl.click(_clickOnSave);
        WebWaits.waitForTextToBePresent(_settingSavedSuccessfully,"Setting saved successfully");
        softAssert.assertEquals("Setting saved successfully", CheckBoxControl.getByText(_settingSavedSuccessfully), "Error: Single emails not saved.");
        ClickControl.click(_clickOnHome);
        softAssert.assertEquals("Invapay Authorisations", CheckBoxControl.getByText(_verifyInvapayAuthorisations), "Error: Use not at home page");
        softAssert.assertAll();
        return PageFactory.initElements(webDriver, AuthWebAppAcceptOrderPage.class);
    }

    public AuthWebAppAcceptOrderPage acceptOrder() {
        WebWaits.waitUntilLocatorToBeClickable(_clickOnAccept,5);
        ClickControl.click(_clickOnAccept);
        WebWaits.waitUntilLocatorToBeClickable(_clickOnAcceptConfirm,5);
        ClickControl.click(_clickOnAcceptConfirm);
        return PageFactory.initElements(webDriver, AuthWebAppAcceptOrderPage.class);
    }

    public AuthWebAppAcceptOrderPage logOut() {
        ClickControl.click(_clickOnLogOut);
        return PageFactory.initElements(webDriver, AuthWebAppAcceptOrderPage.class);
    }

    public AuthWebAppAcceptOrderPage clickOnTransactionSearch(){
        ClickControl.click(_clickOnTransactionSearch);
        return PageFactory.initElements(webDriver, AuthWebAppAcceptOrderPage.class);
    }
}

