package com.optal.WebAppPage;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AuthWebAppRejectOrderPage extends BasePage {

    public AuthWebAppRejectOrderPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//div[@class=\"transaction-list ng-scope\"]/div[last()-1]/div[7]/button[contains(text(),'Reject')]")
    private WebElement _clickOnReject;
    @FindBy(xpath = "//div[@class=\"transaction-list ng-scope\"]/div[last()-1]/div[8]/textarea")
    private WebElement _enterComment;
    @FindBy(xpath = "//div[@class=\"transaction-list ng-scope\"]/div[last()-1]/div[7]/button[@ng-show=\"processing && rejecting\" and contains(text(),'Confirm')]")
    private WebElement _clickOnRejectConfirm;

    public AuthWebAppRejectOrderPage rejectOrder() {
        WebWaits.waitUntilLocatorToBeClickable(_clickOnReject,5);
        ClickControl.click(_clickOnReject);
        TextFieldsControl.enterText(_enterComment, "Testing team rejected your order " + TextFieldsControl.dateStamp());
        WebWaits.waitUntilLocatorToBeClickable(_clickOnRejectConfirm,5);
        ClickControl.click(_clickOnRejectConfirm);
        return PageFactory.initElements(webDriver, AuthWebAppRejectOrderPage.class);
    }

}
