package com.optal.support;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.CodeReusabilityPage.SchemesPage;
import com.optal.CorporateSettingsPage.AccountingCodesSectionCS.AmendBudgetCodePage;
import com.optal.CorporateSettingsPage.AccountingCodesSectionCS.AmendCostCentreCodePage;
import com.optal.CorporateSettingsPage.AccountingCodesSectionCS.AmendGLCodePage;
import com.optal.CorporateSettingsPage.CorporateProfileSection.AmendCustomFieldsPage;
import com.optal.CorporateSettingsPage.CorporateProfileSection.AmendProfileDetailsPage;
import com.optal.CorporateSettingsPage.CorporateProfileSection.CorporateMessagesPage;
import com.optal.CorporateSettingsPage.FinancialSectionCS.PCardManagementLodgeCardPage;
import com.optal.CorporateSettingsPage.PermissionRoleSecurityCS.LoginOptionsPage;
import com.optal.CorporateSettingsPage.PermissionRoleSecurityCS.ManagePermissionRolePage;
import com.optal.CorporateSettingsPage.UserManagementSection.AddNewUserPage;
import com.optal.CorporateSettingsPage.UserManagementSection.ModifyCurrentUserPage;
import com.optal.CustomerSupportPage.FAQSection.FrequentlyAskedQuestionPage;
import com.optal.CustomerSupportPage.SupportContactSection.GeneralSupportContactDetailPage;
import com.optal.CustomerSupportPage.SupportContactSection.PaymentSupportContactDetailPage;
import com.optal.CustomerSupportPage.SupportContactSection.TechSupportContactDetailPage;
import com.optal.CustomerSupportPage.TicketSection.RaiseASupportTicketPage;
import com.optal.End2EndPages.*;
import com.optal.HomePage.*;
import com.optal.InviteANewVendorPage.InviteANewVendorPage;
import com.optal.SettingsPage.AccountingCodes.AmendAccountingCodesPage;
import com.optal.SettingsPage.Authorisation.AmendTransactionAuthPage;
import com.optal.SettingsPage.Authorisation.AuthorisationSummaryPage;
import com.optal.SettingsPage.Communication.AmendCommunicationSettingsPage;
import com.optal.SettingsPage.Financial.BuyerCardsPage;
import com.optal.SettingsPage.PermissionsRoles.AssignUserPermissionsPage;
import com.optal.WebAppPage.AuthWebAppAcceptOrderPage;
import com.optal.WebAppPage.AuthWebAppRejectOrderPage;
import com.optal.browsers.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class WorldHelper {

    private WebDriver driver = WebDriverFactory.getThreadedDriver();
    private static BasePage basePage;
    private static HomePage homePage;
    private static VendorSearchPage vendorSearchPage;
    private static TransactionSearchPage transactionSearchPage;
    private static AcceptOrderViaTransRecordPage acceptOrderViaTransRecordPage;
    private static AmendTransactionAuthPage amendTransactionAuthPage;
    private static AmendGLCodePage amendGLcodePage;
    private static AmendProfileDetailsPage amendProfileDetailsPage;
    private static HoverPage hoverPage;
    private static CreateANewOrderPage createAnewOrderPage;
    private static ForgottenYourPswPage forgottenYourPswPage;
    private static CreateOrderNoVendorPage createOrderNoVendorPage;
    private static RequestANewQuotationpage requestANewQuotationPage;
    private static InviteANewVendorPage inviteANewVendorPage;
    private static YourInvapayOverviewPage yourInvapayOverview;
    private static PCardManagementLodgeCardPage pCardManagementLodgeCardPage;
    private static LoginOptionsPage loginOptionsPage;
    private static ManagePermissionRolePage managePermissionRolePage;
    private static ModifyCurrentUserPage modifyCurrentUserPage;
    private static AddNewUserPage addNewUserPage;
    private static AuthorisationSummaryPage authSummaryPage;
    private static AmendBudgetCodePage amendBudgetCodePage;
    private static AmendCommunicationSettingsPage amendCommunicationSettingsPage;
    private static RaiseASupportTicketPage raiseASupportTicketPage;
    private static AssignUserPermissionsPage assignUserPermissionsPage;
    private static AmendCustomFieldsPage amendCustomFieldsPage;
    private static AmendAccountingCodesPage amendAccountingCodesPage;
    private static CorporateMessagesPage corporateMessagesPage;
    private static CommonObjectPage commonObjectPage;
    private static TechSupportContactDetailPage techSupportContactDetailPage;
    private static PaymentSupportContactDetailPage paymentSupportContactDetailPage;
    private static SchemesPage schemesPage;
    private static AmendOrderByBuyerVendorPage amendOrderByBuyerVendorPage;
    private static RepeatOrderAndCancelOrderPage repeatOrderAndCancelOrderPage;
    private static FrequentlyAskedQuestionPage frequentlyAskedQuestionPage;
    private static BuyerCardsPage buyerCardsPage;
    private static GeneralSupportContactDetailPage generalSupportContactDetailPage;
    private static AlignFRecordWithIRecordPage alignFRecordWithIRecordPage;
    private static AuthoriseAndDeclineIRecordPage authoriseAndCancelIRecordPage;
    private static AuthWebAppAcceptOrderPage authWebAppAcceptOrderPage;
    private static AuthWebAppRejectOrderPage authWebAppRejectOrderPage;

    public AlignFRecordWithIRecordPage getAlignFRecordWithIRecordPage() {
        if (acceptOrderViaTransRecordPage != null) return alignFRecordWithIRecordPage;
        return PageFactory.initElements(driver, AlignFRecordWithIRecordPage.class);
    }

    public BuyerCardsPage getBuyerCardsPage() {
        if (buyerCardsPage != null) return buyerCardsPage;
        return PageFactory.initElements(driver, BuyerCardsPage.class);
    }

    private static AuthoriseOrderFlowPage authoriseOrderFlowPage;
    private static AmendCostCentreCodePage amendCostCentreCodePage;

    public AuthoriseOrderFlowPage getAuthoriseOrderFlowPage() {
        if (authoriseOrderFlowPage != null) return authoriseOrderFlowPage;
        return PageFactory.initElements(driver, AuthoriseOrderFlowPage.class);
    }

    public RepeatOrderAndCancelOrderPage getRepeatOrderAndCancelOrderPage() {
        if (repeatOrderAndCancelOrderPage != null) return repeatOrderAndCancelOrderPage;
        return PageFactory.initElements(driver, RepeatOrderAndCancelOrderPage.class);
    }

    public AmendOrderByBuyerVendorPage getAmendOrderByBuyerVendorPage() {
        if (amendOrderByBuyerVendorPage != null) return amendOrderByBuyerVendorPage;
        return PageFactory.initElements(driver, AmendOrderByBuyerVendorPage.class);
    }

    public CorporateMessagesPage getCorporateMessagePage() {
        if (corporateMessagesPage != null) return corporateMessagesPage;
        return PageFactory.initElements(driver, CorporateMessagesPage.class);
    }

    public AmendAccountingCodesPage getAmendAccountingCodesPage() {
        if (amendAccountingCodesPage != null) return amendAccountingCodesPage;
        return PageFactory.initElements(driver, AmendAccountingCodesPage.class);
    }


    public AmendCustomFieldsPage getamendCustomFieldsPage() {
        if (amendCustomFieldsPage != null) return amendCustomFieldsPage;
        return PageFactory.initElements(driver, AmendCustomFieldsPage.class);
    }


    public BasePage getBasePage() {
        if (basePage != null) return basePage;
        return PageFactory.initElements(driver, BasePage.class);
    }

    public HomePage getAccountHomePage() {
        if (homePage != null) return homePage;
        return PageFactory.initElements(driver, HomePage.class);
    }

    public VendorSearchPage getVendorSearchPage() {
        if (vendorSearchPage != null) return vendorSearchPage;
        return PageFactory.initElements(driver, VendorSearchPage.class);
    }

    public TransactionSearchPage getTransactionSearchPage() {
        if (transactionSearchPage != null) return transactionSearchPage;
        return PageFactory.initElements(driver, TransactionSearchPage.class);
    }

    public AcceptOrderViaTransRecordPage getAcceptOrderPage() {
        if (acceptOrderViaTransRecordPage != null) return acceptOrderViaTransRecordPage;
        return PageFactory.initElements(driver, AcceptOrderViaTransRecordPage.class);
    }

    public AmendTransactionAuthPage getTransactionAuthPage() {
        if (amendTransactionAuthPage != null) return amendTransactionAuthPage;
        return PageFactory.initElements(driver, AmendTransactionAuthPage.class);
    }

    public AmendGLCodePage getAmendGLcodePage() {
        if (amendGLcodePage != null) return amendGLcodePage;
        return PageFactory.initElements(driver, AmendGLCodePage.class);
    }

    public AmendProfileDetailsPage getAmendProfDetailsPage() {
        if (amendProfileDetailsPage != null) return amendProfileDetailsPage;
        return PageFactory.initElements(driver, AmendProfileDetailsPage.class);
    }

    public HoverPage getHoverPage() {
        if (hoverPage != null) return hoverPage;
        return PageFactory.initElements(driver, HoverPage.class);
    }

    public CreateANewOrderPage getCreateANewOrderPage() {
        if (createAnewOrderPage != null) return createAnewOrderPage;
        return PageFactory.initElements(driver, CreateANewOrderPage.class);
    }

    public ForgottenYourPswPage getForgottenYourPswPage() {
        if (forgottenYourPswPage != null) return forgottenYourPswPage;
        return PageFactory.initElements(driver, ForgottenYourPswPage.class);
    }

    public CreateOrderNoVendorPage getCreateANewOrderNoVendorPage() {
        if (createOrderNoVendorPage != null) return createOrderNoVendorPage;
        return PageFactory.initElements(driver, CreateOrderNoVendorPage.class);
    }

    public RequestANewQuotationpage getRequestANewQuotationPage() {
        if (requestANewQuotationPage != null) return getRequestANewQuotationPage();
        return PageFactory.initElements(driver, RequestANewQuotationpage.class);
    }

    public InviteANewVendorPage getRequestANewVendorpage() {
        if (inviteANewVendorPage != null) return inviteANewVendorPage;
        return PageFactory.initElements(driver, InviteANewVendorPage.class);
    }

    public YourInvapayOverviewPage getYourInvapayOverview() {
        if (yourInvapayOverview != null) return yourInvapayOverview;
        return PageFactory.initElements(driver, YourInvapayOverviewPage.class);
    }

    public PCardManagementLodgeCardPage getPCardManagementLodgeCardPage() {
        if (pCardManagementLodgeCardPage != null) return pCardManagementLodgeCardPage;
        return PageFactory.initElements(driver, PCardManagementLodgeCardPage.class);
    }

    public LoginOptionsPage getLoginOptionsPage() {
        if (loginOptionsPage != null) return loginOptionsPage;
        return PageFactory.initElements(driver, LoginOptionsPage.class);
    }

    public ManagePermissionRolePage getPermissionRolePage() {
        if (managePermissionRolePage != null) return managePermissionRolePage;
        return PageFactory.initElements(driver, ManagePermissionRolePage.class);
    }

    public AddNewUserPage getAddNewUserPage() {
        if (addNewUserPage != null) return addNewUserPage;
        return PageFactory.initElements(driver, AddNewUserPage.class);
    }

    public ModifyCurrentUserPage getModifyCurrentUserPage() {
        if (modifyCurrentUserPage != null) return modifyCurrentUserPage;
        return PageFactory.initElements(driver, ModifyCurrentUserPage.class);
    }

    public AuthorisationSummaryPage getAuthSummaryPage() {
        if (authSummaryPage != null) return authSummaryPage;
        return PageFactory.initElements(driver, AuthorisationSummaryPage.class);
    }

    public AmendBudgetCodePage getAmendBudgetCodePage() {
        if (amendBudgetCodePage != null) return amendBudgetCodePage;
        return PageFactory.initElements(driver, AmendBudgetCodePage.class);
    }

    public AmendCommunicationSettingsPage getAmendCommunicationSettingsPage() {
        if (amendCommunicationSettingsPage != null) return amendCommunicationSettingsPage;
        return PageFactory.initElements(driver, AmendCommunicationSettingsPage.class);
    }

    public RaiseASupportTicketPage getRaiseASupportTicketPage() {
        if (raiseASupportTicketPage != null) return raiseASupportTicketPage;
        return PageFactory.initElements(driver, RaiseASupportTicketPage.class);
    }

    public AssignUserPermissionsPage getAssignUserPermissionsPage() {
        if (assignUserPermissionsPage != null) return assignUserPermissionsPage;
        return PageFactory.initElements(driver, AssignUserPermissionsPage.class);
    }

    public CommonObjectPage getCommonObjectPage() {
        if (commonObjectPage != null) return commonObjectPage;
        return PageFactory.initElements(driver, CommonObjectPage.class);
    }

    public TechSupportContactDetailPage getTechSupportContactDetailPage() {
        if (techSupportContactDetailPage != null) return techSupportContactDetailPage;
        return PageFactory.initElements(driver, TechSupportContactDetailPage.class);
    }

    public PaymentSupportContactDetailPage getPaymentSupportContactDetailPage() {
        if (paymentSupportContactDetailPage != null) return paymentSupportContactDetailPage;
        return PageFactory.initElements(driver, PaymentSupportContactDetailPage.class);
    }

    public SchemesPage getSchemesPage() {
        if (schemesPage != null) return schemesPage;
        return PageFactory.initElements(driver, SchemesPage.class);
    }

    public FrequentlyAskedQuestionPage getFrequentlyAskedQuestionPage() {
        if (frequentlyAskedQuestionPage != null) return frequentlyAskedQuestionPage;
        return PageFactory.initElements(driver, FrequentlyAskedQuestionPage.class);
    }

    public AmendCostCentreCodePage getAmendCostCentreCodePage() {
        if (amendCostCentreCodePage != null) return amendCostCentreCodePage;
        return PageFactory.initElements(driver, AmendCostCentreCodePage.class);
    }

    public GeneralSupportContactDetailPage getGeneralSupportContactDetailPage() {
        if (generalSupportContactDetailPage != null) return generalSupportContactDetailPage;
        return PageFactory.initElements(driver, GeneralSupportContactDetailPage.class);
    }

    public AuthoriseAndDeclineIRecordPage getAuthoriseAndCancelIRecordPage() {
        if (authoriseAndCancelIRecordPage != null) return authoriseAndCancelIRecordPage;
        return PageFactory.initElements(driver, AuthoriseAndDeclineIRecordPage.class);
    }

    public AuthWebAppAcceptOrderPage getAuthWebAppAcceptOrderPage() {
        if (authWebAppAcceptOrderPage != null) return authWebAppAcceptOrderPage;
        return PageFactory.initElements(driver, AuthWebAppAcceptOrderPage.class);
    }

    public AuthWebAppRejectOrderPage getAuthWebAppRejectOrderPage() {
        if (authWebAppRejectOrderPage != null) return authWebAppRejectOrderPage;
        return PageFactory.initElements(driver, AuthWebAppRejectOrderPage.class);
    }
}





