package com.optal.HomePage;

import com.optal.End2EndPages.CreateANewOrderPage;
import com.optal.webControls.ClickControl;
import com.optal.webControls.TextFieldsControl;
import com.optal.webControls.DropDownControl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class VendorSearchPage extends BasePage {

    @FindBy(id = "nameSearchCorporate")
    private WebElement nameSearchCorporate;
    @FindBy(id = "buttonSubmitVendorSearch")
    private WebElement buttonSubmitVendorSearch;
    @FindBy(id = "highlightRow")
    private WebElement placeOrder;
    @FindBy(id = "keywordSearchCorporate")
    private WebElement keyword;
    @FindBy(xpath = "//*[contains(text(), 'CorporateVendor')]")
    private WebElement Name;
    @FindBy(xpath = "//*[@id=\"searchCriteria\"]/table/tbody/tr[3]/td[2]/select")
    private WebElement mcc;
    @FindBy(linkText = "FAST FOOD RESTAURANTS")
    private WebElement fast;
    @FindBy(id = "countrySelection")
    private WebElement country;
    @FindBy(linkText = "UNITED KINGDOM")
    private WebElement uk;
    @FindBy(name = "city")
    private WebElement city;

    public VendorSearchPage(WebDriver webDriver) {
        super(webDriver);
    }

    public VendorSearchPage searchForVendor(String vendor) {
        TextFieldsControl.enterText(nameSearchCorporate, vendor);
        ClickControl.click(Name);
        TextFieldsControl.enterText(keyword, "Vendor");
        ClickControl.click(mcc);
        DropDownControl.selectByVisibleText(mcc,"FAST FOOD RESTAURANTS");
        ClickControl.click(country);
        DropDownControl.selectByVisibleText(country,"UNITED KINGDOM");
        TextFieldsControl.enterText(city,"London");
        ClickControl.click(buttonSubmitVendorSearch);
        return this;
    }

    public boolean validateSearch(String vendorName) {
        for (WebElement rowElement : webDriver.findElements(By.id("highlightRow"))) {
            for (WebElement dataElement : rowElement.findElements(By.tagName("td"))) {
                if (!dataElement.getText().toLowerCase().equalsIgnoreCase(vendorName)) continue;
                return true;
            }
        }
        return false;
    }

    public CreateANewOrderPage clickPlaceOrderButton(String value) {
        for (WebElement buttonElement : placeOrder.findElements(By.tagName("td"))) {
            for (WebElement spanElement : buttonElement.findElements(By.tagName("span"))) {
                if (!spanElement.getText().toLowerCase().contains(value)) continue;
                ClickControl.click(spanElement);
                break;
            }
        }
        return PageFactory.initElements(webDriver, CreateANewOrderPage.class);
    }
}
