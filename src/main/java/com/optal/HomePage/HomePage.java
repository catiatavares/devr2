package com.optal.HomePage;

import com.optal.End2EndPages.AcceptOrderViaTransRecordPage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {

    @FindBy(id = "welcomeMessage")
    private WebElement welcomeElement;
    @FindBy(css = "td.top.right.pad-5-right > a")
    private WebElement logOut;
    @FindBy(css = "#hoverRow > td.pad-10-left.pad-10-right > a")
    private WebElement searchElement;
    @FindBy(css = "td.pad-10-left.pad-10-right:nth-child(2) > a.linkclass")
    private WebElement createOrder;
    @FindBy(css = "#maintab > li:nth-child(2) > a")
    private WebElement searchTab;
    @FindBy(css = "#searchOptions > a:nth-child(1)")
    private WebElement transactionSearch;
    @FindBy(css = "#maintab > li:nth-child(7) > a")
    private WebElement settingsTab;
    @FindBy(css = "a:nth-child(2)")
    private WebElement overviewTransactions;
    @FindBy(xpath = "//*[@id=\"maintab\"]/li[5]/a")
    private WebElement corporateSettings;
    @FindBy(linkText = "Date")
    private WebElement dateColumn;

    public HomePage(WebDriver webDriver) {
        super(webDriver);
    }

    public boolean validateLogin(String message) {
        WebWaits.waitForTextToBePresent(welcomeElement, message);
        return welcomeElement.getText().toLowerCase().contains(message.toLowerCase());
    }

    public VendorSearchPage goToVendorSearchPage() {
        ClickControl.click(searchElement);
        return PageFactory.initElements(webDriver, VendorSearchPage.class);
    }

    public VendorSearchPage selectCreateANewOrderLink() {
        ClickControl.click(createOrder);
        return PageFactory.initElements(webDriver, VendorSearchPage.class);
    }

    public TransactionSearchPage goToTransactionSearchPage() {
        Actions action = new Actions(webDriver);
        action.moveToElement(searchTab).moveToElement(transactionSearch).click().build().perform();
        return PageFactory.initElements(webDriver, TransactionSearchPage.class);
    }

    public BasePage logOut() {
        ClickControl.click(logOut);
        return PageFactory.initElements(webDriver, BasePage.class);
    }

    public AcceptOrderViaTransRecordPage selectTransactionsAwaitingYourActions() {
        ClickControl.click(overviewTransactions);
        ClickControl.click(dateColumn);
        ClickControl.click(dateColumn);
        return PageFactory.initElements(webDriver, AcceptOrderViaTransRecordPage.class);
    }
}

