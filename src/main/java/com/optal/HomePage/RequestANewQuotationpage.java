package com.optal.HomePage;

import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RequestANewQuotationpage extends BasePage {

    public RequestANewQuotationpage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Request a new quotation")
    private WebElement requestquote;
    @FindBy(id = "vendorName")
    private WebElement name;
    @FindBy(id = "keywordSearchCorporate")
    private WebElement keyword;
    @FindBy(name = "merchantCategoryCode")
    private WebElement mccCode;
    @FindBy(id = "countrySelection")
    private WebElement contry;
    @FindBy(id = "county")
    private WebElement areastate;
    @FindBy(name = "city")
    private WebElement city;
    @FindBy(xpath = "//span[contains(text(),'View Profile')]")
    private WebElement viewProfile;
    @FindBy(xpath = "//span[contains(text(),'Select Vendor')]")
    private WebElement selectVendor;
    @FindBy(xpath = "//span[contains(text(),'Add Product Items')]")
    private WebElement addProductItems;
    @FindBy(id = "itemName")
    private WebElement itemNamebox;
    @FindBy(id = "quantity")
    private WebElement quantityBox;
    @FindBy(id = "unitOfMeasure0")
    private WebElement uomBox;
    @FindBy(id = "commentInput")
    private WebElement commentBox;
    @FindBy(xpath = "//span[contains(text(),'Add Comment')]")
    private WebElement addCommentBox;
    @FindBy(xpath = "//span[contains(text(),'Request Quote')]")
    private WebElement requestQuoteButton;
    @FindBy(className = "comment-normal-row")
    private WebElement quoteCreationComplete;

    public RequestANewQuotationpage clickRequestQuote() {
        ClickControl.click(requestquote);
        return PageFactory.initElements(webDriver, RequestANewQuotationpage.class);
    }

    public RequestANewQuotationpage vendorSelectionForm(String nameSearch, String keywordSearch, String area, String cty) {
        TextFieldsControl.enterText(name, nameSearch);
        TextFieldsControl.enterText(keyword, keywordSearch);
        TextFieldsControl.enterText(areastate, area);
        TextFieldsControl.enterText(city, cty);
        return this;
    }

    public RequestANewQuotationpage selectDropdowndetails(String mcCode, String contryy) {
        DropDownControl.selectDropDownByVisibleText(mccCode, mcCode);
        DropDownControl.selectDropDownByVisibleText(contry, contryy);
        return this;
    }

    public RequestANewQuotationpage clickViewProfile() {
        ClickControl.click(viewProfile);
        return this;
    }

    public RequestANewQuotationpage clickOnSelectVendor() {
        ClickControl.click(selectVendor);
        WebWaits.waitForNoOfSeconds(5);
        return this;
    }

    public RequestANewQuotationpage clickAddProdItems() {
        ClickControl.click(addProductItems);
        return this;
    }

    public RequestANewQuotationpage addProductItemsForm(String itemNambox, String quantBox, String uomBoxx) {
        TextFieldsControl.enterText(itemNamebox, itemNambox);
        TextFieldsControl.enterText(quantityBox, quantBox);
        TextFieldsControl.enterText(uomBox, uomBoxx);
        return this;
    }

    public RequestANewQuotationpage commentTextBox(String commentBoxText) {
        TextFieldsControl.enterText(commentBox, commentBoxText);
        return this;
    }

    public RequestANewQuotationpage clickAddComment() {
        ClickControl.click(addCommentBox);
        return this;
    }

    public void clickQuoteRequest() {
        ClickControl.click(requestQuoteButton);
        if (quoteCreationComplete.isDisplayed()) {
            System.out.println(quoteCreationComplete.getText());
        }
    }
}


