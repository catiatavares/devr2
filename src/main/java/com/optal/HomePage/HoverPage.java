package com.optal.HomePage;

import com.optal.waits.WebWaits;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HoverPage extends BasePage {

    public HoverPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Order")
    private WebElement orderScheme;
    @FindBy(linkText = "Search")
    private WebElement searchScheme;
    @FindBy(linkText = "Create a new order")
    private WebElement createANewOrderHover;
    @FindBy(linkText = "Request a new quotation")
    private WebElement requestNewQuoteHover;
    @FindBy(linkText = "Create an order with an unregistered vendor")
    private WebElement createOrderUnregVendorHover;
    @FindBy(linkText = "Transaction Search")
    private WebElement transactionSearchHover;
    @FindBy(linkText = "Vendor Search")
    private WebElement VendorSearchHover;
    @FindBy(linkText = "Data File Search")
    private WebElement DataFileSearchHover;
    @FindBy(tagName = "h3")
    private WebElement setDataFileSearchText;
    @FindBy(tagName = "h3")
    private WebElement setNoVendorText;
    @FindBy(tagName = "h3")
    private WebElement setVendorSelectionText;
    @FindBy(name = "VendorSearchForm")
    private WebElement setVendorSearchText;
    @FindBy(tagName = "h3")
    private WebElement setSearchText;
    Actions hover = new Actions(webDriver);

    public HoverPage createANewOrderHover() {
        hover.moveToElement(orderScheme).moveToElement(createANewOrderHover).click().build().perform();
        return PageFactory.initElements(webDriver, HoverPage.class);
    }

    public boolean validateCreateOrderText(String vendorSearchText) {
        WebWaits.waitForNoOfSeconds(5);
        String getVendorSerchText = setVendorSearchText.getText();
        return getVendorSerchText.toLowerCase().contains(vendorSearchText.toLowerCase());
    }

    public HoverPage requestNewQuoteHover() {
        WebWaits.waitForNoOfSeconds(2);
        hover.moveToElement(orderScheme).moveToElement(requestNewQuoteHover).click().build().perform();
        return PageFactory.initElements(webDriver, HoverPage.class);
    }

    public boolean validateRequestNewQuoteText(String validateVendorSelection) {
        WebWaits.waitForNoOfSeconds(5);
        String getVendorSelectionText = setVendorSelectionText.getText();
        return getVendorSelectionText.toLowerCase().contains(validateVendorSelection.toLowerCase());
    }

    public HoverPage createOrderUnregVendorHover() {
        WebWaits.waitForNoOfSeconds(5);
        hover.moveToElement(orderScheme).moveToElement(createOrderUnregVendorHover).click().build().perform();
        return PageFactory.initElements(webDriver, HoverPage.class);
    }

    public boolean validateOrderNoVendor(String validateNoVendorText) {
        WebWaits.waitForNoOfSeconds(5);
        String getOrderNoVendortext = setNoVendorText.getText();
        return getOrderNoVendortext.toLowerCase().contains(validateNoVendorText.toLowerCase());
    }

    public HoverPage transactionSearchHover() {
        WebWaits.waitForNoOfSeconds(2);
        hover.moveToElement(searchScheme).moveToElement(transactionSearchHover).click().build().perform();
        return PageFactory.initElements(webDriver, HoverPage.class);
    }

    public boolean validateTranSearchHover(String validateSearchText) {
        WebWaits.waitForNoOfSeconds(5);
        String getSearchHover = setSearchText.getText();
        return getSearchHover.toLowerCase().contains(validateSearchText.toLowerCase());
    }

    public HoverPage DataFileSearchHover() {
        WebWaits.waitForNoOfSeconds(2);
        hover.moveToElement(searchScheme).moveToElement(DataFileSearchHover).click().build().perform();
        return PageFactory.initElements(webDriver, HoverPage.class);
    }

    public boolean validateDataFileSearch(String validateExportFileText) {
        WebWaits.waitForNoOfSeconds(5);
        String getDataFileSearch = setDataFileSearchText.getText();
        return getDataFileSearch.toLowerCase().contains(validateExportFileText.toLowerCase());
    }

    public HoverPage VendorSearchHover() {
        WebWaits.waitForNoOfSeconds(5);
        hover.moveToElement(searchScheme).moveToElement(VendorSearchHover).click().build().perform();
        return PageFactory.initElements(webDriver, HoverPage.class);
    }

    public boolean validateVendorSearch(String validateVendorSearchText) {
        WebWaits.waitForNoOfSeconds(5);
        String getVendorSearch = setVendorSearchText.getText();
        return getVendorSearch.toLowerCase().contains(validateVendorSearchText.toLowerCase());
    }

    public String assertMessage() {
        setNoVendorText.isDisplayed();
        return setNoVendorText.getText();
    }
}
