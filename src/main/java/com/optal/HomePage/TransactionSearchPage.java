package com.optal.HomePage;

import com.optal.End2EndPages.AcceptOrderViaTransRecordPage;
import com.optal.End2EndPages.CreateANewOrderPage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.OrderControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TransactionSearchPage extends BasePage {


    @FindBy(id = "transactionNoInput")
    private WebElement transactionNo;
    @FindBy(css = "#highlightRow > td:nth-child(1) > a")
    private WebElement orderNumber;
    @FindBy(name = "fromDate")
    private WebElement validFromDate;
    @FindBy(name = "toDate")
    private WebElement validToDate;
    @FindBy(name = "type")
    private WebElement type;
    @FindBy(xpath = "//*[@id=\"type\"]/option[3]")
    private WebElement order;
    @FindBy(name = "name")
    private WebElement name;
    @FindBy(name = "status")
    private WebElement status;
    @FindBy(xpath = "//*[@id=\"statusSelection\"]/option[17]")
    private WebElement approved;
    @FindBy(className = "ui-menu-item")
    private WebElement corp;
    @FindBy(id = "customExportButtonTop")
    private WebElement custom;
    @FindBy(id = "exportDescription")
    private WebElement export;
    @FindBy(id = "transactionNoInput")
    private WebElement input;
    @FindBy(id = "searchCriteriaHeader")
    private WebElement pageAssert;


    public TransactionSearchPage(WebDriver webDriver) {
        super(webDriver);
    }

    public boolean validateTransactionSearch(String transactionNumber) {
        for (WebElement rowElement : webDriver.findElements(By.id("highlightRow"))) {
            for (WebElement dataElement : rowElement.findElements(By.tagName("td"))) {
                if (!dataElement.getText().equalsIgnoreCase(transactionNumber)) continue;
                return true;
            }
        }
        return false;
    }

    public AcceptOrderViaTransRecordPage selectNewOrder() {
        ClickControl.click(orderNumber);
        return PageFactory.initElements(webDriver, AcceptOrderViaTransRecordPage.class);
    }

    public CreateANewOrderPage selectAcceptedOrder() {
        ClickControl.click(orderNumber);
        return PageFactory.initElements(webDriver, CreateANewOrderPage.class);
    }

    public TransactionSearchPage searchForTransaction(String transaction) {
        TextFieldsControl.enterText(transactionNo, transaction);
        return this;
    }

    public TransactionSearchPage searchNewOrder() {
        OrderControl.storeAndSearchOrderNumber(transactionNo);
        return this;
    }

    public TransactionSearchPage otherFields() {
        String valueFrom = "arguments[0].value = '06/11/2018'";
        String valueTo = "arguments[0].value = '06/11/2018'";
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) webDriver;
        javascriptExecutor.executeScript(valueFrom, validFromDate);
        javascriptExecutor.executeScript(valueTo, validToDate);
        ClickControl.click(type);
        ClickControl.click(order);
        ClickControl.click(name);
        TextFieldsControl.enterText(name, "C");
        ClickControl.click(corp);
        ClickControl.click(status);
        scrollToBottomOfPage();
        ClickControl.click(approved);
        return this;
    }

    public TransactionSearchPage customExport() {
        WebWaits.waitForElement(custom);
        ClickControl.click(custom);
        return this;
    }

    public TransactionSearchPage exportDescription() {
        TextFieldsControl.enterText(export, "Testing");
        return this;
    }

    public TransactionSearchPage transactionNumber() {
        ClickControl.click(input);
        TextFieldsControl.enterText(input, "CA001056/F1000");
        return this;
    }

    public String actualResultCustom() {
        return pageAssert.getText();
    }

}
