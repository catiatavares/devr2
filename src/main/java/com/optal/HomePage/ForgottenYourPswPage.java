package com.optal.HomePage;

import com.optal.utilities.TestData;
import com.optal.waits.WebWaits;
import com.optal.webControls.AssertControl;
import com.optal.webControls.ClickControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.List;
import java.util.Set;

public class ForgottenYourPswPage extends BasePage {

    public ForgottenYourPswPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Forgotten your password?")
    private WebElement resetLink;
    @FindBy(name = "username")
    private WebElement usernameField;
    @FindBy(id = "informationMessage")
    private WebElement resetEmailConfirmation;
    @FindBy(xpath = "(//span[@class='ui-icon ui-icon-info'])[1]")
    private WebElement _exclamationMark;
    @FindBy(xpath = "//strong[text()='Information']")
    private WebElement _information;
    @FindBy(xpath = "//div[contains(@class,'ui-state-highlight ui-corner-all pad-5-all')]")
    private WebElement _invapayCustomerSupport;
    @FindBy(id = "identifierId")
    private WebElement _enterEmailAddress;
    @FindBy(xpath = "//input[@type=\"password\" and @autocomplete=\"current-password\" ]")
    private WebElement _enterPassword;
    @FindBy(xpath = "//span[@class='bog']")
    private List<WebElement> _clickOnLink;
    @FindBy(xpath = "//span[@class='gb_xa gbii']")
    private WebElement _profileLogo;
    @FindBy(xpath = "//div[@class=\"G-tF\"]/div[5]")
    private WebElement _refreshEmailBox;
    @FindBy(xpath = "//div[@class=\"gs\"]/div[3]/div[3]/div/div/table/tbody/tr/td/img")
    private WebElement _R2Logo;
    @FindBy(xpath = "//div[@class=\"gs\"]/div[3]/div[3]/div/div/table/tbody/tr/td[2]/table")
    private WebElement _emailHeader;
    @FindBy(xpath = "//div[@class=\"gs\"]/div[3]/div[3]/div/div/table/tbody/tr[2]")
    private WebElement _bodyLine1;
    @FindBy(xpath = "//div[@class=\"gs\"]/div[3]/div[3]/div/div/table/tbody/tr[3]/td")
    private WebElement _emailBody2;
    @FindBy(xpath = "//div[@class=\"gs\"]/div[3]/div[3]/div/div/table/tbody/tr[5]")
    private WebElement _emailBody3;
    @FindBy(xpath = "(//td[@colspan='2'])[5]")
    private WebElement _emailFooter;
    @FindBy(xpath = "//div[@class=\"gs\"]/div[3]/div[3]/div/div/table/tbody/tr[2]/td/a")
    private WebElement _clickOnUrlLink;
    @FindBy(id = "newPassword")
    private WebElement _newPassword;
    @FindBy(id = "passwordConfirm")
    private WebElement _passwordConfirm;
    @FindBy(id = "copy")
    private WebElement _clickOnCopyPassword;
    @FindBy(className = "ui-button-text")
    private WebElement _clickOnSetNewPassword;
    @FindBy(tagName = "li")
    private WebElement _informationReset;
    @FindBy(xpath = "//*[@id=\"informationMessage\"]/div")
    private WebElement _passwordResetMassage;
    @FindBy(id = "securePassword")
    private WebElement _securePassword;
    @FindBy(xpath = "//div/strong[contains(text(),'Validation Errors:')]")
    private WebElement _validationErrors;
    @FindBy(xpath = "//*[@id=\"errorData\"]/div")
    private WebElement _validationErrorsMassage;
    @FindBy(linkText = "Click Here")
    private WebElement _validateClickHere;
    @FindBy(linkText = "Inbox")
    private WebElement _clickOnInbox;
    @FindBy(xpath = "//span[contains(@class,'T-Jo J-J5-Ji')]")
    private WebElement _selectAllEmail;
    @FindBy(xpath = "(//div[@class='asa'])[3]")
    private WebElement _deleteAllEmail;
    @FindBy(id = "userName")
    private WebElement _userName;
    @FindBy(id = "password")
    private WebElement _password;
    @FindBy(id = "buttonSubmitLogin")
    private WebElement _clickOnSign;
    @FindBy(xpath = "//td[contains(@class,'top right')]")
    private WebElement _verifyUserName;

    public ForgottenYourPswPage landingPage() {
        if (resetLink.isDisplayed()) {
            System.out.println("Welcome to Invapay Landing Page");
        }
        return PageFactory.initElements(webDriver, ForgottenYourPswPage.class);
    }

    public ForgottenYourPswPage clickForgottenYourPsw() {
        ClickControl.click(resetLink);
        return PageFactory.initElements(webDriver, ForgottenYourPswPage.class);
    }

    public ForgottenYourPswPage enterUsername() {
        TextFieldsControl.enterText(usernameField, TestData.getValue("user name"));
        return PageFactory.initElements(webDriver, ForgottenYourPswPage.class);
    }

    public ForgottenYourPswPage validateInformationEmailSentMassage() {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(_exclamationMark.isDisplayed(), "Exclamation Mark is not there");
        softAssert.assertEquals(_information.getText(), "Information", "Error: Information word is not there");
        softAssert.assertEquals(_invapayCustomerSupport.getText(), "To recover your Username contact Invapay Customer Support\n" +
                "Tel: +441992500589 or Email: inv.support@optal.com\n" +
                "Support Desk Hours: Monday - Friday 8.30am - 5pm (GMT)\n" +
                "(Exclusive of UK National Holidays and Weekends)", "Error footer massage not showing");
        softAssert.assertAll();
        return PageFactory.initElements(webDriver, ForgottenYourPswPage.class);
    }

    public ForgottenYourPswPage loginGmailAccount() {
        // webDriver.get(TestData.getValue("email url"));
        TextFieldsControl.enterText(_enterEmailAddress, TestData.getValue("user email") + Keys.ENTER);
        WebWaits.waitUntilLocatorToBeClickable(_enterPassword, 60);
        TextFieldsControl.enterText(_enterPassword, TestData.getValue("user password") + Keys.ENTER);
        return PageFactory.initElements(webDriver, ForgottenYourPswPage.class);
    }

    public void clickEmail(String emailSubject) {
        WebWaits.waitForNoOfSeconds(2);
        ClickControl.click(_refreshEmailBox);
        WebWaits.waitForNoOfSeconds(2);
        WebWaits.waitUntilVisibilityOfElement(_profileLogo, 30);
        ClickControl.click(_refreshEmailBox);
        WebWaits.waitForNoOfSeconds(2);
        for (int i = 0; i < _clickOnLink.size(); i++) {
            if (_clickOnLink.get(i).getText().contains(emailSubject)) {
                WebWaits.waitForElements(_clickOnLink);
                _clickOnLink.get(i).click();
                System.out.println("email clicked");
                break;
            }
        }
    }

    public void validateR2Logo() {
        WebWaits.waitForNoOfSeconds(1);
        WebWaits.waitUntilVisibilityOfElement(_R2Logo, 10);
        Assert.assertTrue(_R2Logo.isDisplayed());
        System.out.println("Logo class " + _R2Logo.getAttribute("class"));
    }

    public void validateHeader() {
        WebWaits.waitUntilVisibilityOfElement(_emailHeader, 10);
        WebWaits.waitForNoOfSeconds(1);
        AssertControl.assertByGetTextString(_emailHeader.getText(), "Help Desk Support:\n" +
                "Telephone: +441992500589\n" +
                "Email: inv.support@optal.com\n" +
                "Support Hours: Monday - Friday 8.30am - 5pm", "Error; Header is not correct");
        System.out.println(_emailHeader.getText());
    }

    public void validateBody() {
        WebWaits.waitForNoOfSeconds(1);
        WebWaits.waitUntilVisibilityOfElement(_R2Logo, 15);
        String actualResult = _bodyLine1.getText();
        String actualResultWithoutToken = actualResult.substring(0, actualResult.length() - 32);
        AssertControl.assertByGetTextString(actualResultWithoutToken, "Tester,\n" +
                "\n" +
                "Click the link below to reset your password.\n" +
                "\n" +
                "Click Here\n" +
                "\n" +
                "If the direct link does not work, please copy and paste the link into your browser:\n" +
                "\n" +
                "https://demo.invapay.com/n/resetPassword.do?token=", "First line of body is incorrect");
        System.out.println("\n" + actualResultWithoutToken);
        AssertControl.assertByGetTextString(_emailBody2.getText(), "Regards,\n" +
                "\n" +
                "Invapay Payment Solutions Ltd\n" +
                "http://demo.invapay.com", "Error; Middle of body incorrect");
        System.out.println("\n" + _emailBody2.getText());
        //  System.out.println(urlTokenLink);
        AssertControl.assertByGetTextString(_emailBody3.getText(), "Help Desk Support:\n" +
                "Telephone: +441992500589\n" +
                "Email: inv.support@optal.com\n" +
                "Support Hours: Monday - Friday 8.30am - 5pm (excludes weekends and UK public holidays)", "First line of header is incorrect");
        System.out.println(_emailBody3.getText());
    }

    public void validateEmailFooter() {
        AssertControl.assertByGetTextString(_emailFooter.getText(), "© 2019 Optal Limited. Invapay Payment Solutions Limited is a private limited company registered in England and Wales (no. 06402512) with its registered office at 27/28 Eastcastle Street, London W1W 8DH. Invapay Payment Solutions Limited is authorised as an Authorised Payment Institution by the Financial Conduct Authority under the Payment Services Regulations 2017 (reference no. 631411) and is a wholly-owned subsidiary of Optal Limited.", "Email footer is incorrect");
        System.out.println(_emailFooter.getText());
    }

    public void clickOnLinkWithToken() {
        String parent = webDriver.getWindowHandle();
        ClickControl.click(_clickOnUrlLink);
        System.out.println(_clickOnUrlLink.getText() + "  link with token");
        Set<String> allWindows = webDriver.getWindowHandles();
        int count = allWindows.size();
        System.out.println("Total window is " + count);
        for (String child : allWindows) {
            if (!parent.equalsIgnoreCase(child)) {
                webDriver.switchTo().window(child);
                continueUserJourney();
                webDriver.close();
            }
            webDriver.switchTo().window(parent);
        }
    }

    private static String copyPassword;

    private static String newPassword(WebElement element) {
        copyPassword = element.getText();
        return copyPassword;
    }

    public ForgottenYourPswPage continueUserJourney() {
        TextFieldsControl.enterText(_newPassword, "!W}5p$|-eTest");
        TextFieldsControl.enterText(_passwordConfirm, "!W}5p$|-eTest");
        ClickControl.click(_clickOnCopyPassword);
        newPassword(_securePassword);
        System.out.println("\n" + "New Password " + _securePassword.getText());
        ClickControl.click(_clickOnSetNewPassword);
        AssertControl.assertByGetTextString(_passwordResetMassage.getText(), "Information\n" +
                "\n" +
                "Your password has been reset", "Error: your password has not been reset");
        System.out.println("\n" + _passwordResetMassage.getText() + "\n");
        WebWaits.waitForNoOfSeconds(2);
        return PageFactory.initElements(webDriver, ForgottenYourPswPage.class);
    }

    public ForgottenYourPswPage clickLinkAgain() {
        String parent = webDriver.getWindowHandle();
        ClickControl.click(_clickOnUrlLink);
        Set<String> allWindows = webDriver.getWindowHandles();
        for (String child : allWindows) {
            if (!parent.equalsIgnoreCase(child)) {
                webDriver.switchTo().window(child);
                WebWaits.waitForNoOfSeconds(5);
                AssertControl.assertByGetTextString(_validationErrorsMassage.getText(), "Validation Errors:\n" +
                        "Password change request is invalid", "Error : Please make sure link");
                System.out.println("Validation error through token link\n " + _validationErrorsMassage.getText());
                // webDriver.close();
                webDriver.switchTo().window(parent);
                WebWaits.waitForNoOfSeconds(5);
                ClickControl.click(_validateClickHere);
                webDriver.switchTo().window(child);
                WebWaits.waitForNoOfSeconds(2);
                AssertControl.assertByGetTextString(_validationErrorsMassage.getText(), "Validation Errors:\n" +
                        "Password change request is invalid", "Error : Please make sure link");
                System.out.println("\n" + "Validation error through Click Here \n " + _validationErrorsMassage.getText());
            }
        }
        webDriver.switchTo().window(parent);
        return PageFactory.initElements(webDriver, ForgottenYourPswPage.class);
    }

    public ForgottenYourPswPage deleteAllEmail() {
        ClickControl.click(_clickOnInbox);
        ClickControl.click(_selectAllEmail);
        WebWaits.waitUntilLocatorToBeClickable(_deleteAllEmail, 10);
        ClickControl.click(_deleteAllEmail);
        //webDriver.quit();
        return PageFactory.initElements(webDriver, ForgottenYourPswPage.class);
    }

    public ForgottenYourPswPage loginWithNewPassword() {
        TextFieldsControl.enterText(_userName, TestData.getValue("user name"));
        TextFieldsControl.enterText(_password, copyPassword);
        ClickControl.click(_clickOnSign);
        return PageFactory.initElements(webDriver, ForgottenYourPswPage.class);
    }

    public ForgottenYourPswPage verifyMyAccount() {
        AssertControl.assertByGetTextString(_verifyUserName.getText().substring(0, 12), "Tester Optal", "Error : User name not match");
        System.out.println("Verify login name - " + _verifyUserName.getText().substring(0, 12));
        return PageFactory.initElements(webDriver, ForgottenYourPswPage.class);
    }
}




