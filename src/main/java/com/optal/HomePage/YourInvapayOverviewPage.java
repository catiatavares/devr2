package com.optal.HomePage;

import com.optal.CodeReusabilityPage.CommonObjectPage;
import com.optal.End2EndPages.CreateANewOrderPage;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YourInvapayOverviewPage extends BasePage {


    private CommonObjectPage commonObjectPage;
    CreateANewOrderPage createANewOrderPage;

    public YourInvapayOverviewPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(partialLinkText = "goods receipt")
    private WebElement clickTransGoodReceipt;
    @FindBy(partialLinkText = "CA001056")
    private WebElement clickOrder;
    @FindBy(partialLinkText = "draft transactions")
    private WebElement clickDraftTransaction;
    @FindBy(xpath = "//a[@href='/s/processLink.do?group=corporateHome&linkName=corporateInProgressMeTransaction']")
    private WebElement clickAwaitingYourAction;
    @FindBy(partialLinkText = "in progress")
    private WebElement clickInProgress;
    @FindBy(partialLinkText = "authorisation")
    private WebElement awaitingAuthorisation;
    @FindBy(partialLinkText = "archived transaction")
    private WebElement archivedTransaction;
    @FindBy(partialLinkText = "in error")
    private WebElement inErrorTrans;
    @FindBy(partialLinkText = "rejected/declined")
    private WebElement rejectedDeclined;
    @FindBy (id = "rejectButton")
    private WebElement rejectOrder;
    @FindBy (xpath = "//a[contains(text(), 'Date')]")
    private WebElement clickDate;

    public YourInvapayOverviewPage setTransGoodReceipt() {
        ClickControl.click(clickTransGoodReceipt);
        return PageFactory.initElements(webDriver, YourInvapayOverviewPage.class);
    }

    public YourInvapayOverviewPage setclickOrder() {
        ClickControl.click(clickOrder);
        return PageFactory.initElements(webDriver, YourInvapayOverviewPage.class);
    }

    public YourInvapayOverviewPage setDraftTransaction() {
        ClickControl.click(clickDraftTransaction);
        return PageFactory.initElements(webDriver, YourInvapayOverviewPage.class);
    }

    public YourInvapayOverviewPage setAwaitingYourAction() {
        ClickControl.click(clickAwaitingYourAction);
        return PageFactory.initElements(webDriver, YourInvapayOverviewPage.class);
    }

    public YourInvapayOverviewPage setInProgressTran() {
        ClickControl.click(clickInProgress);
        return PageFactory.initElements(webDriver, YourInvapayOverviewPage.class);
    }

    public YourInvapayOverviewPage setTransAwaitingAuth() {
        ClickControl.click(awaitingAuthorisation);
        return PageFactory.initElements(webDriver, YourInvapayOverviewPage.class);
    }

    public YourInvapayOverviewPage setArchivedTransaction() {
        ClickControl.click(archivedTransaction);
        return PageFactory.initElements(webDriver, YourInvapayOverviewPage.class);
    }

    public YourInvapayOverviewPage setInErrorTransaction() {
        ClickControl.click(inErrorTrans);
        return PageFactory.initElements(webDriver, YourInvapayOverviewPage.class);
    }

    public YourInvapayOverviewPage setRejectedDeclinedTrans() {
        ClickControl.click(rejectedDeclined);
        return PageFactory.initElements(webDriver, YourInvapayOverviewPage.class);
    }

    public YourInvapayOverviewPage rejectOrder() {
        ClickControl.click(rejectOrder);
        return PageFactory.initElements(webDriver, YourInvapayOverviewPage.class);
    }

    public YourInvapayOverviewPage getLatestDate() {
        ClickControl.click(clickDate);
        ClickControl.click(clickDate);
        return PageFactory.initElements(webDriver, YourInvapayOverviewPage.class);
    }
}
