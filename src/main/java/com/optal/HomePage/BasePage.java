package com.optal.HomePage;

import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BasePage {
    protected WebDriver driver;
    protected WebDriver webDriver;
    @FindBy(name = "username")
    private WebElement usernameElement;
    @FindBy(name = "password")
    private WebElement passwordElement;
    @FindBy(id = "buttonSubmitLogin")
    private WebElement loginButton;
    @FindBy(id = "orderDetailsTable")
    private WebElement orderDetailsTable;

    public BasePage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public HomePage loginWith(String username, String password) {
        TextFieldsControl.enterText(usernameElement, username);
        TextFieldsControl.enterText(passwordElement, password);
        ClickControl.click(loginButton);
        return PageFactory.initElements(webDriver, HomePage.class);
    }

    public boolean validateAppUrl(String appUrl) {
        return webDriver.getCurrentUrl().toLowerCase().contains(appUrl.toLowerCase());
    }

    public boolean validateOrderStatus(String orderStatus) {
        WebWaits.waitForNoOfSeconds(1);
        for (WebElement rowElement : orderDetailsTable.findElement(By.tagName("tbody")).findElements(By.tagName("tr"))) {
            for (WebElement dataElement : rowElement.findElements(By.tagName("td"))) {
                if (!dataElement.getText().contains(orderStatus)) continue;
                return true;
            }
        }
        return false;
    }

    public void scrollToBottomOfPage() {
        ((JavascriptExecutor) webDriver)
                .executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }
}
