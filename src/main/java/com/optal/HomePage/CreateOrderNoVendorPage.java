package com.optal.HomePage;

import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateOrderNoVendorPage extends BasePage {

    public CreateOrderNoVendorPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Create an order with an unregistered vendor")
    private WebElement createOrderNoVendorLink;
    @FindBy(id = "vendorName")
    private WebElement nameField;
    @FindBy(id = "vendorAddress1")
    private WebElement address1;
    @FindBy(id = "vendorAddress2")
    private WebElement address2;
    @FindBy(id = "vendorCity")
    private WebElement city;
    @FindBy(id = "postalCode")
    private WebElement postCode;
    @FindBy(id = "county")
    private WebElement areaState;
    @FindBy(id = "phoneNumber")
    private WebElement telephoneNum;
    @FindBy(id = "mobileNumber")
    private WebElement mobileNum;
    @FindBy(id = "faxNumber")
    private WebElement faxNumber;
    @FindBy(id = "emailAddress")
    private WebElement emailAddress;
    @FindBy(id = "confirmEmail")
    private WebElement confirmEmail;
    @FindBy(id = "countrySelection")
    private WebElement country;
    @FindBy(id = "taxCountrySelection")
    private WebElement vatTaxRegCountry;
    @FindBy(id = "taxNo")
    private WebElement vatTaxRegNumber;
    @FindBy(id = "createVendor")
    private WebElement createOrderPlaceOrderButton;
    @FindBy(xpath = "//span[contains(text(),'Delete Vendor')]")
    private WebElement tableRowXpath;
    @FindBy(xpath = "//*[@id=\"placeOrder1962\"]/span[2]")
    private WebElement placeOrderButtonNoVendor;
    @FindBy(tagName = "h3")
    private WebElement asserCreateOrderScreen;

    public CreateOrderNoVendorPage clickCreateOrderNoVendorLink() {
        ClickControl.click(createOrderNoVendorLink);
        return this;
    }

    public CreateOrderNoVendorPage fillFormText(String name, String add1, String add2, String cty, String pcode,
                                                String area, String telNum, String mobNum, String faxNum, String emailAdd, String confEmail) {
        TextFieldsControl.enterText(nameField, name);
        TextFieldsControl.enterText(address1, add1);
        TextFieldsControl.enterText(address2, add2);
        TextFieldsControl.enterText(city, cty);
        TextFieldsControl.enterText(postCode, pcode);
        TextFieldsControl.enterText(areaState, area);
        TextFieldsControl.enterText(telephoneNum, telNum);
        TextFieldsControl.enterText(mobileNum, mobNum);
        TextFieldsControl.enterText(faxNumber, faxNum);
        TextFieldsControl.enterText(emailAddress, emailAdd);
        TextFieldsControl.enterText(confirmEmail, confEmail);
        return this;
    }

    public CreateOrderNoVendorPage selectDropdowndetail(String ctry, String vatTaxRegCtry) {
        DropDownControl.selectDropDownByVisibleText(country, ctry);
        DropDownControl.selectDropDownByVisibleText(vatTaxRegCountry, vatTaxRegCtry);
        return this;
    }

    public CreateOrderNoVendorPage enterFaxAndClickButton(String vatTaxRegNum) {
        TextFieldsControl.enterText(vatTaxRegNumber, vatTaxRegNum);
        ClickControl.click(createOrderPlaceOrderButton);
        return this;
    }

    public CreateOrderNoVendorPage deleteVendor() {
        for (int deleteRow = 1; deleteRow <= 1; deleteRow++) {
            tableRowXpath.click();
        }
        return PageFactory.initElements(webDriver, CreateOrderNoVendorPage.class);
    }

    public CreateOrderNoVendorPage placeOrderNoVendorButton() {
        ClickControl.click(placeOrderButtonNoVendor);
        return PageFactory.initElements(webDriver, CreateOrderNoVendorPage.class);
    }

    public boolean validateCreateOrder(String textDisplayed) {
        String assertText = asserCreateOrderScreen.getText();
        return assertText.toLowerCase().contains(textDisplayed.toLowerCase());
    }
}

