package com.optal.CodeReusabilityPage;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CommonObjectPage extends BasePage {

    public CommonObjectPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//span[contains(text(),'Place Order')]")
    private WebElement placeOrderButton;
    @FindBy(xpath = "//span[contains(text(),'OK')]")
    private WebElement OKButton;
    @FindBy(xpath = "//span[contains(text(),'Ok')}")
    private WebElement OkButton;
    @FindBy(xpath = "//span[contains (text(), 'Save')]")
    private WebElement savebutton;
    @FindBy(xpath = "//*[contains(@type,'submit')]")
    private WebElement submitButton;
    @FindBy(xpath = "//*[contains(@type,'checkbox')]")
    private WebElement checkbox;
    @FindBy(xpath = "//span[contains (text(), 'Edit')]")
    private WebElement editButton;
    @FindBy(xpath = "//span[contains (text(), 'Delete')]")
    private WebElement deleteButton;
    @FindBy(xpath = "//span[contains(text(),'Search')]")
    private WebElement searchButton;
    @FindBy(xpath = "//*[@id=\"highlightRow\"]/td[1]/a")
    private WebElement transNo;
    @FindBy(xpath = "//span[contains (text(), 'Continue')]")
    private WebElement continueButton;
    @FindBy(id = "back")
    private WebElement back;
    @FindBy(linkText = "Home")
    private WebElement homeButton;

    public CommonObjectPage clickPlaceOrderButton() {
        ClickControl.click(placeOrderButton);
        return PageFactory.initElements(webDriver, CommonObjectPage.class);
    }

    public CommonObjectPage clickOKButton() {
        ClickControl.click(OKButton);
        return PageFactory.initElements(webDriver, CommonObjectPage.class);
    }

    public CommonObjectPage clickOkButton() {
        ClickControl.click(OkButton);
        return PageFactory.initElements(webDriver, CommonObjectPage.class);
    }

    public CommonObjectPage clickSaveButton() {
        ClickControl.click(savebutton);
        return PageFactory.initElements(webDriver, CommonObjectPage.class);
    }

    public CommonObjectPage clickSubmitTypeButton() {
        ClickControl.click(submitButton);
        WebWaits.waitForNoOfSeconds(2);
        return PageFactory.initElements(webDriver, CommonObjectPage.class);
    }

    public CommonObjectPage clickEditButton() {
        ClickControl.click(editButton);
        return PageFactory.initElements(webDriver, CommonObjectPage.class);
    }

    public CommonObjectPage clickDeleteButton() {
        ClickControl.click(deleteButton);
        return PageFactory.initElements(webDriver, CommonObjectPage.class);
    }

    public CommonObjectPage clickSearchButton() {
        ClickControl.click(searchButton);
        return this;
    }

    public CommonObjectPage clickFirstTransaction() {
        ClickControl.click(transNo);
        return PageFactory.initElements(webDriver, CommonObjectPage.class);
    }

    public CommonObjectPage clickContinue() {
        ClickControl.click(continueButton);
        return PageFactory.initElements(webDriver, CommonObjectPage.class);
    }

    public CommonObjectPage clickOnBackButton() {
        ClickControl.click(back);
        return PageFactory.initElements(webDriver, CommonObjectPage.class);
    }

    public CommonObjectPage clickHomeScheme(){
        ClickControl.click(homeButton);
        return PageFactory.initElements(webDriver, CommonObjectPage.class);
    }
}
