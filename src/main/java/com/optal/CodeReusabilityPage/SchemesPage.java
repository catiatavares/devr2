package com.optal.CodeReusabilityPage;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SchemesPage extends BasePage {
    public SchemesPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(linkText = "Corporate Settings")
    private WebElement corporatesetting;
    @FindBy(linkText = "Settings")
    private WebElement setting;
    @FindBy(linkText = "Customer Support")
    private WebElement customerSupport;

    public SchemesPage clickCorporateSettings() {
        ClickControl.click(corporatesetting);
        return PageFactory.initElements(webDriver, SchemesPage.class);
    }

    public SchemesPage clickSettingsScheme() {
        ClickControl.click(setting);
        return PageFactory.initElements(webDriver, SchemesPage.class);
    }

    public SchemesPage customerSupport() {
        ClickControl.click(customerSupport);
        return PageFactory.initElements(webDriver, SchemesPage.class);
    }

}
