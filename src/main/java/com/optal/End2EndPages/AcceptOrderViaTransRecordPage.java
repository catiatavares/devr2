package com.optal.End2EndPages;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import com.optal.webControls.OrderControl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AcceptOrderViaTransRecordPage extends BasePage {

    @FindBy(id = "acceptButton")
    private WebElement acceptButton;
    @FindBy(css = "table.fullpage-width.comment-table.table-pad-5-right")
    private WebElement table;

    public AcceptOrderViaTransRecordPage(WebDriver webDriver) {
        super(webDriver);
    }

    public AcceptOrderViaTransRecordPage acceptNewOrder() {
        ClickControl.click(acceptButton);
        return this;
    }

    private void selectNewRow(String orderNo) {
        try {
            for (WebElement tableRow : table.findElement(By.tagName("tbody")).findElements(By.tagName("tr"))) {
                for (WebElement tableRowElement : tableRow.findElements(By.tagName("td"))) {
                    if (!tableRowElement.getText().equalsIgnoreCase(orderNo)) continue;
                    ClickControl.click(tableRowElement);
                    break;
                }
            }
        } catch (org.openqa.selenium.StaleElementReferenceException ex) {
        }
    }

    public AcceptOrderViaTransRecordPage selectARow() {
        String orderNo = OrderControl.storeAndGetOrderNumber();
        this.selectNewRow(orderNo);
        return PageFactory.initElements(webDriver, AcceptOrderViaTransRecordPage.class);
    }
}

