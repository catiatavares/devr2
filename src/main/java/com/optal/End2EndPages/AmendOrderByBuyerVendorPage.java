package com.optal.End2EndPages;

import com.optal.HomePage.BasePage;
import com.optal.utilities.TestData;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmendOrderByBuyerVendorPage extends BasePage {

    public AmendOrderByBuyerVendorPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//button[@id='addTransactionLineBottom']")
    private WebElement _addLine;
    @FindBy(xpath = "//*[@id='saveButton']")
    private WebElement _submitChange;
    @FindBy(xpath = "//input[@id=\"checkSelectionRow1\"] // following:: textarea[@id=\"itemName\"]")
    private WebElement _itemName;
    @FindBy(xpath = " //tr[@id=\"highlightRowNoRollover1\"] // following::input[@id=\"itemCode\"]")
    private WebElement _itemCode;
    @FindBy(xpath = "//tr[@id=\"highlightRowNoRollover1\"] // following::input[@id=\"unitPrice\"]")
    private WebElement _unitPrice;
    @FindBy(xpath = "//tr[@id=\"highlightRowNoRollover1\"] // following::select[@id=\"vatRate\"]")
    private WebElement _vatRate;
    @FindBy(id = "currency")
    private WebElement _Currency;
    @FindBy(xpath = "//td[@id=\"readOnlyLineCodingTableRow1\"] // following:: img[@id=\"toggleImg\"]")
    private WebElement _toggleImg;
    @FindBy(id = "split1")
    private WebElement _split1;
    @FindBy(id = "lineCodeTop1")
    private WebElement _splitLine;
    @FindBy(css = "#codingTable1budgetCode0")
    private WebElement _budgetCode1;
    @FindBy(css = "#codingTable1glCode0")
    private WebElement _glCode1;
    @FindBy(css = "#codingTable1costCenter0")
    private WebElement _costCode1;
    private Actions actions = new Actions(webDriver);
    @FindBy(id = "codingTable1lineSlider0")
    private WebElement _lineSlider1;
    @FindBy(xpath = "//span[@id='ui-dialog-title-generic-dialog-confirm']// following ::span[contains(text(),'OK')]")
    private WebElement _clickOKGeneralLedgerCodeInfo;
    @FindBy(xpath = "//span[contains(text(),'OK')]")
    private WebElement _okButton;
    @FindBy(id = "commentInput")
    private WebElement _commentInput;
    @FindBy(id = "addComment1")
    private WebElement _clickOnAddComment;
    @FindBy(id = "updateButton")
    private WebElement _updateButton;
    @FindBy(id = "authoriseButton")
    private WebElement _clickAccept;
    @FindBy(id = "dispatchTypeFull")
    private WebElement _dispatchTypeFull;
    @FindBy(id = "dispatchGoodsButton")
    private WebElement _dispatchGoodsButton;
    @FindBy(id = "receiptTypeFull")
    private WebElement _clickFullOrderByBuyer;
    @FindBy(id = "authoriseGoodsButton")
    private WebElement _clickReceiptGoodsAndAuthorisePayment;
    @FindBy(id = "codingTable1codeDescription0")
    private WebElement _description;
    @FindBy(css = "#lineCodeTop0 > span.ui-button-text")
    private WebElement splitLine;
    @FindBy(id = "codingTable0glCode0")
    private WebElement _generalLedgerCode;
    @FindBy(xpath = "//span[contains(text(),'OK')]")
    private WebElement _okGLButton;
    @FindBy(id = "codingTable0costCenter0")
    private WebElement _costCentre;
    @FindBy(id = "codingTable0lineSlider0")
    private WebElement _lineSlider;
    @FindBy(id = "codingTable0budgetCode1")
    private WebElement budgetCode1;
    @FindBy(id = "codingTable0glCode1")
    private WebElement _generalLedgerCode1;
    @FindBy(id = "codingTable0costCenter1")
    private WebElement _costCentre1;
    @FindBy(id = "codingTable0lineSlider1")
    private WebElement lineSlider1;
    @FindBy(id = "codingTable0codeDescription1")
    private WebElement description;
    @FindBy(xpath = "/html/body/div[5]/div[3]/div/button/span")
    private WebElement okButton;
    @FindBy(xpath = "//span[contains(text(),'Place Order')]")
    private WebElement _placeOrder;
    @FindBy(id = "selectAdd25")
    private WebElement _selectAddress;
    @FindBy(id = "selectShippingAddress")
    private WebElement _selectShippingAddress;
    @FindBy(xpath = "//button[@id='addTransactionLineTop']")
    private WebElement addLine;
    @FindBy(id = "checkSelectionRow1")
    private WebElement _tickBox;
    @FindBy(xpath = "//button[@id='removeTransactionLineTop']")
    private WebElement _removeLine;
    @FindBy(id = "commentInput")
    private WebElement commentInput;
    @FindBy(id = "addComment1")
    private WebElement clickOnAddComment;
    @FindBy(id = "updateCodingAndCustomFieldsButton")
    private WebElement _clickOnUpdateLineCodingCustomFields;

    public AmendOrderByBuyerVendorPage vendorAddNewOrder(String itemName, String itemCode, String orderPrice) {
        ClickControl.click(_addLine);
        TextFieldsControl.enterText(_itemName, itemName);
        TextFieldsControl.enterText(_itemCode, itemCode);
        TextFieldsControl.enterText(_unitPrice, orderPrice);
        DropDownControl.selectDropDownByVisibleText(_vatRate, "5.000");
        return PageFactory.initElements(webDriver, AmendOrderByBuyerVendorPage.class);
    }

    public AmendOrderByBuyerVendorPage changeCurrency() {
        DropDownControl.selectDropDownByVisibleText(_Currency, TestData.getValue("select currency"));
        //  DropDownControl.selectDropDownByVisibleText(_Currency, "Dollars (United States)");
        //    ClickControl.click(_submitChange);
        return PageFactory.initElements(webDriver, AmendOrderByBuyerVendorPage.class);
    }

    public AmendOrderByBuyerVendorPage submitChange() {
        ClickControl.click(_submitChange);
        return PageFactory.initElements(webDriver, AmendOrderByBuyerVendorPage.class);
    }

    public AmendOrderByBuyerVendorPage splitLine() {
        ClickControl.click(_toggleImg);
        System.out.println("toggleImg click");
        ClickControl.click(_split1);
        ClickControl.click(_splitLine);
        DropDownControl.selectDropDownByVisibleText(_budgetCode1, "regr");
        DropDownControl.selectDropDownByVisibleText(_glCode1, "REGRESSION2");
        ClickControl.click(_clickOKGeneralLedgerCodeInfo);
        //DropDownControl.selectDropDownByVisibleText(_costCode1, "regression");
        WebWaits.waitUntilLocatorToBeClickable(_lineSlider1, 5);
        actions.clickAndHold(_lineSlider1).moveByOffset(100, 100).release().perform();
        TextFieldsControl.enterText(_description, "Add description here");
        try {
            ClickControl.click(_okButton);
        } catch (Throwable t) {
            System.out.println("Element not found");
        }
        return PageFactory.initElements(webDriver, AmendOrderByBuyerVendorPage.class);
    }

    public AmendOrderByBuyerVendorPage updateSubsidyChange() {
        TextFieldsControl.enterText(_commentInput, "Amend vendor and Place repeat order " + TextFieldsControl.dateStamp());
        ClickControl.click(_clickOnAddComment);
        // ClickControl.click(_updateButton);
        ClickControl.click(_clickOnUpdateLineCodingCustomFields);
        ClickControl.click(_clickAccept);
        return PageFactory.initElements(webDriver, AmendOrderByBuyerVendorPage.class);
    }

    public AmendOrderByBuyerVendorPage acceptFullOrderAndDispatchGood() {
        ClickControl.click(_dispatchTypeFull);
        ClickControl.click(_dispatchGoodsButton);
        TextFieldsControl.enterText(_commentInput, "Accept full order and dispatch good  " + TextFieldsControl.dateStamp());
        ClickControl.click(_clickOnAddComment);
        return PageFactory.initElements(webDriver, AmendOrderByBuyerVendorPage.class);
    }

    public AmendOrderByBuyerVendorPage acceptAndReceiptGoodsAndAuthorisePayment() {
        ClickControl.click(_clickFullOrderByBuyer);
        ClickControl.click(_clickReceiptGoodsAndAuthorisePayment);
        TextFieldsControl.enterText(_commentInput, "Hooray end of the test  " + TextFieldsControl.dateStamp());
        ClickControl.click(_clickOnAddComment);
        return PageFactory.initElements(webDriver, AmendOrderByBuyerVendorPage.class);
    }

    public AmendOrderByBuyerVendorPage splitFirstLine() {
        ClickControl.click(splitLine);
        DropDownControl.selectDropDownByVisibleText(_generalLedgerCode, "REGRESSION2");
        ClickControl.click((_okGLButton));
        //DropDownControl.selectDropDownByVisibleText(_costCentre, "regression");
        actions.clickAndHold(_lineSlider).moveByOffset(0, 50).release().perform();
        return PageFactory.initElements(webDriver, AmendOrderByBuyerVendorPage.class);
    }

    public AmendOrderByBuyerVendorPage splitSecondLine() {
        ClickControl.click(splitLine);
        // DropDownControl.selectDropDownByVisibleText(_budgetCode1, "old codes");
        DropDownControl.selectDropDownByVisibleText(budgetCode1, "regr");
        DropDownControl.selectDropDownByVisibleText(_generalLedgerCode1, "REGRESSION2");
        ClickControl.click(_okGLButton);
        //DropDownControl.selectDropDownByVisibleText(_costCentre1, "regression");
        actions.clickAndHold(lineSlider1).moveByOffset(100, 100).release().perform();
        TextFieldsControl.enterText(_description, "Two split line added");
        try {
            ClickControl.click(okButton);
        } catch (Throwable t) {
            System.out.println("Element not found");
        }
        return PageFactory.initElements(webDriver, AmendOrderByBuyerVendorPage.class);
    }

    public AmendOrderByBuyerVendorPage addRemoveLine() {
        ClickControl.click(addLine);
        ClickControl.click(_tickBox);
        ClickControl.click(_removeLine);
        return PageFactory.initElements(webDriver, AmendOrderByBuyerVendorPage.class);
    }

    public AmendOrderByBuyerVendorPage addComment() {
        TextFieldsControl.enterText(commentInput, "Should be comment added  " + TextFieldsControl.dateStamp());
        ClickControl.click(clickOnAddComment);
        return PageFactory.initElements(webDriver, AmendOrderByBuyerVendorPage.class);
    }
}

