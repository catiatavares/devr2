package com.optal.End2EndPages;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import com.optal.webControls.OrderControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateANewOrderPage extends BasePage {
    @FindBy(id = "itemName")
    private WebElement itemName;
    @FindBy(id = "lineAuxFieldDatePicker-transactionLines[0].transactionLineAuxiliaryTransactionFieldList[0]")
    private WebElement datePickerFrom;
    @FindBy(id = "lineAuxFieldDatePicker-transactionLines[0].transactionLineAuxiliaryTransactionFieldList[1]")
    private WebElement datePickerTo;
    @FindBy(id = "unitPrice")
    private WebElement unitPrice;
    @FindBy(id = "glCode0")
    private WebElement glCode;
    @FindBy(xpath = "//span[contains(text(),'OK')]")
    private WebElement okGLButton;
    @FindBy(id = "commodityDescription0")
    private WebElement commodityDescription;
    @FindBy(css = "body > ul:nth-child(5) > li.ui-menu-item")
    private WebElement dropDownOption;
    @FindBy(className = "pad-12-top")
    private WebElement splitLineCoding;
    @FindBy(css = "td > table > tbody > tr:nth-child(3)")
    private WebElement orderDetails;

    @FindBy(xpath = "//td[@class=\"fullpage-height top medium-padding\"]/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr/td/a")
    //@FindBy(css = "tr:nth-child(3) > td > table > tbody > tr > td > a")
    private WebElement orderNumber;
    @FindBy(id = "selectAdd25")
    private WebElement selectAddress;
    @FindBy(id = "selectShippingAddress")
    private WebElement selectShippingAddress;
    @FindBy(css = "#lineCodeTop0 > span.ui-button-text")
    private WebElement splitLine;
    @FindBy(id = "codingTable0budgetCode0")
    private WebElement budgetCode;
    @FindBy(id = "codingTable0glCode0")
    private WebElement generalLedgerCode;
    @FindBy(id = "codingTable0costCenter0")
    private WebElement costCentre;
    @FindBy(id = "codingTable0lineSlider0")
    private WebElement lineSlider;
    @FindBy(id = "codingTable0budgetCode1")
    private WebElement budgetCode1;
    @FindBy(id = "codingTable0glCode1")
    private WebElement generalLedgerCode1;
    @FindBy(id = "codingTable0costCenter1")
    private WebElement costCentre1;
    @FindBy(id = "codingTable0lineSlider1")
    private WebElement lineSlider1;
    private Actions actions = new Actions(webDriver);
    @FindBy(id = "chkReceipted0")
    private WebElement goodReceiptsCheckbox;
    @FindBy(id = "authoriseGoodsButton")
    private WebElement authoriseGoodsButton;
    @FindBy(name = "paymentDataId")
    private WebElement selectCardOption;
    @FindBy(id = "selectPayment")
    private WebElement selectPayment;
    @FindBy(xpath = "/html/body/div[5]/div[3]/div/button/span")
    private WebElement splitLineOkButton;
    @FindBy(xpath = "//td[@class=\" ui-datepicker-days-cell-over  ui-datepicker-today\"]")
    private WebElement _clickOnTodayDate;
    @FindBy(linkText = "P-Card Management - Lodge Cards")
    private WebElement clickPCardLink;

    public CreateANewOrderPage(WebDriver webDriver) {
        super(webDriver);
    }

    public CreateANewOrderPage fillRequiredOrderFields(String orderName, String price, String ledgerCode) {
        TextFieldsControl.enterText(itemName, orderName);
        TextFieldsControl.enterText(unitPrice, price);
        DropDownControl.selectDropDownByVisibleText(glCode, ledgerCode);
        return this;
    }

    public CreateANewOrderPage moveToSplitLineCodingModal() {
        splitLineCoding.click();
        return PageFactory.initElements(webDriver, CreateANewOrderPage.class);
    }

    public CreateANewOrderPage clickPCardLink() {
        ClickControl.click(clickPCardLink);
        return PageFactory.initElements(webDriver, CreateANewOrderPage.class);
    }

    public CreateANewOrderPage selectDateAndPlaceOrder() {
        ClickControl.click(datePickerFrom);
        ClickControl.click(_clickOnTodayDate);
        ClickControl.click(datePickerTo);
        ClickControl.click(_clickOnTodayDate);
        return PageFactory.initElements(webDriver, CreateANewOrderPage.class);
    }

    public void selectCommodityCode(String commText) {
        String codeToSearch = "Abc";
        commodityDescription.sendKeys(codeToSearch);
        DropDownControl.selectDropDownByVisibleText(commodityDescription, commText);
        dropDownOption.click();
    }

    public String getOrderNumber() {
        return OrderControl.getOrderNumber(orderDetails);
    }

    public CreateANewOrderPage clickOrderNumber() {
        ClickControl.click(orderNumber);
        return PageFactory.initElements(webDriver, CreateANewOrderPage.class);
    }

    public CreateANewOrderPage selectAddress() {
        ClickControl.click(selectAddress);
        ClickControl.click(selectShippingAddress);
        return PageFactory.initElements(webDriver, CreateANewOrderPage.class);
    }

    private void splitFirstLine(String budget, String ledger, String cost) {
        ClickControl.click(splitLine);
        DropDownControl.selectDropDownByVisibleText(budgetCode, budget);
        DropDownControl.selectDropDownByVisibleText(generalLedgerCode, ledger);
        ClickControl.click(okGLButton);
        DropDownControl.selectDropDownByVisibleText(costCentre, cost);
        actions.clickAndHold(lineSlider).moveByOffset(0, 50).release().perform();
    }

    private void splitSecondLine(String budget1, String ledger1, String cost1) {
        ClickControl.click(splitLine);
        DropDownControl.selectDropDownByVisibleText(budgetCode1, budget1);
        DropDownControl.selectDropDownByVisibleText(generalLedgerCode1, ledger1);
        ClickControl.click(okGLButton);
        DropDownControl.selectDropDownByVisibleText(costCentre1, cost1);
        actions.clickAndHold(lineSlider1).moveByOffset(100, 100).release().perform();
    }

    public CreateANewOrderPage splitLineAndClickOk(String budget, String ledger, String cost, String budget1, String ledger1, String cost1) {
        this.splitFirstLine(budget, ledger, cost);
        this.splitSecondLine(budget1, ledger1, cost1);
        try {
            splitLineOkButton.click();
        } catch (Throwable t) {
            System.out.println("Element not found");
        }
        return PageFactory.initElements(webDriver, CreateANewOrderPage.class);
    }

    public CreateANewOrderPage authorisePayment() {
        goodReceiptsCheckbox.click();
        ClickControl.click(authoriseGoodsButton);
        return this;
    }

    public CreateANewOrderPage selectCard() {
        ClickControl.click(selectCardOption);
        ClickControl.click(selectPayment);
        return PageFactory.initElements(webDriver, CreateANewOrderPage.class);
    }
}


