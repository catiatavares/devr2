package com.optal.End2EndPages;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AuthoriseOrderFlowPage extends BasePage {
    public AuthoriseOrderFlowPage(WebDriver webDriver) {
        super(webDriver);
    }


    @FindBy(className = "linkclass")
    private WebElement transactionNos;
    @FindBy(id = "authoriseTransactionButton")
    private WebElement authoriseButton;
    @FindBy(id = "welcomeMessage")
    private WebElement welcomegreeting;
    @FindBy(id="submitSubsidyChanges")
    private WebElement subsidy;
    @FindBy(id = "feeSubsidyInput")
    private WebElement feeInput;
    @FindBy(id = "rejectButton")
    private WebElement declineButton;
    @FindBy(id = "rejectionCommentInput")
    private WebElement declineComment;
    @FindBy(id = "rejectOrderComment1")
    private WebElement rejectOrder;


    public AuthoriseOrderFlowPage clickTransactionNos() {
        ClickControl.click(transactionNos);
        return PageFactory.initElements(webDriver, AuthoriseOrderFlowPage.class);
    }

    public AuthoriseOrderFlowPage clickSubsidyChanges(){
        WebWaits.waitUntilLocatorToBeClickable(feeInput,10);
        TextFieldsControl.enterText(feeInput,"100");
        ClickControl.click(subsidy);
        scrollToBottomOfPage();
        return PageFactory.initElements(webDriver, AuthoriseOrderFlowPage.class);
    }

    public AuthoriseOrderFlowPage clickAuthorise(){
        WebWaits.waitUntilLocatorToBeClickable(authoriseButton,10);
        ClickControl.click(authoriseButton);
        return PageFactory.initElements(webDriver, AuthoriseOrderFlowPage.class);
    }
    public AuthoriseOrderFlowPage clickDecline() {
        WebWaits.waitUntilLocatorToBeClickable(declineButton, 10);
        ClickControl.click(declineButton);
        return PageFactory.initElements(webDriver, AuthoriseOrderFlowPage.class);
    }

    public AuthoriseOrderFlowPage clickReject(){
        WebWaits.waitUntilLocatorToBeClickable(declineComment,10);
        TextFieldsControl.enterText(declineComment,"Authorisation Declined");
        ClickControl.click(rejectOrder);
        return PageFactory.initElements(webDriver, AuthoriseOrderFlowPage.class);
    }
}
