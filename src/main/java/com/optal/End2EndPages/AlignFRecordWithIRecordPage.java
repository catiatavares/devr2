package com.optal.End2EndPages;

import com.optal.HomePage.BasePage;
import com.optal.utilities.TestData;
import com.optal.waits.WebWaits;
import com.optal.webControls.CheckBoxControl;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

public class AlignFRecordWithIRecordPage extends BasePage {

    public AlignFRecordWithIRecordPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(id = "addTransactionLineTop")
    private WebElement _clickOnAddTransactionLineTop;
    @FindBy(id = "glCode1")
    private WebElement _glCode1;
    @FindBy(xpath = "//div[@id=\"generic-dialog-confirm\"] / following :: button")
    private WebElement _clickOnOK;
    @FindBy(id = "lineAuxFieldDatePicker-transactionLines[1].transactionLineAuxiliaryTransactionFieldList[0]")
    private WebElement _datePickerFrom;
    @FindBy(id = "lineAuxFieldDatePicker-transactionLines[1].transactionLineAuxiliaryTransactionFieldList[1]")
    private WebElement _datePickerTo;
    @FindBy(xpath = "//td[@class=\" ui-datepicker-days-cell-over  ui-datepicker-today\"]")
    private WebElement _clickOnTodayDate;
    @FindBy(id = "chkReceipted0")
    private WebElement _clickOnFirstTransaction;
    @FindBy(id = "chkReceipted1")
    private WebElement _clickOnSecondTransaction;
    @FindBy(id = "authoriseGoodsButton")
    private WebElement _clickReceiptGoodsAndAuthorisePayment;
    @FindBy(xpath = "//div[@id=\"generic-dialog-confirm\"] / following :: span[contains(text(),'OK')]")
    private WebElement _clickOnOKButton;
    @FindBy(xpath = "//div/a[contains(text(),'2 Transaction Records')]")
    private WebElement _clickOn2TransactionRecords;
    @FindBy(xpath = "//td/h3")
    private WebElement _associatedTransactionRecord;
    @FindBy(xpath = "//tbody/tr[2]/td[1]/a")
    private WebElement _clickOnFirstIRecord;
    @FindBy(xpath = "//tbody/tr[4]/td[1]/a")
    private WebElement _clickOnSecondIRecord;
    @FindBy(xpath = "//div/a[contains(text(),'1 Transaction Record')]")
    private WebElement _clickOn1TransactionRecord;
    @FindBy(xpath = "//div[@id=\"generic-dialog-confirm\"] / following :: span[contains(text(),'OK')]")
    private WebElement _ClickOnOKButton;
    @FindBy(xpath = "//table[@class='fullpage-width comment-table table-pad-5-right']/tbody/tr[2]/td[1]/a")
    private WebElement _getTransactionNo;
    @FindBy(xpath = "//table[@class='fullpage-width comment-table table-pad-5-right']/tbody/tr[2]/td[3]/a")
    private WebElement _getOriginalTransactionNo;
    @FindBy(xpath = "//table[@class='fullpage-width comment-table table-pad-5-right']/tbody/tr[2]/td[8]")
    private WebElement _getGross;
    @FindBy(xpath = "//table[@class='fullpage-width comment-table table-pad-5-right']/tbody/tr[2]/td[9]")
    private WebElement _getStatus;
    @FindBy(xpath = "//tbody/tr[2]/td[9]")
    private WebElement _waitForFirstRecordsStatusArchived;
    @FindBy(xpath = "//tbody/tr[4]/td[9]")
    private WebElement _waitForSecondRecordsStatusArchived;
    @FindBy(xpath = "//div/a[contains(text(),'1 Order')]")
    private WebElement _clickOn1Order;
    @FindBy(xpath = "//tr[@id=\"highlightRow\"]/td/a[contains(text(),'CA')]")
    private WebElement _clickOnAssociatedOrders;

    public AlignFRecordWithIRecordPage addGLCode() {
        WebWaits.waitUntilLocatorToBeClickable(_glCode1, 5);
        DropDownControl.selectDropDownByVisibleText(_glCode1, "regression");
        //regression    REGRESSION2
        ClickControl.click(_clickOnOK);
        //commonObjectPage.clickOKButton();
        return PageFactory.initElements(webDriver, AlignFRecordWithIRecordPage.class);
    }

    public AlignFRecordWithIRecordPage selectDateAddLineAuxiliaryDetails() {
        ClickControl.click(_datePickerFrom);
        ClickControl.click(_clickOnTodayDate);
        ClickControl.click(_datePickerTo);
        ClickControl.click(_clickOnTodayDate);
        return PageFactory.initElements(webDriver, AlignFRecordWithIRecordPage.class);
    }

    public AlignFRecordWithIRecordPage acceptFirstPartialOrder() {
        ClickControl.click(_clickOnFirstTransaction);
        return PageFactory.initElements(webDriver, AlignFRecordWithIRecordPage.class);
    }

    public AlignFRecordWithIRecordPage acceptSecondPartialOrder() {
        ClickControl.click(_clickOnSecondTransaction);
        return PageFactory.initElements(webDriver, AlignFRecordWithIRecordPage.class);
    }

    public AlignFRecordWithIRecordPage issueReceiptGoodsAndAuthorisePayment() {
        ClickControl.click(_clickReceiptGoodsAndAuthorisePayment);
        return PageFactory.initElements(webDriver, AlignFRecordWithIRecordPage.class);
    }

    public AlignFRecordWithIRecordPage verifyingFIRecordForFirstOrder() {
        SoftAssert softAssertion = new SoftAssert();
        softAssertion.assertEquals("2 Transaction Records", CheckBoxControl.getByText(_clickOn2TransactionRecords), "error : Please check how may record created");
        WebWaits.waitForTextToBePresent(_clickOn2TransactionRecords, "2 Transaction Records");
        ClickControl.click(_clickOn2TransactionRecords);
        ClickControl.click(_clickOnOKButton);
        softAssertion.assertEquals("Associated Transaction Records", CheckBoxControl.getByText(_associatedTransactionRecord), "Error : redirect wrong page");
        WebWaits.waitForNoOfSeconds(3);
        webDriver.navigate().refresh();
        WebWaits.waitForTextToBePresent(_waitForFirstRecordsStatusArchived, "Archived");
        WebWaits.waitForNoOfSeconds(3);
        webDriver.navigate().refresh();
        WebWaits.waitForTextToBePresent(_waitForSecondRecordsStatusArchived, "Archived");
        webDriver.navigate().refresh();
        ClickControl.click(_clickOnFirstIRecord);
        WebWaits.waitForNoOfSeconds(2);
        webDriver.navigate().refresh();
        WebWaits.waitForTextToBePresent(_clickOn1TransactionRecord, "1 Transaction Record");
        ClickControl.click(_clickOn1TransactionRecord);
        ClickControl.click(_ClickOnOKButton);
        softAssertion.assertEquals("Associated Transaction Records", CheckBoxControl.getByText(_associatedTransactionRecord), "Error : redirect wrong page");
        softAssertion.assertEquals("F", CheckBoxControl.getByText(_getTransactionNo).substring(9, 10), "Error : First F record not generated");
        System.out.println("First F transaction No - " + CheckBoxControl.getByText(_getTransactionNo));
        softAssertion.assertEquals("I", CheckBoxControl.getByText(_getOriginalTransactionNo).substring(9, 10), "Error : First I record not generated");
        System.out.println("First I transaction No - " + CheckBoxControl.getByText(_getOriginalTransactionNo));
        softAssertion.assertEquals(TestData.getValue("IRecord GrossValue"), CheckBoxControl.getByText(_getGross).substring(0,1), "Error : First Gross value must be in another  currency");
        System.out.println("First Gross Value currency - " + CheckBoxControl.getByText(_getGross).substring(0,1));
        System.out.println("First Gross Value - " + _getGross.getText());
        WebWaits.waitForNoOfSeconds(2);
        webDriver.navigate().refresh();
        WebWaits.waitForNoOfSeconds(2);
        webDriver.navigate().refresh();
        WebWaits.waitForTextToBePresent(_getStatus, "Archived");
        webDriver.navigate().refresh();
        softAssertion.assertEquals("Archived", _getStatus.getText(), "error : Please check status");
        softAssertion.assertEquals("Associated Transaction Records", CheckBoxControl.getByText(_associatedTransactionRecord), "Error : redirect wrong page");
        softAssertion.assertAll();
        return PageFactory.initElements(webDriver, AlignFRecordWithIRecordPage.class);
    }

    public AlignFRecordWithIRecordPage verifyingFIRecordForSecondOrder() {
        SoftAssert softAssertion = new SoftAssert();
        webDriver.navigate().back();
        ClickControl.click(_clickOn1Order);
        ClickControl.click(_ClickOnOKButton);
        ClickControl.click(_clickOnAssociatedOrders);
        ClickControl.click(_clickOn2TransactionRecords);
        ClickControl.click(_ClickOnOKButton);
        ClickControl.click(_clickOnSecondIRecord);
        ClickControl.click(_clickOn1TransactionRecord);
        ClickControl.click(_ClickOnOKButton);
        softAssertion.assertEquals("Associated Transaction Records", CheckBoxControl.getByText(_associatedTransactionRecord), "Error : redirect wrong page");
        softAssertion.assertEquals("F", CheckBoxControl.getByText(_getTransactionNo).substring(9, 10), "Error: Second F record not generated");
        System.out.println("Second F transaction No - " + CheckBoxControl.getByText(_getTransactionNo));
        softAssertion.assertEquals("I", CheckBoxControl.getByText(_getOriginalTransactionNo).substring(9, 10), "Error: Second  I record not generated");
        System.out.println("Second I transaction No - " + CheckBoxControl.getByText(_getOriginalTransactionNo));
        softAssertion.assertEquals(TestData.getValue("IRecord GrossValue"), CheckBoxControl.getByText(_getGross).substring(0,1), "Error : Second Gross value must be in another  currency");
        System.out.println("Second Gross Value currency - " + CheckBoxControl.getByText(_getGross).substring(0,1));
        System.out.println("Second Gross Value - " + _getGross.getText());
        WebWaits.waitForNoOfSeconds(3);
        webDriver.navigate().refresh();
        WebWaits.waitForNoOfSeconds(3);
        webDriver.navigate().refresh();
        WebWaits.waitForTextToBePresent(_getStatus, "Archived");
        webDriver.navigate().refresh();
        softAssertion.assertEquals("Archived", _getStatus.getText(), "error : Please check status");
        softAssertion.assertAll();
        return PageFactory.initElements(webDriver, AlignFRecordWithIRecordPage.class);
    }

}
