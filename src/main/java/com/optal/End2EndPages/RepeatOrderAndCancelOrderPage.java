package com.optal.End2EndPages;

import com.optal.HomePage.BasePage;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import com.optal.webControls.OrderControl;
import com.optal.webControls.TextFieldsControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RepeatOrderAndCancelOrderPage extends BasePage {

    public RepeatOrderAndCancelOrderPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(id = "back")
    private WebElement _clickOnBack;
    @FindBy(id = "repeatOrderButton")
    private WebElement _clickOnRepeatOrderButton;
    @FindBy(css = "tbody > tr:nth-child(3) > td > table > tbody > tr > td > a")
    private WebElement _orderNumber;
    @FindBy(xpath = "//div[@id='generic-dialog-confirm'] // following :: span[contains(text(),'OK')]")
    private WebElement _clickOnRepeatOrder;
    @FindBy(id = "cancelOrder")
    private WebElement _clickOnCancelOrder;
    @FindBy(id = "cancellationCommentInput")
    private WebElement _cancellationCommentInput;
    @FindBy(id = "currency")
    private WebElement _Currency;
    @FindBy(xpath = "//*[@id='saveButton']")
    private WebElement _submitChange;
    @FindBy(id = "cancelOrderComment2")
    private WebElement _clickOnCancelOrderComment2;
    @FindBy(xpath = "//*[@id=\"orderDetailsTable\"]/tbody/tr[2]/td[1]")
    private WebElement _getTransactionNo;
    @FindBy(linkText = "Transaction Search")
    private WebElement _clickOnTransactionSearch;
    @FindBy(id = "transactionNoInput")
    private WebElement _transactionNo;
    @FindBy(id = "statusSelection")
    private WebElement _clickOnStatusSelection;
    @FindBy(id = "buttonSubmitSimpleSearch")
    private WebElement _clickOnSearch;
    @FindBy(xpath = "//*[@id=\"highlightRow\"]/td[1]/a")
    private WebElement _clickOnOrderCancelled;

    public RepeatOrderAndCancelOrderPage clickOnRepeatOrder() {
      //  ClickControl.click(_orderNumber);
        ClickControl.click(_clickOnRepeatOrderButton);
        ClickControl.click(_clickOnRepeatOrder);
        DropDownControl.selectDropDownByVisibleText(_Currency, "Dollars (United States)");
       // ClickControl.click(_submitChange);
        OrderControl.getTransactionNo(_getTransactionNo);
        return PageFactory.initElements(webDriver, RepeatOrderAndCancelOrderPage.class);
    }

    public RepeatOrderAndCancelOrderPage cancelOrder() {
        ClickControl.click(_clickOnCancelOrder);
        TextFieldsControl.enterText(_cancellationCommentInput, "I want to cancel order and finish test " + TextFieldsControl.dateStamp());
        ClickControl.click(_clickOnCancelOrderComment2);
        return PageFactory.initElements(webDriver, RepeatOrderAndCancelOrderPage.class);
    }

    public RepeatOrderAndCancelOrderPage getOrderCancelled() {
        ClickControl.click(_clickOnTransactionSearch);
        TextFieldsControl.enterText(_transactionNo, OrderControl.transactionNo);
        DropDownControl.selectDropDownByVisibleText(_clickOnStatusSelection, "Order Cancelled");
        ClickControl.click(_clickOnSearch);
        ClickControl.click(_clickOnOrderCancelled);
        return PageFactory.initElements(webDriver, RepeatOrderAndCancelOrderPage.class);
    }
    
}

