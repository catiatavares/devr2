package com.optal.End2EndPages;

import com.optal.HomePage.BasePage;
import com.optal.waits.WebWaits;
import com.optal.webControls.ClickControl;
import com.optal.webControls.DropDownControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AuthoriseAndDeclineIRecordPage extends BasePage {

    @FindBy(id = "selectedUser")
    private WebElement corpBuyer;
    @FindBy(xpath = "//*[contains(text(), '2 Transaction Records')]")
    private WebElement transRec;
    @FindBy(id = "searchCriteriaHeader")
    private WebElement criteria;
    @FindBy(xpath = "//tbody/tr[4]/td[1]/a")
    private WebElement iRec;

    public AuthoriseAndDeclineIRecordPage(WebDriver webDriver) { super(webDriver);
    }

    public AuthoriseAndDeclineIRecordPage allUsers() {
        DropDownControl.selectDropDownByVisibleText(corpBuyer, "All Users");
        return this;
    }

    public AuthoriseAndDeclineIRecordPage associatedTransaction() {
        ClickControl.click(transRec);
        return this;
    }

    public AuthoriseAndDeclineIRecordPage searchCriteria() {
        WebWaits.waitUntilLocatorToBeClickable(criteria,5);
        ClickControl.click(criteria);
        return this;
    }

    public AuthoriseAndDeclineIRecordPage iRecord() {
        ClickControl.click(iRec);
        return this;
    }

}
